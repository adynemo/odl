
<a name="v3.0.0"></a>
## :bookmark: [v3.0.0](https://gitlab.com/adynemo/odl/compare/v2.1.0...v3.0.0)

### :pencil: Docs

* update readme and create contributing documentation

### :sparkles: Features

* forgotten password process implementation
* implement search bar
* UI to manage all users
* implement maintenance mode
* settings manager
* manage arcs external links
* resize image uploaded
* update arc
* manage arcs (part I)
* form to manage each section (universes, eras and periods)
* Use Semantic UI
* Admin home and journal page of modification history
* Display reading order by era
* manage user, register and login part I

### :tada: Improvements

* more security
* user can crop his avatar
* set logos and links optional

### :train2: Road to Symfony

* rename app name to 'ODL'
* Install Symfony and set controller


<a name="v2.1.0"></a>
## :bookmark: v2.1.0

### :bug: Fixes

* sort era par universe on index
* insert links instead of update if not exists
* try to unlink directory
* bad regexp
* bad query to update arc position
* last bugs before merge
* order get covers
* path for search input
* DateTimeZone
* fix path cover
* cover path inserted in db
* width search bar on mobile
* link button admin
* fix session
* some bugs with update and changelog
* don't remove cover if no file uploaded
* handle various variables
* cover deleted
* quote cuts the input text 
* bug deletes cover

### :tada: Improvements

* logo of ext links in db for preview add
* result page with each universe data
* add screen to back to home
* index and odl page cause by refactor db
* form to add arc
* create section for universe, era, period
* universe creation
* create section to create universe

### :sparkles: Features

* customize external links for comics editors
* css for manage universes page bonus: remove old image
* update era per universe
* screen to manage universes
* one file for display reading order
* create index and display universe and era
* add robots.txt
* ACL first blood for editor
* get covers
* input file accept image only
* improve results
* fix responsive for search bar
* improve update-in-line
* look and feel
* display id
* delete with ajax
* message when change position
* refresh data after update
* update in line

### :recycle: Code Refactoring

* rename file from modify to update-arc

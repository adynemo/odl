<?php

namespace ODL\Tests\Security;

use ODL\Entity\Era;
use ODL\Entity\User;
use ODL\Security\EraVoter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class EraVoterTest extends TestCase
{
    public function testUnsupportWrongAttribute()
    {
        [$token, $voter] = $this->getRequirements();

        $token
            ->expects($this->never())
            ->method('getUser');

        $isSupported = $voter->vote($token, null, ['random']);

        $this->assertEquals(VoterInterface::ACCESS_ABSTAIN, $isSupported);
    }

    public function testUnsupportNoEraSubject()
    {
        [$token, $voter] = $this->getRequirements();
        $subject = new \stdClass();

        $token
            ->expects($this->never())
            ->method('getUser');

        $isSupported = $voter->vote($token, $subject, [EraVoter::EDIT]);

        $this->assertEquals(VoterInterface::ACCESS_ABSTAIN, $isSupported);
    }

    public function testDeniesAnonymousUser()
    {
        [$token, $voter, $subject] = $this->getRequirements();

        $token
            ->expects($this->once())
            ->method('getUser')
            ->willReturn(null);

        $isGranted = $voter->vote($token, $subject, [EraVoter::EDIT]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $isGranted);
    }

    public function testDeniesNotAllowedUser()
    {
        [$token, $voter, $subject, $user] = $this->getRequirements();
        $otherEra = (new Era())
            ->setName('Other Era');

        $user->addEditableEras($otherEra);
        $token
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $isGranted = $voter->vote($token, $subject, [EraVoter::EDIT]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $isGranted);
    }

    public function testGrantUser()
    {
        [$token, $voter, $subject, $user] = $this->getRequirements();

        $user->addEditableEras($subject);
        $token
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $isGranted = $voter->vote($token, $subject, [EraVoter::EDIT]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $isGranted);
    }

    private function getRequirements(): array
    {
        $token = $this->createMock(TokenInterface::class);
        $voter = new EraVoter();
        $subject = (new Era())
            ->setName('Subject');
        $user = new User();

        return [$token, $voter, $subject, $user];
    }
}

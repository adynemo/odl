<?php

namespace ODL\Tests\Security;

use ODL\TestCase\DatabaseWebTestCase;

class UserCheckerTest extends DatabaseWebTestCase
{
    public function testLoginMustBeRefusedIfUserIsNotActivated()
    {
        $crawler = $this->client->request('GET', '/security/login');

        $form = $crawler->filter('form.login')->form();
        $this->client->submit($form, [
            '_username' => 'inactive',
            '_password' => 'password',
        ]);
        $this->client->followRedirect();

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('div.alert-danger', 'Your account is not activated. An email was sent to you with an activation link.');
    }

    public function testLoginMustBeAcceptedIfUserIsActivated()
    {
        $crawler = $this->client->request('GET', '/security/login');

        $form = $crawler->filter('form.login')->form();
        $this->client->submit($form, [
            '_username' => 'user',
            '_password' => 'password',
        ]);

        $this->assertResponseRedirects('http://localhost/');

        $this->client->followRedirect();

        $this->assertResponseIsSuccessful();
    }
}

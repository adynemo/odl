<?php

namespace ODL\Tests\Controller\API;

use ODL\DataFixtures\PeriodFixtures;
use ODL\Entity\Arc;
use ODL\Entity\Era;
use ODL\Entity\Period;
use ODL\Entity\User;
use ODL\Service\FileManager;
use ODL\TestCase\DatabaseWebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class ArcAPITest extends DatabaseWebTestCase
{
    public function testCanCreateArcToBeValidatedWithoutCoverAndOnFirstPositionOfLastPeriod()
    {
        // Given
        $era = $this->entityManager->getRepository(Era::class)->findOneBy(['name' => 'Era']);
        $lastPeriod = $this->entityManager->getRepository(Period::class)->findOneBy(['era' => $era, 'position' => PeriodFixtures::NEXT_POSITION - 1]);

        $data = [
            'title' => 'New Arc',
            'description' => 'New arc created by API',
            'isEvent' => false,
            'era' => $era->getId(),
        ];

        // When
        $this->doRequest('contributor', $data);

        // Then
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent());
        $this->assertSame(Response::HTTP_CREATED, $response->getStatusCode());

        $arc = $this->entityManager->getRepository(Arc::class)->find($content->id);

        $this->assertNotNull($arc);
        $this->assertSame($lastPeriod, $arc->getPeriod());
        $this->assertSame(1, $arc->getPosition());
        $this->assertFalse($arc->getIsValidated());
        $this->assertSame($data['title'], $arc->getTitle());
        $this->assertSame($data['description'], $arc->getContent());
        $this->assertSame($data['isEvent'], $arc->getIsEvent());
        $this->assertSame($era, $arc->getPeriod()->getEra());
        $this->assertSame('', $arc->getCover());
    }

    public function testCanCreateArcToBeValidatedWithoutCoverAndOnLastPositionOfLastPeriod()
    {
        // Given
        $era = $this->entityManager->getRepository(Era::class)->findOneBy(['name' => 'Era']);
        $lastPeriod = $this->entityManager->getRepository(Period::class)->findOneBy(['era' => $era, 'position' => PeriodFixtures::NEXT_POSITION - 1]);
        $firstArc = (new Arc())
            ->setTitle('First Arc')
            ->setContent('Description')
            ->setCover('filename')
            ->setPosition(1)
            ->setIsEvent(false)
            ->setIsValidated(true)
            ->setPeriod($lastPeriod);
        $this->entityManager->persist($firstArc);
        $this->entityManager->flush();

        $data = [
            'title' => 'New Arc',
            'description' => 'New arc created by API',
            'isEvent' => false,
            'era' => $era->getId(),
        ];

        // When
        $this->doRequest('contributor', $data);

        // Then
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent());
        $this->assertSame(Response::HTTP_CREATED, $response->getStatusCode());

        $arc = $this->entityManager->getRepository(Arc::class)->find($content->id);

        $this->assertNotNull($arc);
        $this->assertSame($lastPeriod, $arc->getPeriod());
        $this->assertSame(2, $arc->getPosition());
        $this->assertFalse($arc->getIsValidated());
        $this->assertSame($data['title'], $arc->getTitle());
        $this->assertSame($data['description'], $arc->getContent());
        $this->assertSame($data['isEvent'], $arc->getIsEvent());
        $this->assertSame($era, $arc->getPeriod()->getEra());
        $this->assertSame('', $arc->getCover());
    }

    public function testCanCreateArcToBeValidatedWithCover()
    {
        // Given
        $era = $this->entityManager->getRepository(Era::class)->findOneBy(['name' => 'Era']);
        $lastPeriod = $this->entityManager->getRepository(Period::class)->findOneBy(['era' => $era, 'position' => PeriodFixtures::NEXT_POSITION - 1]);

        $filename = 'filename.jpg';
        $cover = new UploadedFile(
            dirname(dirname(__DIR__)).'/assets/cover.jpg',
            'cover.jpeg',
            'image/jpeg',
            null
        );
        $data = [
            'title' => 'New Arc',
            'description' => 'New arc created by API',
            'isEvent' => false,
            'era' => $era->getId(),
        ];

        $fileManagerMock = $this->createMock(FileManager::class);
        $fileManagerMock
            ->expects($this->once())
            ->method('setTargetDirectory')
            ->with(FileManager::COVERS);
        $fileManagerMock
            ->expects($this->once())
            ->method('upload')
            ->willReturn($filename);
        self::$container->set('ODL\Service\FileManager', $fileManagerMock);

        // When
        $this->doRequest('contributor', $data, ['cover' => $cover]);

        // Then
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent());
        $this->assertSame(Response::HTTP_CREATED, $response->getStatusCode());

        $arc = $this->entityManager->getRepository(Arc::class)->find($content->id);

        $this->assertNotNull($arc);
        $this->assertSame($lastPeriod, $arc->getPeriod());
        $this->assertSame(1, $arc->getPosition());
        $this->assertFalse($arc->getIsValidated());
        $this->assertSame($data['title'], $arc->getTitle());
        $this->assertSame($data['description'], $arc->getContent());
        $this->assertSame($data['isEvent'], $arc->getIsEvent());
        $this->assertSame($era, $arc->getPeriod()->getEra());
        $this->assertSame($filename, $arc->getCover());
    }

    public function testCanReturnBadRequestWhenCalledWithDataMissing()
    {
        // Given
        $era = $this->entityManager->getRepository(Era::class)->findOneBy(['name' => 'Era']);
        $lastPeriod = $this->entityManager->getRepository(Period::class)->findOneBy(['era' => $era, 'position' => PeriodFixtures::NEXT_POSITION - 1]);
        $checkReturn = function (Response $response) {
            $this->checkResponseError($response, Response::HTTP_BAD_REQUEST, 'Some required fields are missing');
        };

        // No data
        // When
        $this->doRequest('contributor');

        // Then
        $response = $this->client->getResponse();
        $checkReturn($response);

        // Title missing
        $data = [
            'description' => 'New arc created by API',
            'era' => $era->getId(),
        ];

        // When
        $this->doRequest('contributor', $data);

        // Then
        $response = $this->client->getResponse();
        $checkReturn($response);

        // Description missing
        $data = [
            'title' => 'New Arc',
            'era' => $era->getId(),
        ];

        // When
        $this->doRequest('contributor', $data);

        // Then
        $response = $this->client->getResponse();
        $checkReturn($response);

        // Era missing
        $data = [
            'title' => 'New Arc',
            'description' => 'New arc created by API',
        ];

        // When
        $this->doRequest('contributor', $data);

        // Then
        $response = $this->client->getResponse();
        $checkReturn($response);

        $arc = $this->entityManager->getRepository(Arc::class)->findBy(['period' => $lastPeriod]);
        $this->assertEmpty($arc);
    }

    public function testCanReturnBadRequestIfNoEraFound()
    {
        // Given
        $era = $this->entityManager->getRepository(Era::class)->findOneBy(['name' => 'Era']);
        $lastPeriod = $this->entityManager->getRepository(Period::class)->findOneBy(['era' => $era, 'position' => PeriodFixtures::NEXT_POSITION - 1]);
        $data = [
            'title' => 'New Arc',
            'description' => 'New arc created by API',
            'isEvent' => false,
            'era' => 'wrong-era-id',
        ];

        // When
        $this->doRequest('contributor', $data);

        // Then
        $response = $this->client->getResponse();
        $this->checkResponseError($response, Response::HTTP_BAD_REQUEST, 'Era does not match anyway');

        $arc = $this->entityManager->getRepository(Arc::class)->findBy(['period' => $lastPeriod]);
        $this->assertEmpty($arc);
    }

    public function testCanReturnInternalServerErrorIfNoPeriodExistsOnEra()
    {
        // Given
        $era = $this->entityManager->getRepository(Era::class)->findOneBy(['slug' => 'era-2']);
        $this->assertEmpty(($era->getPeriods()));
        $data = [
            'title' => 'New Arc',
            'description' => 'New arc created by API',
            'era' => $era->getId(),
        ];

        // When
        $this->doRequest('contributor', $data);

        // Then
        $response = $this->client->getResponse();
        $this->checkResponseError($response, Response::HTTP_INTERNAL_SERVER_ERROR, 'API cannot create arc, the process fails');
    }

    public function testCanReturnForbiddenErrorIfUserHasNotAtLeastContributorRole()
    {
        // When
        $this->doRequest('user');

        // Then
        $response = $this->client->getResponse();
        $this->checkResponseError($response, Response::HTTP_FORBIDDEN, null);
    }

    private function doRequest(string $username, array $parameters = [], array $files = [])
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $username]);
        $this->client->request('POST', '/api/arc/create', $parameters, $files, ['HTTP_X_AUTH_TOKEN' => $user->getApiToken()]);
    }

    private function checkResponseError(Response $response, int $code, ?string $message)
    {
        $this->assertSame($code, $response->getStatusCode());
        $this->assertSame($message, json_decode($response->getContent()));
    }
}

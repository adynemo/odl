<?php

namespace ODL\Tests\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\User;
use ODL\EventListener\SecurityListener;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityListenerTest extends TestCase
{
    public function testCanStoreLoginDateAndDisplayWelcomeMessage()
    {
        $userLocale = 'en';
        $username = 'LukeS';
        $translatedMessage = 'Welcome!';
        $user = (new User())
            ->setUsername($username)
            ->setLocale($userLocale);

        $request = $this->createMock(Request::class);
        $tokenInterface = $this->createMock(TokenInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $session = $this->createMock(Session::class);
        $flashBag = $this->createMock(FlashBagInterface::class);

        $event = new InteractiveLoginEvent($request, $tokenInterface);

        $securityListener = new SecurityListener($entityManager, $translator);

        $tokenInterface
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $translator
            ->expects($this->once())
            ->method('trans')
            ->with(
                $this->equalTo('welcome_message'),
                ['username' => $username],
                $this->equalTo('general'),
                $this->equalTo($userLocale)
            )
            ->willReturn($translatedMessage);

        $entityManager
            ->expects($this->once())
            ->method('persist')
            ->with($user);

        $entityManager
            ->expects($this->once())
            ->method('flush');

        $flashBag
            ->expects($this->once())
            ->method('add')
            ->with($this->equalTo('info'), $translatedMessage);

        $session
            ->expects($this->once())
            ->method('getFlashBag')
            ->willReturn($flashBag);

        $request
            ->expects($this->once())
            ->method('getSession')
            ->willReturn($session);

        $this->assertNull($user->getLastLogin());

        $securityListener->onSecurityInteractiveLogin($event);

        $this->assertInstanceOf(\DateTimeInterface::class, $user->getLastLogin());
    }
}

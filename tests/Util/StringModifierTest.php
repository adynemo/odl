<?php

namespace ODL\Tests\Util;

use ODL\Util\StringModifier;
use PHPUnit\Framework\TestCase;

class StringModifierTest extends TestCase
{
    public function testCanGenerateSlug()
    {
        // Given
        $stringModifier = new StringModifier();

        $input = 'Skywalker (Luke/Anakin) \ Jedi';
        $expected = 'skywalker--luke-anakin----jedi';

        // When
        $output = $stringModifier->generateSlug($input);

        // Then
        $this->assertSame($expected, $output);
    }

    public function testCanGenerateRandomString()
    {
        // Given
        $stringModifier = new StringModifier();

        // When
        $output = $stringModifier->generateRandomString();

        // Then
        $this->assertIsString($output);
        $this->assertSame(32, strlen($output));
    }
}

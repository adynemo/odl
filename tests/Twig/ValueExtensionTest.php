<?php

namespace ODL\Tests\Twig;

use ODL\Twig\ValueExtension;
use PHPUnit\Framework\TestCase;

class ValueExtensionTest extends TestCase
{
    public function testUnescape()
    {
        // Given
        $value = "J'ai &quot;sorti&quot; le &lt;strong&gt;chien&lt;/strong&gt; tout &agrave; l'heure";
        $customExtension = new ValueExtension();

        // Expected
        $expected = "J'ai \"sorti\" le <strong>chien</strong> tout à l'heure";

        // When
        $output = $customExtension->unescape($value);

        // Then
        $this->assertSame($expected, $output);
    }

    /**
     * @dataProvider getDataForBoolTest
     */
    public function testBool($given, $expected)
    {
        // Given
        $customExtension = new ValueExtension();

        // When
        $output = $customExtension->bool($given);

        // Then
        $this->assertSame($expected, $output);
    }

    public function testCanDecodeJsonToObject()
    {
        // Given
        $customExtension = new ValueExtension();
        $expected = (object) ['key' => 'value'];

        // When
        $output = $customExtension->jsonDecode('{"key": "value"}');

        // Then
        $this->assertIsObject($output);
        $this->assertEquals($expected, $output);
    }

    public function testCanDecodeJsonToArray()
    {
        // Given
        $customExtension = new ValueExtension();
        $expected = ['value', 'other'];

        // When
        $output = $customExtension->jsonDecode('["value", "other"]');

        // Then
        $this->assertIsArray($output);
        $this->assertEquals($expected, $output);
    }

    public function testCanComputePercent()
    {
        // Given
        $customExtension = new ValueExtension();

        // When
        $output = $customExtension->computePercent(1, 4);

        // Then
        $this->assertIsFloat($output);
        $this->assertEquals(25, $output);
    }

    public function getDataForBoolTest(): array
    {
        return [
            ['1', true],
            ['0', false],
            ['true', true],
            ['false', false],
            ['null', false],
            [null, false],
        ];
    }
}

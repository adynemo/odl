<?php

namespace ODL\Tests\Service;

use InvalidArgumentException;
use ODL\Service\FileManager;
use ODL\Service\ImageManager;
use ODL\Util\StringModifier;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBag;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\ExtensionFileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileManagerTest extends TestCase
{
    public function testCanUploadCover()
    {
        // Given
        $filenameExpected = 'filename';
        $extensionExpected = 'jpg';

        [$mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem] = $this->getMocks();
        $mockedUploadedFile = $this->createMock(UploadedFile::class);

        $mockedContainer->expects($this->once())
            ->method('get')
            ->with('app.assets_image_covers')
            ->willReturn('/path/to/images/covers');
        $mockedStringModifier->expects($this->once())
            ->method('generateRandomString')
            ->willReturn($filenameExpected);
        $mockedUploadedFile->expects($this->atLeastOnce())
            ->method('guessExtension')
            ->willReturn($extensionExpected);
        $mockedImageManager->expects($this->exactly(2))
            ->method('setParameters')
            ->willReturn($mockedImageManager);
        $mockedImageManager->expects($this->exactly(2))
            ->method('resize')
            ->willReturn(true);
        $mockedUploadedFile->expects($this->exactly(2))
            ->method('move')
            ->willReturn(null);

        $fileManager = new FileManager($mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem);
        $fileManager->setTargetDirectory($fileManager::COVERS);

        $expected = "{$filenameExpected}.{$extensionExpected}";

        // When
        $upload = $fileManager->upload($mockedUploadedFile);

        // Then
        $this->assertSame($expected, $upload);

        // When
        $upload = $fileManager->upload($mockedUploadedFile, $filenameExpected);

        // Then
        $this->assertSame($expected, $upload);
    }

    public function testUploadThrowRuntimeExceptionIfResizingFails()
    {
        // Given
        [$mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem] = $this->getMocks();
        $mockedUploadedFile = $this->createMock(UploadedFile::class);

        $mockedContainer->expects($this->once())
            ->method('get')
            ->with('app.assets_image_covers')
            ->willReturn('/path/to/images/covers');
        $mockedStringModifier->expects($this->once())
            ->method('generateRandomString')
            ->willReturn('filename');
        $mockedUploadedFile->expects($this->atLeastOnce())
            ->method('guessExtension')
            ->willReturn('jpg');
        $mockedImageManager->expects($this->once())
            ->method('setParameters')
            ->with(FileManager::COVERS);
        $mockedImageManager->expects($this->once())
            ->method('resize')
            ->willReturn(false);
        $mockedUploadedFile->expects($this->never())
            ->method('move');

        $fileManager = new FileManager($mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem);
        $fileManager->setTargetDirectory(FileManager::COVERS);

        // Expect
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Image resizing fails');

        // When
        $fileManager->upload($mockedUploadedFile);
    }

    public function testSetTargetDirectoryMethodCanThrowInvalidArgumentExceptionIfTypeDoesNotMatch()
    {
        // Given
        [$mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem] = $this->getMocks();

        $mockedContainer->expects($this->never())
            ->method('get');

        $fileManager = new FileManager($mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem);

        // Expect
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Target directory cannot be set, wrong type provided');

        // When
        $fileManager->setTargetDirectory('wrong type');
    }

    public function testCanUploadImageFromBase64()
    {
        // Given
        $path = 'tests/assets/cover.jpg';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/'.$type.';base64,'.base64_encode($data);
        $filenameExpected = 'filename';

        [$mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem] = $this->getMocks();

        $mockedContainer->expects($this->once())
            ->method('get')
            ->with('app.assets_image_user_avatars')
            ->willReturn('/path/to/images/avatars');
        $mockedStringModifier->expects($this->once())
            ->method('generateRandomString')
            ->willReturn($filenameExpected);
        $mockedImageManager->expects($this->exactly(1))
            ->method('createFromData');

        $fileManager = new FileManager($mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem);
        $fileManager->setTargetDirectory($fileManager::AVATAR);

        $expected = "{$filenameExpected}.jpeg";

        // When
        $filename = $fileManager->upload64($base64);

        // Then
        $this->assertSame($expected, $filename);
    }

    public function testCanThrowExtensionFileExceptionIfBase64HasNotGoodMimeType()
    {
        $path = 'tests/Service/FileManagerTest.php';
        $data = file_get_contents($path);
        $base64 = 'data:image/jpeg;base64,'.base64_encode($data);

        [$mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem] = $this->getMocks();

        $mockedContainer->expects($this->once())
            ->method('get')
            ->with('app.assets_image_user_avatars')
            ->willReturn('/path/to/images/avatars');
        $mockedStringModifier->expects($this->never())
            ->method('generateRandomString');
        $mockedImageManager->expects($this->never())
            ->method('createFromData');

        $fileManager = new FileManager($mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem);
        $fileManager->setTargetDirectory($fileManager::AVATAR);

        $this->expectException(ExtensionFileException::class);
        $this->expectExceptionMessage('Image must be png or jpeg file');

        $fileManager->upload64($base64);
    }

    public function testCanRemoveFile()
    {
        $filename = 'filename.ext';
        $path = '/path/to/images/avatars';
        $filepath = sprintf('%s/%s', $path, $filename);

        [$mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem] = $this->getMocks();

        $mockedContainer->expects($this->once())
            ->method('get')
            ->with('app.assets_image_user_avatars')
            ->willReturn($path);

        $mockedFilesystem->expects($this->once())
            ->method('exists')
            ->with($filepath)
            ->willReturn(true);

        $mockedFilesystem->expects($this->once())
            ->method('remove')
            ->with($filepath);

        $fileManager = new FileManager($mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem);
        $fileManager->setTargetDirectory($fileManager::AVATAR);

        $fileManager->remove($filename);
    }

    private function getMocks()
    {
        $mockedContainer = $this->createMock(ContainerBag::class);
        $mockedStringModifier = $this->createMock(StringModifier::class);
        $mockedImageManager = $this->createMock(ImageManager::class);
        $mockedFilesystem = $this->createMock(Filesystem::class);

        return [$mockedContainer, $mockedStringModifier, $mockedImageManager, $mockedFilesystem];
    }
}

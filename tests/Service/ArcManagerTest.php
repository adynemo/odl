<?php

namespace ODL\Tests\Service;

use InvalidArgumentException;
use ODL\DataFixtures\ArcFixtures;
use ODL\Entity\Arc;
use ODL\Entity\Era;
use ODL\Entity\Period;
use ODL\Event\ArcEvent;
use ODL\Service\ArcManager;
use ODL\Service\FileManager;
use ODL\TestCase\DatabaseTestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ArcManagerTest extends DatabaseTestCase
{
    public function testCanCreateArcOnFirstPosition()
    {
        [$fileManagerMock, $dispatcherMock, $fileMock, $_] = $this->getRequirements();

        $filename = 'cover.jpg';
        $period = $this->entityManager->getRepository(Period::class)->findOneBy(['slug' => 'period-2']);
        $arc = (new Arc())
            ->setTitle('New Arc')
            ->setContent('Description')
            ->setIsEvent(false)
            ->setPeriod($period);

        $fileManagerMock
            ->expects($this->once())
            ->method('setTargetDirectory')
            ->with(FileManager::COVERS);
        $fileManagerMock
            ->expects($this->once())
            ->method('upload')
            ->with($fileMock)
            ->willReturn($filename);

        $dispatcherMock
            ->expects($this->once())
            ->method('dispatch')
            ->with(new ArcEvent($arc), ArcEvent::CREATED);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $arcManager->create($arc, $fileMock);

        $this->assertSame($filename, $arc->getCover());
        $this->assertSame(1, $arc->getPosition());
    }

    public function testCanCreateArcOnLastPosition()
    {
        [$fileManagerMock, $dispatcherMock, $fileMock, $period] = $this->getRequirements();

        $filename = 'cover.jpg';
        $arc = (new Arc())
            ->setTitle('Third Arc')
            ->setContent('Description')
            ->setIsEvent(false)
            ->setPeriod($period);

        $fileManagerMock
            ->expects($this->once())
            ->method('setTargetDirectory')
            ->with(FileManager::COVERS);
        $fileManagerMock
            ->expects($this->once())
            ->method('upload')
            ->with($fileMock)
            ->willReturn($filename);

        $dispatcherMock
            ->expects($this->once())
            ->method('dispatch')
            ->with(new ArcEvent($arc), ArcEvent::CREATED);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $arcManager->create($arc, $fileMock);

        $this->assertSame($filename, $arc->getCover());
        $this->assertSame(ArcFixtures::NEXT_POSITION, $arc->getPosition());
    }

    public function testCanCreateArcAndUpdatePositions()
    {
        [$fileManagerMock, $dispatcherMock, $fileMock, $period] = $this->getRequirements();

        $filename = 'cover.jpg';
        $arc = (new Arc())
            ->setTitle('Between')
            ->setContent('Description')
            ->setIsEvent(false)
            ->setPosition(2)
            ->setPeriod($period);

        $fileManagerMock
            ->expects($this->once())
            ->method('setTargetDirectory')
            ->with(FileManager::COVERS);
        $fileManagerMock
            ->expects($this->once())
            ->method('upload')
            ->with($fileMock)
            ->willReturn($filename);

        $dispatcherMock
            ->expects($this->once())
            ->method('dispatch')
            ->with(new ArcEvent($arc), ArcEvent::CREATED);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $arcManager->create($arc, $fileMock);

        $this->assertSame($filename, $arc->getCover());
        $this->assertSame(2, $arc->getPosition());

        $arc2 = $this->entityManager->getRepository(Arc::class)->findOneBy(['title' => 'Second Arc']);
        $this->assertSame(3, $arc2->getPosition());
    }

    public function testCanRemoveLastArc()
    {
        [$fileManagerMock, $dispatcherMock, $_, $period] = $this->getRequirements();

        $filename = 'cover.jpg';
        $title = 'Last Arc';
        $arc = (new Arc())
            ->setTitle($title)
            ->setContent('Description')
            ->setIsEvent(false)
            ->setPosition(ArcFixtures::NEXT_POSITION)
            ->setCover($filename)
            ->setPeriod($period);

        $this->entityManager->persist($arc);
        $this->entityManager->flush();

        $fileManagerMock
            ->expects($this->once())
            ->method('setTargetDirectory')
            ->with(FileManager::COVERS);
        $fileManagerMock
            ->expects($this->once())
            ->method('remove')
            ->with($filename);

        $dispatcherMock
            ->expects($this->once())
            ->method('dispatch')
            ->with(new ArcEvent($arc), ArcEvent::REMOVED);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $arcManager->remove($arc);

        $result = $this->entityManager->getRepository(Arc::class)->findOneBy(['title' => $title]);
        $this->assertNull($result);
    }

    public function testCanRemoveAnyArcAndReplaceOthers()
    {
        [$fileManagerMock, $dispatcherMock, $_, $period] = $this->getRequirements();
        $arcToRemove = $this->entityManager->getRepository(Arc::class)->find(2);

        $title = 'Last Arc';
        $arc = (new Arc())
            ->setTitle($title)
            ->setContent('Description')
            ->setIsEvent(false)
            ->setPosition(ArcFixtures::NEXT_POSITION)
            ->setCover('cover.jpg')
            ->setPeriod($period);

        $this->entityManager->persist($arc);
        $this->entityManager->flush();

        $fileManagerMock
            ->expects($this->once())
            ->method('setTargetDirectory')
            ->with(FileManager::COVERS);
        $fileManagerMock
            ->expects($this->once())
            ->method('remove')
            ->with('filename');

        $dispatcherMock
            ->expects($this->once())
            ->method('dispatch')
            ->with(new ArcEvent($arcToRemove), ArcEvent::REMOVED);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $arcManager->remove($arcToRemove);

        $result = $this->entityManager->getRepository(Arc::class)->findAll();
        $this->assertCount(2, $result);
        $this->assertSame(2, $arc->getPosition());
    }

    public function testCanUpdateCoverAndRemoveOlder()
    {
        [$fileManagerMock, $dispatcherMock, $fileMock] = $this->getRequirements();
        $arc = $this->entityManager->getRepository(Arc::class)->find(1);
        $this->assertSame('First Arc', $arc->getTitle());
        $this->assertSame('filename', $arc->getCover());

        $oldArc = clone $arc;
        $cover = 'cover.jpg';
        $title = 'Updated Arc';
        $arc->setTitle($title);

        $fileManagerMock
            ->expects($this->once())
            ->method('setTargetDirectory')
            ->with(FileManager::COVERS);
        $fileManagerMock
            ->expects($this->once())
            ->method('remove')
            ->with('filename');
        $fileManagerMock
            ->expects($this->once())
            ->method('upload')
            ->with($fileMock)
            ->willReturn($cover);

        $dispatcherMock
            ->expects($this->once())
            ->method('dispatch')
            ->with(new ArcEvent($arc, $oldArc), ArcEvent::UPDATED);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $arcManager->update($arc, $fileMock, $oldArc);

        $result = $this->entityManager->getRepository(Arc::class)->find(1);
        $this->assertSame($title, $result->getTitle());
        $this->assertSame($cover, $result->getCover());
    }

    public function testCanMoveArcToTheRight()
    {
        [$fileManagerMock, $dispatcherMock, $_, $period] = $this->getRequirements();

        $title = 'Third Arc';
        $newArc = (new Arc())
            ->setTitle($title)
            ->setContent('Description')
            ->setIsEvent(false)
            ->setPosition(ArcFixtures::NEXT_POSITION)
            ->setCover('cover.jpg')
            ->setPeriod($period);
        $this->entityManager->persist($newArc);
        $this->entityManager->flush();

        $arc = $this->entityManager->getRepository(Arc::class)->find(2);
        $this->assertSame('Second Arc', $arc->getTitle());

        $oldArc = clone $arc;
        $updatedTitle = 'Updated Arc';
        $arc
            ->setPosition(3)
            ->setTitle($updatedTitle);

        $fileManagerMock
            ->expects($this->never())
            ->method('setTargetDirectory');
        $fileManagerMock
            ->expects($this->never())
            ->method('remove');
        $fileManagerMock
            ->expects($this->never())
            ->method('upload');

        $dispatcherMock
            ->expects($this->once())
            ->method('dispatch')
            ->with(new ArcEvent($arc, $oldArc), ArcEvent::UPDATED);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $arcManager->update($arc, null, $oldArc);

        $newArcUpdated = $this->entityManager->getRepository(Arc::class)->find(3);
        $this->assertSame($title, $newArcUpdated->getTitle());
        $this->assertSame(2, $newArcUpdated->getPosition());

        $updatedArc = $this->entityManager->getRepository(Arc::class)->find(2);
        $this->assertSame($updatedTitle, $updatedArc->getTitle());
        $this->assertSame(3, $updatedArc->getPosition());
    }

    public function testCanTakeOldArcsPeriodIfUpdatedArcHasNoOne()
    {
        [$fileManagerMock, $dispatcherMock, $_] = $this->getRequirements();
        $arc = $this->entityManager->getRepository(Arc::class)->find(1);
        $this->assertSame('First Arc', $arc->getTitle());

        $oldArc = clone $arc;
        $title = 'Updated Arc';
        $arc
            ->setTitle($title)
            ->setPeriod(null);

        $fileManagerMock
            ->expects($this->never())
            ->method('setTargetDirectory');
        $fileManagerMock
            ->expects($this->never())
            ->method('remove');
        $fileManagerMock
            ->expects($this->never())
            ->method('upload');

        $dispatcherMock
            ->expects($this->once())
            ->method('dispatch')
            ->with(new ArcEvent($arc, $oldArc), ArcEvent::UPDATED);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $arcManager->update($arc, null, $oldArc);

        $result = $this->entityManager->getRepository(Arc::class)->find(1);
        $this->assertSame($title, $result->getTitle());
        $this->assertSame($oldArc->getPeriod(), $result->getPeriod());
    }

    public function testCanSearchQueryIntoTitleAndDescriptionFromAllPeriodsOfOneEra()
    {
        [$fileManagerMock, $dispatcherMock] = $this->getRequirements();
        $period = $this->entityManager->getRepository(Period::class)->findOneBy(['slug' => 'period-2']);
        $era = $this->entityManager->getRepository(Era::class)->findOneBy(['slug' => 'era']);

        $fileManagerMock
            ->expects($this->never())
            ->method('setTargetDirectory');
        $fileManagerMock
            ->expects($this->never())
            ->method('remove');
        $fileManagerMock
            ->expects($this->never())
            ->method('upload');

        $dispatcherMock
            ->expects($this->never())
            ->method('dispatch');

        $arc = (new Arc())
            ->setTitle('New Arc')
            ->setContent('Arc from Second period')
            ->setIsEvent(false)
            ->setPosition(1)
            ->setCover('cover.jpg')
            ->setPeriod($period);
        $this->entityManager->persist($arc);
        $this->entityManager->flush();

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $arcs = $arcManager->findArcsByQuery('Second', $era);

        $this->assertCount(2, $arcs);
        $this->assertSame('Second Arc', $arcs[0]->getTitle());
        $this->assertSame('New Arc', $arcs[1]->getTitle());
    }

    public function testLogCanCheckTypeArgumentAndThrowInvalidArgumentException()
    {
        $type = 'wrong type';
        [$fileManagerMock, $dispatcherMock, $_, $period] = $this->getRequirements();

        $dispatcherMock
            ->expects($this->never())
            ->method('dispatch');

        $arc = (new Arc())
            ->setTitle('New Arc')
            ->setContent('Arc from Second period')
            ->setIsEvent(false)
            ->setPosition(1)
            ->setCover('cover.jpg')
            ->setPeriod($period);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(sprintf('`%s` argument is not provided by %s', $type, ArcEvent::class));

        $arcManager->log($arc, $type);
    }

    public function testLogCanThrowInvalidArgumentExceptionIfNoOldArcIsPassedForAnUpdate()
    {
        [$fileManagerMock, $dispatcherMock, $_, $period] = $this->getRequirements();

        $dispatcherMock
            ->expects($this->never())
            ->method('dispatch');

        $arc = (new Arc())
            ->setTitle('New Arc')
            ->setContent('Arc from Second period')
            ->setIsEvent(false)
            ->setPosition(1)
            ->setCover('cover.jpg')
            ->setPeriod($period);

        $arcManager = new ArcManager($this->entityManager, $fileManagerMock, $dispatcherMock);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('You must pass the old arc for an update event');

        $arcManager->log($arc, ArcEvent::UPDATED);
    }

    private function getRequirements(): array
    {
        $period = $this->entityManager->getRepository(Period::class)->findOneBy(['slug' => 'period']);

        $fileManagerMock = $this->createMock(FileManager::class);
        $dispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $fileMock = $this->createMock(UploadedFile::class);

        return [$fileManagerMock, $dispatcherMock, $fileMock, $period];
    }
}

<?php

namespace ODL\Tests\Service;

use ODL\Entity\Setting;
use ODL\Service\FileManager;
use ODL\Service\SettingManager;
use ODL\TestCase\DatabaseTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SettingManagerTest extends DatabaseTestCase
{
    public function testCanFindAllAndSerializeData()
    {
        $expected = [
            'param_one' => 'value_one',
            'param_two' => 'value_two',
        ];
        $fileManagerMock = $this->createMock(FileManager::class);

        $fileManagerMock
            ->expects($this->never())
            ->method('setTargetDirectory');
        $fileManagerMock
            ->expects($this->never())
            ->method('upload');

        $settingManager = new SettingManager($this->entityManager, $fileManagerMock);

        $settings = $settingManager->findAll();

        $this->assertEmpty(array_diff($expected, $settings));
    }

    public function testCanCreateBanner()
    {
        $param = 'website_banner';
        $filename = $param.'.png';
        $fileManagerMock = $this->createMock(FileManager::class);
        $file = $this->createMock(UploadedFile::class);
        $data = [$param => $file];

        $fileManagerMock
            ->expects($this->once())
            ->method('setTargetDirectory')
            ->with(FileManager::WEBSITE_BANNER);
        $fileManagerMock
            ->expects($this->once())
            ->method('upload')
            ->with($file, $param)
            ->willReturn($filename);

        $settingManager = new SettingManager($this->entityManager, $fileManagerMock);

        $settingManager->update($data);

        $setting = $this->entityManager->getRepository(Setting::class)->findOneBy(['param' => $param]);

        $this->assertInstanceOf(Setting::class, $setting);
        $this->assertEquals($filename, $setting->getValue());
    }

    public function testCanLoopCreateAndUpdateSettings()
    {
        $param1 = 'param_name_1';
        $value1 = 'value_1';
        $param2 = 'param_name_2';
        $value2 = 'value_2';
        $setting1 = (new Setting())
            ->setParam($param1)
            ->setValue('old_value');
        $this->entityManager->persist($setting1);
        $this->entityManager->flush();

        $fileManagerMock = $this->createMock(FileManager::class);
        $data = [
            $param1 => $value1,
            $param2 => $value2,
        ];

        $fileManagerMock
            ->expects($this->never())
            ->method('setTargetDirectory');
        $fileManagerMock
            ->expects($this->never())
            ->method('upload');

        $settingManager = new SettingManager($this->entityManager, $fileManagerMock);

        $settingManager->update($data);

        $setting = $this->entityManager->getRepository(Setting::class)->findOneBy(['param' => $param1]);

        $this->assertInstanceOf(Setting::class, $setting);
        $this->assertEquals($value1, $setting->getValue());

        $setting = $this->entityManager->getRepository(Setting::class)->findOneBy(['param' => $param2]);

        $this->assertInstanceOf(Setting::class, $setting);
        $this->assertEquals($value2, $setting->getValue());
    }
}

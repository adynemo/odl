<?php

namespace ODL\Tests\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\User;
use ODL\Service\ForgottenPasswordManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class ForgottenPasswordManagerTest extends TestCase
{
    public function testGetForgottenPasswordToken()
    {
        // Given
        $user = new User();
        $now = (new \DateTime('now'))->format('YmdHis');
        $token = 'token';
        $expectedToken = $token.'date'.$now;

        $mockedEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockedTokenGenerator = $this->createMock(TokenGeneratorInterface::class);

        $mockedEntityManager->expects($this->any())
            ->method('persist')
            ->willReturn(null);

        $mockedEntityManager->expects($this->any())
            ->method('flush')
            ->willReturn(null);

        $mockedTokenGenerator->expects($this->any())
            ->method('generateToken')
            ->willReturn($token);

        $forgottenPasswordManager = new ForgottenPasswordManager($mockedEntityManager, $mockedTokenGenerator);

        // When token was generated less than 10 minutes ago.
        $outputToken = $forgottenPasswordManager->getForgottenPasswordToken($user);

        // Then
        $this->assertSame($expectedToken, $outputToken);
        $this->assertSame($expectedToken, $user->getForgottenPassword());
    }

    public function testIsValidDate()
    {
        // Given
        $mockedEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockedTokenGenerator = $this->createMock(TokenGeneratorInterface::class);
        $forgottenPasswordManager = new ForgottenPasswordManager($mockedEntityManager, $mockedTokenGenerator);

        $date = (new \DateTime('now'))->format('YmdHis');
        $token = 'tokendate'.$date;

        // When token was generated less than 10 minutes ago.
        $isValidDate = $forgottenPasswordManager->isValidDate($token);

        // Then
        $this->assertTrue($isValidDate);

        // Given
        $date = (new \DateTime('20 minutes ago'))->format('YmdHis');
        $token = 'tokendate'.$date;

        // When token was generated more than 10 minutes ago.
        $isValidDate = $forgottenPasswordManager->isValidDate($token);

        // Then
        $this->assertFalse($isValidDate);
    }
}

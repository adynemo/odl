<?php

namespace ODL\Tests\Service;

use ODL\DataCollector\Collection;
use ODL\Entity\Universe;
use ODL\Service\FileManager;
use ODL\Service\UniverseManager;
use ODL\TestCase\DatabaseTestCase;
use ODL\Util\StringModifier;

class UniverseManagerTest extends DatabaseTestCase
{
    public function testFindAll()
    {
        // Given
        $universe1 = new Universe();
        $universe1->setName('Marvel')
            ->setSlug('marvel');
        $universe2 = new Universe();
        $universe2->setName('DC Comics')
            ->setSlug('dc-comics');
        $fileManager = $this->createMock(FileManager::class);
        $repository = $this->entityManager->getRepository(Universe::class);

        $universeManager = new UniverseManager($this->entityManager, $repository, new StringModifier(), $fileManager);

        // When
        $universes = $universeManager->findAll();

        // Then
        $this->assertInstanceOf(Collection::class, $universes);
        $this->assertSame($universe1->getName(), $universes->getCollection()->first()->getName());
        $this->assertSame($universe2->getName(), $universes->getCollection()->next()->getName());
    }

    public function testUpsertUniverse()
    {
        // Given
        $repository = $this->entityManager->getRepository(Universe::class);
        $allUniverses = $repository->findAll();
        $universes = new Collection($allUniverses, Universe::class);

        $nameUpdated = 'Update Marvel';
        $slugUpdated = 'update-marvel';
        $nameNewUniverse = 'Buffyverse';
        $slugNewUniverse = 'buffyverse';

        $universes
            ->getCollection()
            ->first()
            ->setName($nameUpdated);
        $newUniverse = new Universe();
        $newUniverse
            ->setName($nameNewUniverse);
        $universes
            ->getCollection()
            ->add($newUniverse);
        $fileManager = $this->createMock(FileManager::class);
        $fileManager
            ->expects($this->never())
            ->method('upload');
        $fileManager
            ->expects($this->never())
            ->method('remove');

        $universeManager = new UniverseManager($this->entityManager, $repository, new StringModifier(), $fileManager);

        // When
        $universeManager->upsert($universes, [['logo' => null], ['logo' => null], ['logo' => null]]);

        // Then
        $newUniverses = $universeManager->findAll();
        $this->assertSame($nameUpdated, $newUniverses->getCollection()->first()->getName());
        $this->assertSame($slugUpdated, $newUniverses->getCollection()->first()->getSlug());
        $this->assertSame($nameNewUniverse, $newUniverses->getCollection()->last()->getName());
        $this->assertSame($slugNewUniverse, $newUniverses->getCollection()->last()->getSlug());
    }
}

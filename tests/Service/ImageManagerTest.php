<?php

namespace ODL\Tests\Service;

use ODL\Service\ImageManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\ExtensionFileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageManagerTest extends TestCase
{
    private const ASSETS_PATH = 'tests/assets/';
    private const COVER_TYPE = 'cover';
    private const BANNER_TYPE = 'banner';
    private const SIZE = 150;
    private const PARAMETERS = [
        self::COVER_TYPE => [
            'size' => self::SIZE,
            'type' => 'width',
        ],
        self::BANNER_TYPE => [
            'size' => self::SIZE,
            'type' => 'height',
        ],
    ];

    public function testCanThrowInvalidArgumentExceptionIfParameterNotExists()
    {
        $imageManager = new ImageManager(self::PARAMETERS);

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("You must pass valid parameter's name");

        $imageManager->setParameters('avatar');
    }

    public function testCanResizeImageByWidth()
    {
        $filename = 'tmp.jpg';
        $filepath = self::ASSETS_PATH.$filename;
        $filesystem = new Filesystem();
        $filesystem->copy('tests/assets/cover-test-image-manager.jpg', $filepath);
        $size = @getimagesize($filepath);
        $this->assertTrue($filesystem->exists($filepath));
        $this->assertTrue(self::SIZE < $size[0]);

        $file = new UploadedFile($filepath, $filename);

        $imageManager = new ImageManager(self::PARAMETERS);
        $imageManager->setParameters(self::COVER_TYPE);

        $return = $imageManager->resize($file);
        $this->assertTrue($return);

        $newSize = @getimagesize($filepath);
        $this->assertTrue(self::SIZE === $newSize[0]);
        $this->assertTrue($size['mime'] === $newSize['mime']);
        $filesystem->remove($filepath);
    }

    public function testCanResizeImageByHeight()
    {
        $filename = 'tmp.png';
        $filepath = self::ASSETS_PATH.$filename;
        $filesystem = new Filesystem();
        $filesystem->copy('tests/assets/cover-test-image-manager.png', $filepath);
        $size = @getimagesize($filepath);
        $this->assertTrue($filesystem->exists($filepath));
        $this->assertTrue(self::SIZE < $size[1]);

        $file = new UploadedFile($filepath, $filename);

        $imageManager = new ImageManager(self::PARAMETERS);
        $imageManager->setParameters(self::BANNER_TYPE);

        $return = $imageManager->resize($file);
        $this->assertTrue($return);

        $newSize = @getimagesize($filepath);
        $this->assertTrue(self::SIZE === $newSize[1]);
        $this->assertTrue($size['mime'] === $newSize['mime']);
        $filesystem->remove($filepath);
    }

    public function testResizeCanThrowExtensionFileException()
    {
        $filename = 'ImageManagerTest.php';
        $filepath = 'tests/Service/'.$filename;

        $file = new UploadedFile($filepath, $filename);

        $imageManager = new ImageManager(self::PARAMETERS);
        $imageManager->setParameters(self::COVER_TYPE);

        $this->expectException(ExtensionFileException::class);
        $this->expectExceptionMessage(sprintf('You must pass an available mime type: %s or %s', ImageManager::PNG_MIME_TYPE, ImageManager::JPEG_MIME_TYPE));

        $imageManager->resize($file);
    }

    public function testCreateFromDataCanThrowExtensionFileException()
    {
        $filename = 'ImageManagerTest.php';
        $filepath = 'tests/Service/'.$filename;
        $data = file_get_contents($filepath);
        $base64 = 'data:image/jpeg;base64,'.base64_encode($data);

        $imageManager = new ImageManager(self::PARAMETERS);
        $imageManager->setParameters(self::COVER_TYPE);

        $this->expectException(ExtensionFileException::class);
        $this->expectExceptionMessage(
            sprintf(
                'You must pass an available mime type: %s or %s. %s provided',
                ImageManager::PNG_MIME_TYPE,
                ImageManager::JPEG_MIME_TYPE,
                'text/plain'
            )
        );

        $imageManager->createFromData($base64, $filepath);
    }

    public function testCreateJPEGFromData()
    {
        $filename = 'cover-test-image-manager.jpg';
        $filepath = self::ASSETS_PATH.$filename;
        $newFilepath = self::ASSETS_PATH.'tmp.jpeg';
        $data = file_get_contents($filepath);

        $imageManager = new ImageManager(self::PARAMETERS);
        $imageManager->setParameters(self::COVER_TYPE);

        $return = $imageManager->createFromData($data, $newFilepath);
        $this->assertTrue($return);

        $filesystem = new Filesystem();
        $this->assertTrue($filesystem->exists($newFilepath));
        $oldSize = @getimagesize($filepath);
        $newSize = @getimagesize($newFilepath);
        $this->assertTrue($oldSize[0] === $newSize[0]);
        $this->assertTrue(ImageManager::JPEG_MIME_TYPE === $newSize['mime']);
        $filesystem->remove($newFilepath);
    }

    public function testCreatePNGFromData()
    {
        $filename = 'cover-test-image-manager.png';
        $filepath = self::ASSETS_PATH.$filename;
        $newFilepath = self::ASSETS_PATH.'tmp.png';
        $data = file_get_contents($filepath);

        $imageManager = new ImageManager(self::PARAMETERS);
        $imageManager->setParameters(self::COVER_TYPE);

        $return = $imageManager->createFromData($data, $newFilepath);
        $this->assertTrue($return);

        $filesystem = new Filesystem();
        $this->assertTrue($filesystem->exists($newFilepath));
        $oldSize = @getimagesize($filepath);
        $newSize = @getimagesize($newFilepath);
        $this->assertTrue($oldSize[0] === $newSize[0]);
        $this->assertTrue(ImageManager::PNG_MIME_TYPE === $newSize['mime']);
        $filesystem->remove($newFilepath);
    }
}

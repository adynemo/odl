<?php

namespace ODL\Constant;

class EraStatus
{
    public const IN_PROGRESS = 1;
    public const PAUSED = 2;
    public const ENDED = 3;
    public const ABANDONED = 4;

    public const ALLOWED_STATUS = [
        self::IN_PROGRESS,
        self::PAUSED,
        self::ENDED,
        self::ABANDONED,
    ];
}

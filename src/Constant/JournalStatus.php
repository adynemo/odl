<?php

namespace ODL\Constant;

class JournalStatus
{
    public const CREATED = 'created';
    public const PENDING = 'pending';
    public const UPDATED = 'updated';
    public const REMOVED = 'removed';

    public const ALLOWED_STATUS = [
        self::CREATED,
        self::PENDING,
        self::UPDATED,
        self::REMOVED,
    ];
}

<?php

namespace ODL\Twig;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Arc;
use ODL\Entity\Era;
use ODL\Entity\Period;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ArcExtension extends AbstractExtension
{
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Arc::class);
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('countArcsByPeriod', [$this, 'countArcsByPeriod']),
            new TwigFilter('countArcsByEra', [$this, 'countArcsByEra']),
            new TwigFilter('sortEraArcs', [$this, 'sortEraArcs']),
            new TwigFilter('sortAllArcs', [$this, 'sortAllArcs']),
        ];
    }

    public function countArcsByPeriod(Period $period): int
    {
        return $this->repository->countByPeriod($period);
    }

    public function countArcsByEra(Era $era): int
    {
        return $this->repository->countByEra($era->getPeriods()->toArray());
    }

    /**
     * @param array<Arc> $arcs
     *
     * @return array<Arc>
     */
    public function sortEraArcs(array $arcs): array
    {
        usort($arcs, function ($a, $b) {
            return $a->getPosition() <=> $b->getPosition();
        });
        usort($arcs, function ($a, $b) {
            return $a->getPeriod()->getPosition() <=> $b->getPeriod()->getPosition();
        });

        return $arcs;
    }

    /**
     * @param array<Arc> $arcs
     *
     * @return array<Arc>
     */
    public function sortAllArcs(array $arcs): array
    {
        usort($arcs, function ($a, $b) {
            return $a->getPeriod()->getEra()->getPosition() <=> $b->getPeriod()->getEra()->getPosition();
        });
        usort($arcs, function ($a, $b) {
            return $a->getPeriod()->getEra()->getUniverse()->getPosition() <=> $b->getPeriod()->getEra()->getUniverse()->getPosition();
        });

        return $arcs;
    }
}

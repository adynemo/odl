<?php

namespace ODL\Twig;

use ODL\Entity\Era;
use ODL\Traits\IsGrantedEditorTrait;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SecurityExtension extends AbstractExtension
{
    use IsGrantedEditorTrait;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $securityChecker;

    public function __construct(AuthorizationCheckerInterface $securityChecker)
    {
        $this->securityChecker = $securityChecker;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('isGrantedEditor', [$this, 'isGrantedEditorForEra']),
        ];
    }

    public function isGrantedEditorForEra(Era $era): bool
    {
        return $this->isGrantedEditor($era);
    }

    private function isGranted($role, $object = null): bool
    {
        try {
            return $this->securityChecker->isGranted($role, $object);
        } catch (AuthenticationCredentialsNotFoundException $e) {
            return false;
        }
    }
}

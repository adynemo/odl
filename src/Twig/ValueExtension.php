<?php

namespace ODL\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ValueExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('unescape', [$this, 'unescape']),
            new TwigFilter('bool', [$this, 'bool']),
            new TwigFilter('json_decode', [$this, 'jsonDecode']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('percent', [$this, 'computePercent']),
        ];
    }

    public function unescape(string $value): string
    {
        return html_entity_decode($value, ENT_QUOTES);
    }

    public function bool(?string $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function jsonDecode(string $value)
    {
        return json_decode($value);
    }

    public function computePercent(int $value, int $total): float
    {
        return $value ? ($value * 100) / $total : 0;
    }
}

<?php

namespace ODL\Twig;

use ODL\Service\URLService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class HistoryExtension extends AbstractExtension
{
    /**
     * @var URLService
     */
    private $URLService;

    public function __construct(URLService $URLService)
    {
        $this->URLService = $URLService;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('referrer', [$this->URLService, 'getReferrer']),
        ];
    }
}

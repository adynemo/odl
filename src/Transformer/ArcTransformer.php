<?php

namespace ODL\Transformer;

use Doctrine\ORM\EntityManagerInterface;
use ODL\DTO;
use ODL\Entity\Arc;
use ODL\Entity\Era;
use ODL\Entity\Period;
use Symfony\Component\Serializer\SerializerInterface;

class ArcTransformer
{
    private $entityManager;
    private $repository;
    private $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Arc::class);
        $this->serializer = $serializer;
    }

    public function transform(DTO\Arc $object, Era $era): Arc
    {
        $period = $this->entityManager->getRepository(Period::class)->findLastChildByParent($era);
        if (null === $period) {
            throw new \Exception('No period exists in this era. Arc cannot be created');
        }

        $arc = $this->serializer->deserialize(json_encode($object), Arc::class, 'json');
        $arc->setPeriod($period);

        $lastArc = $this->repository->findLastChildByParent($period);
        $position = 1;
        if (null !== $lastArc) {
            $position = $lastArc->getPosition() + 1;
        }

        $arc->setPosition($position);
        $arc->setCover('');
        $arc->setIsValidated(false);

        return $arc;
    }
}

<?php

namespace ODL\Form\Traits;

use Symfony\Component\Validator\Constraints\File;

trait FileConstraintTrait
{
    private function getFileConstraint(string $maxSize): File
    {
        return new File([
            'maxSize' => $maxSize,
            'mimeTypes' => [
                'image/jpeg',
                'image/png',
            ],
            'mimeTypesMessage' => $this->translator->trans('form.mime_types_message', [], 'general'),
        ]);
    }
}

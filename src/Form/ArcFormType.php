<?php

namespace ODL\Form;

use ODL\Entity\Arc;
use ODL\Entity\Period;
use ODL\Form\Traits\FileConstraintTrait;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ArcFormType extends AbstractType
{
    use FileConstraintTrait;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', null, [
                'label' => 'form.position',
                'required' => false,
                'attr' => [
                    'min' => 1,
                    'data-content' => $this->translator->trans('form.position_info', [], 'add_arc'),
                ],
            ])
            ->add('title', null, [
                'label' => 'form.title',
                'empty_data' => '',
            ])
            ->add('cover', FileType::class, [
                'label' => 'form.cover',
                'mapped' => false,
                'required' => true,
                'constraints' => [$this->getFileConstraint('5000k')],
                'help' => $this->translator->trans('form.cover_help', [], 'add_arc'),
            ])
            ->add('content', null, [
                'label' => 'form.desc',
                'empty_data' => '',
            ])
            ->add('linkA', UrlType::class, [
                'label' => 'form.first_link',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('linkB', UrlType::class, [
                'label' => 'form.second_link',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('isEvent', CheckboxType::class, [
                'label' => 'form.is_event',
                'required' => false,
            ])
            ->add('period', EntityType::class, [
                'label' => 'form.period',
                'class' => Period::class,
                'choice_label' => 'name',
                'group_by' => function ($period) {
                    return "{$period->getEra()->getUniverse()->getName()} / {$period->getEra()->getName()}";
                },
                'placeholder' => 'form.period_default',
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('form.save', [], 'general'),
                'attr' => ['class' => 'ui green button'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Arc::class,
            'translation_domain' => 'add_arc',
        ]);
    }
}

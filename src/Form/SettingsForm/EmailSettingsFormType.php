<?php

namespace ODL\Form\SettingsForm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class EmailSettingsFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('admin_sender_email', EmailType::class, [
                'label' => 'email.email_label',
                'help' => $this->translator->trans('email.email_help', [], 'settings'),
            ])
            ->add('admin_sender_name', TextType::class, [
                'label' => 'email.name_label',
                'required' => false,
                'empty_data' => '',
                'help' => $this->translator->trans('email.name_help', [], 'settings'),
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('form.save', [], 'general'),
                'attr' => ['class' => 'ui green button'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'settings',
        ]);
    }
}

<?php

namespace ODL\Form\SettingsForm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class MaintenanceFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('active', CheckboxType::class, [
                'label' => 'maintenance.label',
                'required' => false,
                'help' => $this->translator->trans('maintenance.help', [], 'settings'),
            ])
            ->add('ttl', NumberType::class, [
                'label' => 'maintenance.time_label',
                'required' => false,
                'html5' => true,
                'input' => 'string',
                'scale' => 0,
                'attr' => ['min' => 0],
                'help' => $this->translator->trans('maintenance.time_help', [], 'settings'),
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('form.save', [], 'general'),
                'attr' => ['class' => 'ui green button'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'settings',
        ]);
    }
}

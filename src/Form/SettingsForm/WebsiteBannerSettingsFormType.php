<?php

namespace ODL\Form\SettingsForm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

class WebsiteBannerSettingsFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('website_banner', FileType::class, [
                'label' => 'banner.label',
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '2000k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                        ],
                        'mimeTypesMessage' => $this->translator->trans('form.mime_types_message', [], 'general'),
                    ]),
                ],
                'help' => $this->translator->trans('banner.help', [], 'settings'),
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('form.save', [], 'general'),
                'attr' => ['class' => 'ui green button'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'settings',
        ]);
    }
}

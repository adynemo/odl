<?php

namespace ODL\Form\UserForm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class DeleteAccountFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', PasswordType::class, [
                'label' => 'password',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => $this->translator->trans('password_message', [], 'user_profile'),
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => $this->translator->trans('password_length', [], 'user_profile'),
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('confirm', CheckboxType::class, [
                'label' => 'remove_account.confirm_removal',
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('remove_account.submit', [], 'user_profile'),
                'attr' => ['class' => 'ui red button'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'user_profile',
        ]);
    }
}

<?php

namespace ODL\Form\UserForm;

use ODL\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserDataFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, [
                'label' => 'username',
            ])
            ->add('email', EmailType::class, [
                'label' => 'email',
            ])
            ->add('avatar_upload', FileType::class, [
                'label' => $this->translator->trans('form.browse', [], 'general'),
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => $this->translator->trans('form.mime_types_message', [], 'general'),
                    ]),
                ],
            ])
            ->add('avatar_base64', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('locale', ChoiceType::class, [
                'label' => 'language_choices.label',
                'required' => false,
                'choices' => [
                    'language_choices.app_default_locale' => '',
                    'language_choices.english' => 'en',
                    'language_choices.french' => 'fr',
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('form.save', [], 'general'),
                'attr' => ['class' => 'ui green button'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'user_profile',
        ]);
    }
}

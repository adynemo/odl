<?php

namespace ODL\Form\UserForm;

use ODL\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserPasswordFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plainActualPassword', PasswordType::class, $this->getPasswordOptions('actual_password'))
            ->add('plainNewPassword', PasswordType::class, $this->getPasswordOptions('new_password'))
            ->add('plainNewPasswordRetry', PasswordType::class, $this->getPasswordOptions('confirm_new_password'))
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('form.save', [], 'general'),
                'attr' => ['class' => 'ui green button'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'user_profile',
        ]);
    }

    private function getPasswordOptions(string $label): array
    {
        return [
            'label' => $label,
            'mapped' => false,
            'required' => false,
            'constraints' => [
                new NotBlank([
                    'message' => $this->translator->trans('password_message', [], 'user_profile'),
                ]),
                new Length([
                    'min' => 6,
                    'minMessage' => $this->translator->trans('password_length', [], 'user_profile'),
                    'max' => 4096,
                ]),
            ],
        ];
    }
}

<?php

namespace ODL\Form\UserForm;

use ODL\Entity\Era;
use ODL\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserRolesFormType extends AbstractType
{
    private TranslatorInterface $translator;
    private array $rolesHierarchy;

    public function __construct(TranslatorInterface $translator, array $rolesHierarchy)
    {
        $this->rolesHierarchy = $rolesHierarchy;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('roles', ChoiceType::class, [
            'label' => 'roles',
            'choices' => $this->getChoicesForRolesHierarchy(),
            'multiple' => true,
        ])
            ->add('editableEras', EntityType::class, [
                'label' => 'editable_eras',
                'multiple' => true,
                'required' => false,
                'class' => Era::class,
                'choice_label' => 'name',
                'group_by' => function ($era) {
                    return $era->getUniverse()->getName();
                },
                'help' => $this->translator->trans('editable_eras_help', [], 'user_profile'),
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('form.save', [], 'general'),
                'attr' => ['class' => 'ui green button'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'user_profile',
        ]);
    }

    private function getChoicesForRolesHierarchy(): array
    {
        $roles = ['ROLE_USER' => 'ROLE_USER'];

        foreach ($this->rolesHierarchy as $role => $children) {
            $roles[$role] = $role;
        }

        return $roles;
    }
}

<?php

namespace ODL\Form\Section;

use ODL\Entity\Universe;
use ODL\Form\Traits\FileConstraintTrait;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UniverseFormType extends AbstractSectionType
{
    use FileConstraintTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('logo', FileType::class, [
                'label' => 'universe.logo',
                'mapped' => false,
                'required' => false,
                'constraints' => [$this->getFileConstraint('250k')],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('data_class', Universe::class);
    }
}

<?php

namespace ODL\Form\Section;

use ODL\Entity\Period;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PeriodFormType extends AbstractSectionType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('data_class', Period::class);
    }
}

<?php

namespace ODL\Form\Section;

use ODL\DataCollector\Collection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $collection = $options['data'];
        $className = str_replace('ODL\\Entity\\', '', $collection->getEntityName());
        $formTypeClass = sprintf('ODL\\Form\\Section\\%sFormType', $className);

        $builder
            ->add('collection', CollectionType::class, [
                'entry_type' => $formTypeClass,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'form.save',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Collection::class,
            'translation_domain' => 'general',
        ]);
    }
}

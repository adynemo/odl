<?php

namespace ODL\Form\Section;

use ODL\Constant\EraStatus;
use ODL\Entity\Era;
use ODL\Form\Traits\FileConstraintTrait;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EraFormType extends AbstractSectionType
{
    use FileConstraintTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('status', ChoiceType::class, [
                'label' => 'era.status',
                'choices' => [
                    'status.in_progress' => EraStatus::IN_PROGRESS,
                    'status.paused' => EraStatus::PAUSED,
                    'status.ended' => EraStatus::ENDED,
                    'status.abandoned' => EraStatus::ABANDONED,
                ],
                'attr' => ['class' => 'ui fluid dropdown'],
            ])
            ->add('image', FileType::class, [
                'label' => 'era.banner_label',
                'mapped' => false,
                'required' => false,
                'constraints' => [$this->getFileConstraint('2000k')],
            ])
            ->add('logoLinkA', FileType::class, [
                'label' => 'era.first_link',
                'mapped' => false,
                'required' => false,
                'constraints' => [$this->getFileConstraint('250k')],
            ])
            ->add('logoLinkB', FileType::class, [
                'label' => 'era.second_link',
                'mapped' => false,
                'required' => false,
                'constraints' => [$this->getFileConstraint('250k')],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('data_class', Era::class);
    }
}

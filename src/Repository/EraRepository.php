<?php

namespace ODL\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use ODL\Entity\Era;
use ODL\Entity\Universe;

/**
 * @method Era|null find($id, $lockMode = null, $lockVersion = null)
 * @method Era|null findOneBy(array $criteria, array $orderBy = null)
 * @method Era[]    findAll()
 * @method Era[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Era::class);
    }

    public function findLastChildByParent(Universe $universe): ?Era
    {
        return $this->createQueryBuilder('e')
            ->where('e.universe = :universe')
            ->setParameter('universe', $universe)
            ->orderBy('e.position', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByUniverseAndEraSlugs(string $uSlug, string $eSlug): ?Era
    {
        return $this->createQueryBuilder('e')
            ->innerJoin('e.universe', 'u', Join::WITH, 'u.slug = :uSlug')
            ->where('e.slug = :eSlug')
            ->setParameters([
                'eSlug' => $eSlug,
                'uSlug' => $uSlug,
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}

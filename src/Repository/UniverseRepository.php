<?php

namespace ODL\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use ODL\Entity\Universe;

/**
 * @method Universe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Universe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Universe[]    findAll()
 * @method Universe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UniverseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Universe::class);
    }

    public function findLastUniverse(): ?Universe
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.position', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllSortedByPosition(): array
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.position', 'ASC')
            ->getQuery()
            ->getResult();
    }
}

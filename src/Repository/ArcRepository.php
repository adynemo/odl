<?php

namespace ODL\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use ODL\Entity\Arc;
use ODL\Entity\Period;

/**
 * @method Arc|null find($id, $lockMode = null, $lockVersion = null)
 * @method Arc|null findOneBy(array $criteria, array $orderBy = null)
 * @method Arc[]    findAll()
 * @method Arc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArcRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Arc::class);
    }

    public function findLastChildByParent(Period $period): ?Arc
    {
        return $this->createQueryBuilder('p')
            ->where('p.period = :period')
            ->setParameter('period', $period)
            ->orderBy('p.position', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return array<Arc>
     */
    public function findArcsFromPeriodBetweenPositions(Period $period, int $first, int $last): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.period = :period')
            ->andWhere('a.position BETWEEN :first AND :last')
            ->setParameter('period', $period)
            ->setParameter('first', $first)
            ->setParameter('last', $last)
            ->orderBy('a.position', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array<Arc>
     */
    public function findArcsByQuery(string $query, array $periodIds = []): array
    {
        $q = $this->createQueryBuilder('a')
            ->where('a.title LIKE :query')
            ->orWhere('a.content LIKE :query')
            ->andWhere('a.isValidated = true')
            ->setParameter('query', '%'.$query.'%');

        if (!empty($periodIds)) {
            $q->andWhere('a.period IN (:periodIds)')
                ->setParameter('periodIds', $periodIds);
        }

        return $q->getQuery()
            ->getResult();
    }

    /**
     * @return array<Arc>
     */
    public function findByPeriodWithLimit(Period $period, int $offset, int $limit): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.period = :period')
            ->andWhere('a.isValidated = true')
            ->setParameter('period', $period)
            ->orderBy('a.position', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array<Arc>
     */
    public function findToValidateWithLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.isValidated = false')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array<int> $ids
     */
    public function validateArcs(array $ids): int
    {
        return $this->createQueryBuilder('a')
            ->update(Arc::class, 'a')
            ->where('a.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->set('a.isValidated', true)
            ->getQuery()
            ->execute();
    }

    public function countByPeriod(Period $period, bool $isValidated = true): int
    {
        return $this->count(['period' => $period, 'isValidated' => $isValidated]);
    }

    /**
     * @param array<Period> $periods
     */
    public function countByEra(array $periods, bool $isValidated = true): int
    {
        $ids = array_map(function ($period) {
            return $period->getId();
        }, $periods);

        return $this->createQueryBuilder('a')
            ->select('COUNT(a.id)')
            ->where('a.period IN (:periods)')
            ->andWhere('a.isValidated = :isValidated')
            ->setParameter('isValidated', $isValidated)
            ->setParameter('periods', $ids)
            ->getQuery()
            ->getSingleScalarResult();
    }
}

<?php

namespace ODL\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use ODL\Entity\Era;
use ODL\Entity\Rate;
use ODL\Entity\User;

/**
 * @method Rate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rate[]    findAll()
 * @method Rate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rate::class);
    }

    public function findByEra(Era $era, ?User $user = null): array
    {
        $qb = $this->createQueryBuilder('r')
            ->join('r.arc', 'a')
            ->join('a.period', 'p')
            ->where('p.era = :era')
            ->setParameter('era', $era);

        if ($user) {
            $qb->andWhere('r.user = :user')
                ->setParameter('user', $user);
        }

        return $qb->getQuery()->getResult();
    }
}

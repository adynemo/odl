<?php

namespace ODL\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;
use ODL\Entity\Reading;
use ODL\Entity\User;

/**
 * @method Reading|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reading|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reading[]    findAll()
 * @method Reading[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReadingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reading::class);
    }

    public function findUserReadingsIdsByPeriods(User $user, Collection $periods): array
    {
        return $this->createQueryBuilder('r')
            ->select('a.id')
            ->innerJoin('r.arc', 'a')
            ->where('r.user = :user')
            ->andWhere('a.period IN (:periods)')
            ->setParameters(['user' => $user, 'periods' => $periods])
            ->orderBy('r.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function countUserReadingsByEra(User $user): array
    {
        return $this->createQueryBuilder('r')
            ->select('COUNT(a.id), IDENTITY(p.era)')
            ->innerJoin('r.arc', 'a')
            ->innerJoin('a.period', 'p')
            ->where('r.user = :user')
            ->setParameter('user', $user)
            ->groupBy('p.era')
            ->getQuery()
            ->getResult();
    }
}

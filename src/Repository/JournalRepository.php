<?php

namespace ODL\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use ODL\Entity\Journal;

/**
 * @method Journal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Journal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Journal[]    findAll()
 * @method Journal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JournalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Journal::class);
    }

    public function garbage(int $max)
    {
        if (0 < $max) {
            $ids = $this->createQueryBuilder('j')
                ->select('j.id')
                ->orderBy('j.createdAt', 'DESC')
                ->setMaxResults($max)
                ->getQuery()
                ->getResult();
            $this->createQueryBuilder('j')
                ->delete()
                ->where('j.id NOT IN (:ids)')
                ->setParameter('ids', $ids)
                ->getQuery()
                ->execute();
        }
    }

    public function countTotal(): int
    {
        return $this->createQueryBuilder('j')
            ->select('COUNT(j.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }
}

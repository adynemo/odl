<?php

namespace ODL\EventListener;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityListener
{
    private EntityManagerInterface $entityManager;
    private TranslatorInterface $translator;

    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        /**
         * @var User $user
         */
        $user = $event->getAuthenticationToken()->getUser();

        $transParam = ['username' => $user->getUsername()];
        $message = $user->getLastLogin()
            ? $this->translator->trans('welcome_back_message', $transParam, 'general', $user->getLocale())
            : $this->translator->trans('welcome_message', $transParam, 'general', $user->getLocale());

        $user->setLastLogin(new DateTime());

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        /**
         * @var Session $session
         */
        $session = $event->getRequest()->getSession();
        $session->getFlashBag()->add('info', $message);
    }
}

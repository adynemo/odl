<?php

namespace ODL\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\UnitOfWork;
use ODL\Entity\Section;

class SectionPositionUniqueConstraintListener
{
    public function prePersist(Section $section, LifecycleEventArgs $event): void
    {
        $section->disableUniqueKey();
    }

    public function preUpdate(Section $section, PreUpdateEventArgs $event): void
    {
        $section->disableUniqueKey();
    }

    public function postUpdate(Section $section, LifecycleEventArgs $event): void
    {
        $this->enableKey($section, $event->getEntityManager()->getUnitOfWork());
    }

    public function postPersist(Section $section, LifecycleEventArgs $event): void
    {
        $this->enableKey($section, $event->getEntityManager()->getUnitOfWork());
    }

    private function enableKey(Section $section, UnitOfWork $uow): void
    {
        $section->enableUniqueKey();

        $uow->scheduleExtraUpdate($section, [Section::ENABLED_CONSTRAINT_FIELD_NAME => [null, Section::ENABLED_CONSTRAINT_DB_VALUE]]);
    }
}

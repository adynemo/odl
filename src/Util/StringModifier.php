<?php

namespace ODL\Util;

class StringModifier
{
    public function generateSlug(string $name): string
    {
        $search = [' ', '(', ')', '/', '\\'];

        return strtolower(str_replace($search, '-', $name));
    }

    public function generateRandomString(): string
    {
        return md5(uniqid(rand(), true));
    }
}

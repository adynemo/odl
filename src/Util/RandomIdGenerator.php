<?php

namespace ODL\Util;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class RandomIdGenerator extends AbstractIdGenerator
{
    public function generate(EntityManager $em, $entity): ?string
    {
        $entity_name = $em->getClassMetadata(get_class($entity))->getName();

        $id = $entity->getId() ?: uniqid();
        $item = $em->find($entity_name, $id);

        // Look in scheduled entity insertions (persisted queue list), too
        if (!$item) {
            $persisted = $em->getUnitOfWork()->getScheduledEntityInsertions();
            $ids = array_map(function ($o) {
                return $o->getId();
            }, $persisted);
            $item = array_search($id, $ids);
        }

        return !$item ? $id : null;
    }
}

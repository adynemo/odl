<?php

namespace ODL\Controller;

use DateTime;
use ODL\Entity\User;
use ODL\Form\RegistrationFormType;
use ODL\Service\MailerManager;
use ODL\Service\UserManager;
use ODL\Traits\MessageFlashTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends AbstractController
{
    use MessageFlashTrait;

    /**
     * @Route("/register", name="odl_register")
     */
    public function register(
        Request $request,
        UserPasswordHasherInterface $passwordEncoder,
        UserManager $userManager,
        MailerManager $mailer,
        TranslatorInterface $translator
    ): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setLocale($request->getDefaultLocale());
            $userManager->setActivationFields($user);
            $userManager->create($user, $form->get('plainPassword')->getData(), $passwordEncoder);

            $params = ['token' => $user->getActivationToken(), 'email' => $user->getEmail()];
            $url = $this->generateUrl('odl_register_activate', $params, UrlGeneratorInterface::ABSOLUTE_URL);
            $mailer->sendRegistrationEmail($user->getEmail(), [
                'user' => $user,
                'url' => $url,
                'website' => $this->getParameter('app.name'),
            ]);

            $this->success($translator->trans('self_creation.success', ['%username%' => $user->getUsername()], 'user_profile'));
            $this->addFlashMessage();

            return $this->redirectToRoute('odl_home_page');
        }

        return $this->render('registration/register.html.twig', ['registrationForm' => $form->createView()]);
    }

    /**
     * @Route("/register/activate/{token}", name="odl_register_activate")
     */
    public function activate(string $token, Request $request, TranslatorInterface $translator): RedirectResponse
    {
        $email = $request->query->get('email');
        if (false === filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new NotFoundHttpException();
        }

        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(User::class);
        $user = $repository->findOneBy(['email' => $email, 'activationToken' => $token]);

        if (null === $user) {
            throw new NotFoundHttpException();
        }

        $transId = 'self_creation.already_activated';
        if (null === $user->getActivationDate()) {
            $user->setActivationDate(new DateTime());
            $doctrine->getManager()->flush();
            $transId = 'self_creation.activation_success';
        }

        $this->success($translator->trans($transId, ['%username%' => $user->getUsername()], 'user_profile'));
        $this->addFlashMessage();

        return $this->redirectToRoute('odl_security_login');
    }
}

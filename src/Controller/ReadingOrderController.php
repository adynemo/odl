<?php

namespace ODL\Controller;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Era;
use ODL\Entity\Universe;
use ODL\Repository\ReadingRepository;
use ODL\Service\UniverseManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class ReadingOrderController extends AbstractController
{
    /**
     * @Route(name="odl_home_page")
     */
    public function home(UniverseManager $universeManager): Response
    {
        $universes = $universeManager->findAll()->getCollection();

        return $this->render('home.html.twig', ['universes' => $universes]);
    }

    /**
     * @Route("reading-order/{slug}", name="odl_universes_list")
     */
    public function universesList(): RedirectResponse
    {
        return $this->redirectToRoute('odl_home_page');
    }

    /**
     * @Route("reading-order/{universe}/{era}", name="odl_reading_order_page")
     * @Entity("era", expr="repository.findByUniverseAndEraSlugs(universe, era)")
     */
    public function readingOrderPage(Era $era, EntityManagerInterface $entityManager): Response
    {
        $universe = $entityManager->getRepository(Universe::class)->find($era->getUniverse());

        return $this->render('reading_order.html.twig', [
            'universe' => $universe,
            'era' => $era,
        ]);
    }

    /**
     * @Route("readings/era/{id}", name="odl_readings_by_era",
     * methods={"GET"},
     * options={"expose"=true})
     */
    public function getReadingsByEra(Era $era, ReadingRepository $repository): JsonResponse
    {
        $readings = [];
        $user = $this->getUser();

        if (null !== $user) {
            $results = $repository->findUserReadingsIdsByPeriods($user, $era->getPeriods());

            foreach ($results as $result) {
                $readings[] = $result['id'];
            }
        }

        return new JsonResponse($readings);
    }
}

<?php

namespace ODL\Controller;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Arc;
use ODL\Form\ArcFormType;
use ODL\Service\ArcManager;
use ODL\Traits\IsGrantedEditorTrait;
use ODL\Traits\MessageFlashTrait;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/arc")
 */
class ArcController extends AbstractController
{
    use MessageFlashTrait;
    use IsGrantedEditorTrait;

    private $request;
    private $translator;
    private $logger;

    public function __construct(
        RequestStack $request,
        TranslatorInterface $translator,
        LoggerInterface $logger
    ) {
        $this->request = $request->getCurrentRequest();
        $this->translator = $translator;
        $this->logger = $logger;
    }

    /**
     * @Route("/create", name="odl_admin_add_arc")
     * @IsGranted("ROLE_CONTRIBUTOR")
     */
    public function createArc(ArcManager $arcManager)
    {
        $form = $this->createForm(ArcFormType::class);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $arc = $form->getData();
            $era = $arc->getPeriod()->getEra();
            $arc->setIsValidated($this->isGrantedEditor($era));
            $cover = $this->request->files->get('arc_form')['cover'];

            try {
                $arcManager->create($arc, $cover);
                $this->success($this->translator->trans('form.confirm_creation', ['%title%' => $arc->getTitle()], 'add_arc'));
                $this->addFlashMessage();

                return $this->redirectToRoute('odl_admin_add_arc');
            } catch (\Throwable $exception) {
                $this->logger->error('Arc creation fails', ['exception' => $exception]);
                $this->error($this->translator->trans('general_error', [], 'general'));
                $this->addFlashMessage();
            }
        }

        return $this->render('admin/arc/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/moderation/list", name="odl_admin_arc_moderation_list", options = { "expose" = true })
     * @IsGranted("ROLE_EDITOR")
     */
    public function listArcsToValidate(EntityManagerInterface $entityManager)
    {
        $repository = $entityManager->getRepository(Arc::class);
        $arcs = $repository->findBy(['isValidated' => false]);
        $count = count($arcs);
        if (20 < $count) {
            $arcs = array_slice($arcs, 0, 20);
        }

        return $this->render('admin/arc/moderation_list.html.twig', [
            'arcs' => $arcs,
            'arcsLength' => $count,
        ]);
    }
}

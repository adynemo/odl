<?php

namespace ODL\Controller;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\User;
use ODL\Event\UserLocaleEvent;
use ODL\Form\UserForm\DeleteAccountFormType;
use ODL\Form\UserForm\UserDataFormType;
use ODL\Form\UserForm\UserPasswordFormType;
use ODL\Form\UserForm\UserRolesFormType;
use ODL\Repository\ReadingRepository;
use ODL\Repository\UniverseRepository;
use ODL\Service\UserManager;
use ODL\Traits\MessageFlashTrait;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

/**
 * @Route("/user")
 * @IsGranted("ROLE_USER")
 */
class UserController extends AbstractController
{
    use MessageFlashTrait;

    /**
     * @Route("/", name="odl_user_home")
     */
    public function index()
    {
        return $this->render('admin/home.html.twig');
    }

    /**
     * @Route("/profile", name="odl_user_profile")
     * @Route("/edit-user/{id}", name="odl_admin_edit_user")
     */
    public function profile(
        EntityManagerInterface $em,
        UserPasswordHasherInterface $passwordEncoder,
        UserManager $userManager,
        TokenStorageInterface $tokenStorage,
        Request $request,
        TranslatorInterface $translator,
        LoggerInterface $logger,
        EventDispatcherInterface $dispatcher,
        User $user = null
    ): Response {
        $currentRoute = $request->get('_route');
        if (null === $user) {
            /** @var User $user */
            $user = $this->getUser();
            $currentRouteParam = [];
        } else {
            $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');
            $currentRouteParam = ['id' => $user->getId()];
        }

        $formUserData = $this->createForm(UserDataFormType::class, $user);
        $formUserData->handleRequest($request);
        $formUserRoles = $this->createForm(UserRolesFormType::class, $user);
        $formUserRoles->handleRequest($request);
        $formUserPassword = $this->createForm(UserPasswordFormType::class, $user);
        $formUserPassword->handleRequest($request);
        $formDeleteAccount = $this->createForm(DeleteAccountFormType::class);
        $formDeleteAccount->handleRequest($request);

        if ($formUserPassword->isSubmitted() && $formUserPassword->isValid()) {
            $newPassword = $formUserPassword->get('plainNewPassword')->getData();
            $newPasswordRetry = $formUserPassword->get('plainNewPasswordRetry')->getData();
            $isOlderPasswordValid = $passwordEncoder->isPasswordValid($user, $formUserPassword->get('plainActualPassword')->getData());

            if (!$isOlderPasswordValid || ($newPassword !== $newPasswordRetry)) {
                $this->error($translator->trans('reset_password.error', [], 'security'));
                $this->addFlashMessage();

                return $this->redirectToRoute($currentRoute, $currentRouteParam);
            }

            $user->setPassword($passwordEncoder->hashPassword($user, $newPassword));

            $em->persist($user);
            $em->flush();

            $this->success($translator->trans('reset_password.success', [], 'security'));
            $this->addFlashMessage();

            return $this->redirectToRoute($currentRoute, $currentRouteParam);
        }

        if ($formUserData->isSubmitted() && $formUserData->isValid()) {
            $avatar64 = $formUserData->get('avatar_base64')->getData() ?: '';
            try {
                $userManager->update($user, $avatar64);
                $dispatcher->dispatch(new UserLocaleEvent($user, $request), UserLocaleEvent::UPDATED);
                $this->success($translator->trans('form.confirm_modification', [], 'general'));
            } catch (Throwable $exception) {
                $this->error($translator->trans('general_error', [], 'general'));
                $logger->error('User update fails', ['exception' => $exception]);
            }
            $this->addFlashMessage();

            return $this->redirectToRoute($currentRoute, $currentRouteParam);
        }

        if ($formUserRoles->isSubmitted() && $formUserRoles->isValid()) {
            $em->persist($user);
            $em->flush();
            $this->success($translator->trans('form.confirm_modification', [], 'general'));
            $this->addFlashMessage();

            return $this->redirectToRoute($currentRoute, $currentRouteParam);
        }

        if ($formDeleteAccount->isSubmitted() && $formDeleteAccount->isValid()) {
            $isPasswordValid = $passwordEncoder->isPasswordValid($user, $formDeleteAccount->get('password')->getData());
            if (true === $isPasswordValid) {
                $userManager->remove($user);

                $tokenStorage->setToken();
                $request->getSession()->invalidate();

                $this->success($translator->trans('remove_account.user_deleted', ['%username%' => $user->getUsername()], 'user_profile'));
                $this->addFlashMessage();
            }

            return $this->redirectToRoute('odl_home_page');
        }

        return $this->render('admin/profile.html.twig', [
            'user' => $user,
            'formUserData' => $formUserData->createView(),
            'formUserRoles' => $formUserRoles->createView(),
            'formUserPassword' => $formUserPassword->createView(),
            'formDeleteAccount' => $formDeleteAccount->createView(),
        ]);
    }

    /**
     * @Route("/reading-progress", name="odl_user_reading_progress")
     */
    public function readingProgression(UniverseRepository $universeRepository, ReadingRepository $readingRepository): Response
    {
        $user = $this->getUser();

        $universes = $universeRepository->findAll();
        $results = $readingRepository->countUserReadingsByEra($user);
        $readings = [];

        foreach ($results as $result) {
            $readings[$result[2]] = $result[1];
        }

        return $this->render('user/reading-progress.html.twig', [
            'universes' => $universes,
            'readings' => $readings,
        ]);
    }
}

<?php

namespace ODL\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use ODL\Entity\Era;
use ODL\Entity\Universe;
use ODL\Exception\BadPositionException;
use ODL\Form\Section\CollectionFormType;
use ODL\Service\EraManager;
use ODL\Service\PeriodManager;
use ODL\Service\UniverseManager;
use ODL\Traits\MessageFlashTrait;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

/**
 * @Route("/admin")
 */
class AdminReadingOrderController extends AbstractController
{
    use MessageFlashTrait;

    private $request;
    private $translator;
    private $logger;

    public function __construct(
        RequestStack $request,
        TranslatorInterface $translator,
        LoggerInterface $logger
    ) {
        $this->request = $request->getCurrentRequest();
        $this->translator = $translator;
        $this->logger = $logger;
    }

    /**
     * @Route("/manage/universes", name="odl_admin_manage_universe")
     * @IsGranted("ROLE_ADMIN")
     */
    public function manageUniverses(UniverseManager $universeManager)
    {
        $universes = $universeManager->findAll();
        $form = $this->createForm(CollectionFormType::class, $universes);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logos = $this->request->files->get('collection_form')['collection'];

            try {
                $universeManager->upsert($universes, $logos);
                $this->success($this->translator->trans('form.confirm_modification', [], 'general'));
                $this->addFlashMessage();

                return $this->redirectToRoute('odl_admin_manage_universe');
            } catch (BadPositionException $exception) {
                $this->error($this->translator->trans('bad_position', [], 'manage_sections'));
            } catch (UniqueConstraintViolationException $exception) {
                $this->error($this->translator->trans('duplicated_position', [], 'manage_sections'));
            } catch (Throwable $exception) {
                $this->logger->error('Upsert universes fails!', ['universes' => $universes, 'exception' => $exception->getMessage()]);
                $this->error($this->translator->trans('general_error', [], 'general'));
            }

            $this->addFlashMessage();
        }

        return $this->render('admin/manage_sections.html.twig', [
            'form' => $form->createView(),
            'entityTypeClean' => 'universe',
            'childEntityType' => 'era',
            'backButton' => $this->generateUrl('odl_admin_home'),
        ]);
    }

    /**
     * @Route("/manage/universe/{id}/eras", name="odl_admin_manage_era",
     * options = { "expose" = true })
     * @IsGranted("ROLE_ADMIN")
     */
    public function manageEras(Universe $universe, EraManager $eraManager)
    {
        $eras = $eraManager->findBy(['universe' => $universe]);
        $form = $this->createForm(CollectionFormType::class, $eras);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $images = $this->request->files->get('collection_form')['collection'];

            try {
                $eraManager->upsert($eras, $images, $universe);
                $this->success($this->translator->trans('form.confirm_modification', [], 'general'));
                $this->addFlashMessage();

                return $this->redirectToRoute('odl_admin_manage_era', ['id' => $universe->getId()]);
            } catch (BadPositionException $exception) {
                $this->error($this->translator->trans('bad_position', [], 'manage_sections'));
            } catch (UniqueConstraintViolationException $exception) {
                $this->error($this->translator->trans('duplicated_position', [], 'manage_sections'));
            } catch (Throwable $exception) {
                $this->error($this->translator->trans('general_error', [], 'general'));
                $this->logger->error('Upsert eras fails!', ['eras' => $eras, 'exception' => $exception]);
            }

            $this->addFlashMessage();
        }

        return $this->render('admin/manage_sections.html.twig', [
            'form' => $form->createView(),
            'entityTypeClean' => 'era',
            'parentEntity' => $universe,
            'childEntityType' => 'period',
            'backButton' => $this->generateUrl('odl_admin_manage_universe'),
        ]);
    }

    /**
     * @Route("/manage/era/{id}/periods", name="odl_admin_manage_period",
     * options = { "expose" = true })
     * @IsGranted("ROLE_ADMIN")
     */
    public function managePeriods(Era $era, PeriodManager $periodManager)
    {
        $periods = $periodManager->findBy(['era' => $era]);
        $form = $this->createForm(CollectionFormType::class, $periods);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $periodManager->upsert($periods, $era);
                $this->success($this->translator->trans('form.confirm_modification', [], 'general'));
                $this->addFlashMessage();

                return $this->redirectToRoute('odl_admin_manage_period', ['id' => $era->getId()]);
            } catch (BadPositionException $exception) {
                $this->error($this->translator->trans('bad_position', [], 'manage_sections'));
            } catch (UniqueConstraintViolationException $exception) {
                $this->error($this->translator->trans('duplicated_position', [], 'manage_sections'));
            } catch (Throwable $exception) {
                $this->error($this->translator->trans('general_error', [], 'general'));
                $this->logger->error('Upsert periods fails!', ['periods' => $periods, 'exception' => $exception]);
            }

            $this->addFlashMessage();
        }

        return $this->render('admin/manage_sections.html.twig', [
            'form' => $form->createView(),
            'entityTypeClean' => 'period',
            'parentEntity' => $era,
        ]);
    }
}

<?php

namespace ODL\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use ODL\Entity\User;
use ODL\Form\ForgottenPasswordFormType;
use ODL\Form\UserForm\UserPasswordFormType;
use ODL\Service\ForgottenPasswordManager;
use ODL\Service\MailerManager;
use ODL\Service\UserManager;
use ODL\Traits\MessageFlashTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/security")
 */
class SecurityController extends AbstractController
{
    use MessageFlashTrait;

    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/login", name="odl_security_login",
     * options = { "expose" = true })
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('odl_home_page');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="odl_security_logout")
     */
    public function logout()
    {
        throw new Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/forgotten-password", name="odl_security_forgotten_password")
     */
    public function forgottenPassword(
        Request $request,
        UserManager $userManager,
        ForgottenPasswordManager $forgottenPasswordManager,
        MailerManager $mailer
    ) {
        $form = $this->createForm(ForgottenPasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $user = $userManager->findOneBy(['email' => $email]);
            $this->warning($this->translator->trans('forgotten_password.no_account', [], 'security'));

            if ($user instanceof User) {
                $token = $forgottenPasswordManager->getForgottenPasswordToken($user);

                $mailer->sendForgottenPasswordEmail($email, [
                    'user' => $user,
                    'url' => $this->generateUrl('odl_reset_password', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL),
                ]);

                $this->info($this->translator->trans('forgotten_password.confirmation_message', [], 'security'));
            }

            $this->addFlashMessage();

            return $this->redirectToRoute('odl_security_forgotten_password');
        }

        return $this->render('security/forgotten-password.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/reset-password", name="odl_reset_password",
     * options = { "expose" = true })
     */
    public function resetPassword(
        Request $request,
        UserManager $userManager,
        ForgottenPasswordManager $forgottenPasswordManager,
        UserPasswordHasherInterface $passwordEncoder,
        EntityManagerInterface $entityManager
    ) {
        $token = $request->query->get('token');
        $user = $userManager->findOneBy(['forgottenPassword' => $token]);
        $twigArgs = [
            'message' => [
                'type' => 'error',
                'content' => $this->translator->trans('reset_password.invalid_link', [], 'security'),
            ],
        ];

        if (null === $token) {
            throw new NotFoundHttpException();
        }

        if ($user instanceof User) {
            $form = $this->createForm(UserPasswordFormType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                $newPassword = $form->get('plainNewPassword')->getData();
                $newPasswordRetry = $form->get('plainNewPasswordRetry')->getData();

                if ($newPassword == $newPasswordRetry) {
                    $user->setPassword($passwordEncoder->hashPassword($user, $newPassword));
                    $userManager->setActivationFields($user, true);

                    $entityManager->flush();

                    $this->addFlash(
                        'info',
                        $this->translator->trans('reset_password.success', [], 'security')
                    );

                    return $this->redirectToRoute('odl_home_page');
                }

                $this->addFlash(
                    'error',
                    $this->translator->trans('reset_password.error', [], 'security')
                );

                return $this->redirectToRoute('odl_reset_password');
            }

            $form->remove('plainActualPassword');
            $twigArgs = [
                'form' => $form->createView(),
            ];

            if (false === $forgottenPasswordManager->isValidDate($token)) {
                $twigArgs = [
                    'message' => [
                        'type' => 'error',
                        'content' => $this->translator->trans('reset_password.link_no_more_valid', [], 'security'),
                    ],
                ];
            }
        }

        return $this->render('security/reset-password.html.twig', $twigArgs);
    }
}

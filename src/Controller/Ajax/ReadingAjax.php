<?php

namespace ODL\Controller\Ajax;

use Doctrine\Persistence\ManagerRegistry;
use ODL\Entity\Arc;
use ODL\Entity\Reading;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ajax/reading")
 * @IsGranted("ROLE_USER")
 */
class ReadingAjax extends AbstractController
{
    /**
     * @Route("/read/{id}", name="odl_ajax_reading_read",
     * methods={"POST"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function read(Arc $arc, Request $request, ManagerRegistry $registry): JsonResponse
    {
        $token = $request->query->get('token');
        if (false === $this->isCsrfTokenValid('read-arc', $token)) {
            return new JsonResponse('Form is not valid', Response::HTTP_BAD_REQUEST);
        }

        $user = $this->getUser();
        $reading = $registry->getRepository(Reading::class)->findOneBy(['user' => $user, 'arc' => $arc]);

        if (null === $reading) {
            $reading = (new Reading())
                ->setUser($user)
                ->setArc($arc);

            $entityManager = $registry->getManager();
            $entityManager->persist($reading);
            $entityManager->flush();
        }

        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    /**
     * @Route("/unread/{id}", name="odl_ajax_reading_unread",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function unread(Arc $arc, Request $request, ManagerRegistry $registry): JsonResponse
    {
        $token = $request->query->get('token');
        if (false === $this->isCsrfTokenValid('unread-arc', $token)) {
            return new JsonResponse('Form is not valid', Response::HTTP_BAD_REQUEST);
        }

        $user = $this->getUser();
        $reading = $registry->getRepository(Reading::class)->findOneBy(['user' => $user, 'arc' => $arc]);

        if (null !== $reading) {
            $entityManager = $registry->getManager();
            $entityManager->remove($reading);
            $entityManager->flush();
        }

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }
}

<?php

namespace ODL\Controller\Ajax;

use ODL\Entity\Universe;
use ODL\Service\UniverseManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ajax/universe")
 */
class UniverseAjax extends AbstractController
{
    /**
     * @Route("/delete/{id}", name="odl_ajax_universe_delete_universe",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Universe $universe, Request $request, UniverseManager $universeManager): JsonResponse
    {
        $token = $request->request->get('token');

        if (true === $this->isCsrfTokenValid('remove-universe', $token)) {
            $universeManager->remove($universe);

            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
        }

        return new JsonResponse('Form is not valid', JsonResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/get/{id}", name="odl_ajax_universe_get_universe",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function getUniverse(Universe $universe): JsonResponse
    {
        return new JsonResponse($universe);
    }
}

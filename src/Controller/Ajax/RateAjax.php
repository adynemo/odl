<?php

namespace ODL\Controller\Ajax;

use Doctrine\Persistence\ManagerRegistry;
use ODL\Entity\Arc;
use ODL\Entity\Era;
use ODL\Entity\Rate;
use ODL\Repository\RateRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ajax")
 */
class RateAjax extends AbstractController
{
    /**
     * @Route("/rate/{id}", name="odl_ajax_rate",
     * methods={"POST"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function rate(Arc $arc, Request $request, ManagerRegistry $registry): JsonResponse
    {
        $value = $request->request->getInt('rate');
        if (0 > $value || 5 < $value) {
            return new JsonResponse('You try to save an unavailable rate value', Response::HTTP_BAD_REQUEST);
        }

        $user = $this->getUser();
        $entityManager = $registry->getManager();
        $rate = $entityManager->getRepository(Rate::class)->findOneBy(['arc' => $arc, 'user' => $user]);

        if (null === $rate) {
            $rate = (new Rate())
                ->setArc($arc)
                ->setUser($this->getUser());
        }

        $rate->setValue($value);

        $entityManager->persist($rate);
        $entityManager->flush();

        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    /**
     * @Route("/rate/{id}", name="odl_ajax_unrate",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function unrate(Arc $arc, ManagerRegistry $registry): JsonResponse
    {
        $user = $this->getUser();
        if (null === $user) {
            throw new UnauthorizedHttpException('You need to be authenticated.');
        }
        $entityManager = $registry->getManager();
        $rate = $entityManager->getRepository(Rate::class)->findOneBy(['arc' => $arc, 'user' => $user]);

        if (null === $rate) {
            return new JsonResponse('Any rate is registered for this arc and this user', Response::HTTP_BAD_REQUEST);
        }

        $entityManager->remove($rate);
        $entityManager->flush();

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/era/{id}/user/rates", name="odl_ajax_era_user_rates",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options={"expose"=true})
     */
    public function getAllUserRatesInEra(Era $era, RateRepository $repository): JsonResponse
    {
        $user = $this->getUser();
        if (null === $user) {
            throw new UnauthorizedHttpException('You need to be authenticated.');
        }
        $rates = $repository->findByEra($era, $user);

        return new JsonResponse($rates, Response::HTTP_OK);
    }

    /**
     * @Route("/era/{id}/rates", name="odl_ajax_era_rates",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options={"expose"=true})
     */
    public function getAllRatesInEra(Era $era, RateRepository $repository): JsonResponse
    {
        $rates = $repository->findByEra($era);

        return new JsonResponse($rates, Response::HTTP_OK);
    }
}

<?php

namespace ODL\Controller\Ajax;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Period;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ajax/period")
 */
class PeriodAjax extends AbstractController
{
    /**
     * @Route("/delete/{id}", name="odl_ajax_period_delete_period",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Period $period, Request $request, EntityManagerInterface $entityManager)
    {
        $token = $request->request->get('token');

        if (true === $this->isCsrfTokenValid('remove-period', $token)) {
            $entityManager->remove($period);
            $entityManager->flush();

            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        }

        return new JsonResponse('Form is not valid', Response::HTTP_BAD_REQUEST);
    }
}

<?php

namespace ODL\Controller\Ajax;

use Exception;
use ODL\Entity\Arc;
use ODL\Entity\Period;
use ODL\Form\ArcFormType;
use ODL\Repository\ArcRepository;
use ODL\Service\ArcManager;
use ODL\Traits\IsGrantedEditorTrait;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

/**
 * @Route("/ajax/arc")
 */
class ArcAjax extends AbstractController
{
    use IsGrantedEditorTrait;

    private LoggerInterface $logger;
    private ArcManager $arcManager;

    public function __construct(
        ArcManager $arcManager,
        LoggerInterface $logger
    ) {
        $this->arcManager = $arcManager;
        $this->logger = $logger;
    }

    /**
     * @Route("/remove/{id}", name="odl_ajax_arc_remove",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function remove(Arc $arc, Request $request): JsonResponse
    {
        $era = $arc->getPeriod()->getEra();
        $this->isGrantedEditorOrThrow($era);
        $token = $request->request->get('token');

        if (true === $this->isCsrfTokenValid('remove-arc', $token)) {
            $this->arcManager->remove($arc);

            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        }

        return new JsonResponse('Form is not valid', Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/get-update-form/{id}", name="odl_ajax_arc_get_update_form",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function getUpdateForm(Arc $arc, Request $request): JsonResponse
    {
        $era = $arc->getPeriod()->getEra();
        $this->isGrantedEditorOrThrow($era);

        $toValidate = $request->query->get('toValidate', false);
        $form = $this->createForm(ArcFormType::class, $arc);

        return new JsonResponse($this->renderView('admin/update_arc.html.twig', [
            'form' => $form->createView(),
            'arc' => $arc,
            'toValidate' => filter_var($toValidate, FILTER_VALIDATE_BOOLEAN),
        ]));
    }

    /**
     * @Route("/update/{id}", name="odl_ajax_arc_update",
     * methods={"POST"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function update(Arc $arc, Request $request, TranslatorInterface $translator): JsonResponse
    {
        $era = $arc->getPeriod()->getEra();
        $this->isGrantedEditorOrThrow($era);

        $token = $request->query->get('token');
        if (false === $this->isCsrfTokenValid('update-arc', $token)) {
            return new JsonResponse('Form is not valid', Response::HTTP_BAD_REQUEST);
        }

        $response = [
            'type' => 'info',
            'message' => $translator->trans('form.no_modification', [], 'general'),
        ];
        $code = Response::HTTP_OK;
        $oldArc = clone $arc;
        $form = $this->createForm(ArcFormType::class, $arc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cover = $request->files->get('arc_form')['cover'];
            try {
                $this->arcManager->update($arc, $cover, $oldArc);
                $response = [
                    'type' => 'success',
                    'message' => $translator->trans('form.confirm_modification', [], 'general'),
                    'arc' => $arc,
                ];
            } catch (Throwable $exception) {
                $response = [
                    'type' => 'error',
                    'message' => $translator->trans('general_error', [], 'general'),
                ];
                $code = Response::HTTP_INTERNAL_SERVER_ERROR;
                $this->logger->error('Update arc fails', ['exception' => $exception]);
            }
        }

        return new JsonResponse($response, $code);
    }

    /**
     * @Route("/arc/{id}", name="odl_ajax_arc_get",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function getArc(Arc $arc): JsonResponse
    {
        return new JsonResponse($arc);
    }

    /**
     * @Route("/period/{id}", name="odl_ajax_arcsByPeriod",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function getArcsByPeriod(Period $period, ArcRepository $repository): JsonResponse
    {
        $arcs = $repository->findBy(['period' => $period, 'isValidated' => true]);

        return new JsonResponse($arcs);
    }

    /**
     * @Route("/period/{id}/{page}", name="odl_ajax_arc_paginate",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true },
     * requirements={"page": "\d+"})
     */
    public function paginate(Period $period, ArcRepository $repository, int $page): JsonResponse
    {
        $limit = 20;
        $offset = $limit * ($page - 1);
        $arcs = $repository->findByPeriodWithLimit($period, $offset, $limit);
        $era = $period->getEra();
        $canEdit = $this->isGrantedEditor($era);
        $html = $this->renderView('partial/arc_empty.html.twig', ['era' => $era, 'canEdit' => $canEdit]);

        return new JsonResponse(['arcs' => $arcs, 'html' => $html]);
    }

    /**
     * @Route("/validate", name="odl_ajax_arc_validation",
     * methods={"POST"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     * @IsGranted("ROLE_EDITOR")
     */
    public function validate(Request $request, TranslatorInterface $translator, ArcRepository $repository): JsonResponse
    {
        $token = $request->request->get('token');
        if (false === $this->isCsrfTokenValid('validate-arcs', $token)) {
            return new JsonResponse('Form is not valid', Response::HTTP_BAD_REQUEST);
        }

        $ids = $request->request->get('ids', []);
        if ([] === $ids) {
            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        }

        $count = count($ids);

        try {
            $result = $repository->validateArcs($ids);
            if ($result !== $count) {
                throw new Exception('Not all arcs have been updated', ['count' => $count, 'updated' => $result]);
            }
            $code = Response::HTTP_OK;
            $response = $translator->trans('form.confirm_modification', [], 'general');
        } catch (Throwable $exception) {
            $this->logger->error('Arcs validation fails', ['exception' => $exception]);
            $response = $translator->trans('general_error', [], 'general');
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new JsonResponse($response, $code);
    }

    /**
     * @Route("/moderation/{page}", name="odl_ajax_arc_moderation_paginate",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true },
     * requirements={"page": "\d+"})
     * @IsGranted("ROLE_EDITOR")
     */
    public function paginateModeration(ArcRepository $repository, int $page): JsonResponse
    {
        $limit = 20;
        $offset = $limit * ($page - 1);
        $arcs = $repository->findToValidateWithLimit($offset, $limit);
        $html = $this->renderView('partial/arcs_moderation.html.twig', ['arcs' => $arcs]);

        return new JsonResponse(['html' => $html]);
    }
}

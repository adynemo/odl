<?php

namespace ODL\Controller\Ajax;

use ODL\Entity\User;
use ODL\Service\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ajax/user")
 */
class UserAjax extends AbstractController
{
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @Route("/{id}", name="odl_ajax_user_delete",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function delete(User $user, Request $request)
    {
        $token = $request->query->get('token');
        if (false === $this->isCsrfTokenValid('delete-user', $token)) {
            return new JsonResponse('Form is not valid', JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->userManager->remove($user);

        return new JsonResponse(null, JsonResponse::HTTP_ACCEPTED);
    }

    /**
     * @Route("/{id}/remove-avatar", name="odl_ajax_user_avatar_remove",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     * @IsGranted("ROLE_USER")
     */
    public function removeAvatar(User $user, Request $request)
    {
        $token = $request->query->get('token');
        if (false === $this->isCsrfTokenValid('remove-avatar', $token)) {
            return new JsonResponse('Form is not valid', JsonResponse::HTTP_BAD_REQUEST);
        }

        if (($user === $this->getUser()) || $this->isGranted('ROLE_SUPER_ADMIN')) {
            $this->userManager->removeAvatar($user);
        }

        return new JsonResponse();
    }

    /**
     * @Route("/{id}/generate-api-token", name="odl_ajax_user_generate_api_token",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function generateAPIToken(User $user, Request $request)
    {
        $token = $request->query->get('token');
        if (false === $this->isCsrfTokenValid('generate-token', $token)) {
            return new JsonResponse('Form is not valid', JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->userManager->generateAPIToken($user);

        return new JsonResponse(['token' => $user->getApiToken()], JsonResponse::HTTP_CREATED);
    }
}

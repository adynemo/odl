<?php

namespace ODL\Controller\Ajax;

use ODL\Entity\Era;
use ODL\Service\EraManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ajax/era")
 */
class EraAjax extends AbstractController
{
    /**
     * @Route("/delete/{id}", name="odl_ajax_era_delete_era",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Era $era, Request $request, EraManager $eraManager)
    {
        $token = $request->request->get('token');

        if (true === $this->isCsrfTokenValid('remove-era', $token)) {
            $eraManager->remove($era);

            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
        }

        return new JsonResponse('Form is not valid', JsonResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/get/{id}", name="odl_ajax_era_get_era",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function getEra(Era $era)
    {
        return new JsonResponse($era);
    }
}

<?php

namespace ODL\Controller\API;

use Doctrine\Persistence\ManagerRegistry;
use ODL\DTO\Arc;
use ODL\Entity\Era;
use ODL\Event\ArcEvent;
use ODL\Service\ArcManager;
use ODL\Transformer\ArcTransformer;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

/**
 * @Route("/api/arc")
 */
class ArcAPI extends AbstractController
{
    /**
     * @Route("/create", name="odl_api_arc_create", methods={"POST"})
     * @IsGranted("ROLE_CONTRIBUTOR")
     */
    public function create(
        Request $request,
        SerializerInterface $serializer,
        ArcManager $arcManager,
        ArcTransformer $transformer,
        ManagerRegistry $registry,
        LoggerInterface $logger
    ): JsonResponse {
        $data = $request->request->all();
        $cover = $request->files->get('cover');

        if (isset($data['isEvent'])) {
            $data['isEvent'] = filter_var($data['isEvent'], FILTER_VALIDATE_BOOLEAN);
        }

        $object = $serializer->deserialize(json_encode($data), Arc::class, 'json');
        if (false === $this->checkRequirements($object, $data)) {
            return new JsonResponse('Some required fields are missing', Response::HTTP_BAD_REQUEST);
        }

        $era = $registry->getRepository(Era::class)->find($data['era']);
        if (null === $era) {
            return new JsonResponse('Era does not match anyway', Response::HTTP_BAD_REQUEST);
        }

        try {
            $arc = $transformer->transform($object, $era);

            if (null !== $cover) {
                $arcManager->uploadCover($arc, $cover);
            }

            $arcManager->save($arc);
            $arcManager->log($arc, ArcEvent::PENDING);
        } catch (Throwable $exception) {
            $logger->error('API cannot create arc, the process fails', ['exception' => $exception]);

            return new JsonResponse('API cannot create arc, the process fails', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse(['id' => $arc->getId()], Response::HTTP_CREATED);
    }

    private function checkRequirements(Arc $object, array $data): bool
    {
        if (
            null === $object->getTitle() ||
            null === $object->getDescription() ||
            !isset($data['era'])
        ) {
            return false;
        }

        return true;
    }
}

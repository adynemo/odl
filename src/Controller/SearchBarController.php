<?php

namespace ODL\Controller;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Arc;
use ODL\Entity\Era;
use ODL\Service\ArcManager;
use ODL\Traits\IsGrantedEditorTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/search")
 */
class SearchBarController extends AbstractController
{
    use IsGrantedEditorTrait;

    private const QUERY_MIN_CHARACTERS = 3;

    /**
     * @Route("/arc/{id}", name="odl_search_arc",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function searchArc(Arc $arc): JsonResponse
    {
        $period = $arc->getPeriod();
        $era = $period->getEra();
        $canEdit = $this->isGrantedEditor($era);
        $html = $this->renderView('partial/arc_empty.html.twig', ['era' => $era, 'canEdit' => $canEdit]);

        return new JsonResponse(['arc' => $arc, 'html' => $html, 'title' => $period->getName()]);
    }

    /**
     * @Route("/arcs", name="odl_search_arcs",
     * options = { "expose" = true })
     */
    public function searchArcs(
        Request $request,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        ArcManager $arcManager
    ): Response {
        $twigArgs = ['error' => $translator->trans('min_characters', ['%min%' => self::QUERY_MIN_CHARACTERS], 'search')];
        $query = urldecode($request->query->get('q', ''));

        if (self::QUERY_MIN_CHARACTERS <= strlen($query)) {
            $eraId = $request->query->get('era');
            $eraRepository = $entityManager->getRepository(Era::class);

            $era = (null !== $eraId) ? $eraRepository->find($eraId) : null;
            $arcs = $arcManager->findArcsByQuery($query, $era);

            $twigArgs = [
                'arcs' => $arcs,
                'era' => $era,
                'query' => $query,
                'from' => $eraId,
            ];
        }

        return $this->render('search.html.twig', $twigArgs);
    }
}

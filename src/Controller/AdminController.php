<?php

namespace ODL\Controller;

use ODL\Entity\User;
use ODL\Form\SettingsForm\CopyrightSettingsFormType;
use ODL\Form\SettingsForm\EmailSettingsFormType;
use ODL\Form\SettingsForm\MaintenanceFormType;
use ODL\Form\SettingsForm\WebsiteBannerSettingsFormType;
use ODL\Form\UserForm\CreateUserFormType;
use ODL\Repository\JournalRepository;
use ODL\Service\ForgottenPasswordManager;
use ODL\Service\MailerManager;
use ODL\Service\MaintenanceManager;
use ODL\Service\SettingManager;
use ODL\Service\UserManager;
use ODL\Traits\MessageFlashTrait;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    use MessageFlashTrait;

    public const JOURNAL_ELEMENTS_BY_PAGE = 50;
    private const JOURNAL_GARBAGE_MAX_ELEMENT = 50;

    private ?Request $request;
    private LoggerInterface $logger;
    private TranslatorInterface $translator;

    public function __construct(
        RequestStack $request,
        LoggerInterface $logger,
        TranslatorInterface $translator
    ) {
        $this->request = $request->getCurrentRequest();
        $this->logger = $logger;
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="odl_admin_home")
     */
    public function index(): Response
    {
        return $this->render('admin/home.html.twig');
    }

    /**
     * @Route("/journal", name="odl_admin_journal", options = {"expose"=true}))
     */
    public function journal(Request $request, JournalRepository $repository): Response
    {
        $page = $request->query->get('page', 1);
        $limit = self::JOURNAL_ELEMENTS_BY_PAGE;
        $offset = $limit * ($page - 1);

        $journal = $repository->findBy([], ['createdAt' => 'DESC'], $limit, $offset);
        $count = $repository->countTotal();

        return $this->render('admin/journal.html.twig', [
            'journal' => $journal,
            'count' => $count,
            'page' => $page,
            'maxGarbage' => self::JOURNAL_GARBAGE_MAX_ELEMENT,
        ]);
    }

    /**
     * @Route("/journal/garbage", name="odl_admin_journal_garbage", methods={"POST"})
     */
    public function journalGarbage(Request $request, CsrfTokenManagerInterface $csrfTokenManager, JournalRepository $repository): RedirectResponse
    {
        $token = $request->request->get('_token', '');

        if (true === $this->isCsrfTokenValid('journal-garbage', $token)) {
            $repository->garbage(self::JOURNAL_GARBAGE_MAX_ELEMENT);
            $csrfTokenManager->refreshToken('journal-garbage');
        }

        return $this->redirectToRoute('odl_admin_journal');
    }

    /**
     * @Route("/settings", name="odl_admin_settings")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function settings(SettingManager $settingManager, MaintenanceManager $maintenanceManager)
    {
        $settings = $settingManager->findAll();
        $formMaintenance = $this->createForm(MaintenanceFormType::class);
        $formMaintenance->handleRequest($this->request);
        $formEmail = $this->createForm(EmailSettingsFormType::class);
        $formEmail->handleRequest($this->request);
        $formBanner = $this->createForm(WebsiteBannerSettingsFormType::class);
        $formBanner->handleRequest($this->request);
        $formCopyright = $this->createForm(CopyrightSettingsFormType::class);
        $formCopyright->handleRequest($this->request);

        $forms = [$formMaintenance, $formEmail, $formBanner, $formCopyright];

        try {
            if ($this->handleSettingsForm($forms, $settingManager, $maintenanceManager)) {
                return $this->redirectToRoute('odl_admin_settings');
            }
        } catch (Throwable $exception) {
            $this->error($this->translator->trans('general_error', [], 'general'));
            $this->addFlashMessage();
            $this->logger->error($exception->getMessage(), ['exception' => $exception]);

            return $this->redirectToRoute('odl_admin_settings');
        }

        return $this->render('admin/settings.html.twig', [
            'settings' => $settings,
            'formEmail' => $formEmail->createView(),
            'formBanner' => $formBanner->createView(),
            'formMaintenance' => $formMaintenance->createView(),
            'formCopyright' => $formCopyright->createView(),
        ]);
    }

    /**
     * @param array<FormInterface> $forms
     */
    private function handleSettingsForm(array $forms, SettingManager $settingManager, MaintenanceManager $maintenanceManager): bool
    {
        $this->success($this->translator->trans('form.confirm_modification', [], 'general'));
        foreach ($forms as $form) {
            if ($form->isSubmitted() && $form->isValid()) {
                $dataSubmitted = $form->getData();
                if ('maintenance_form' === $form->getName()) {
                    $message = $maintenanceManager->handleMaintenance($dataSubmitted['active'], $dataSubmitted['ttl']);
                    $this->info($message);
                } else {
                    $settingManager->update($dataSubmitted);
                }

                $this->addFlashMessage();

                return true;
            }
        }

        return false;
    }

    /**
     * @Route("/users", name="odl_admin_users")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function users(UserManager $userManager): Response
    {
        return $this->render('admin/users.html.twig', ['users' => $userManager->findAll()]);
    }

    /**
     * @Route("/create-user", name="odl_admin_create_user")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function createUser(
        Request $request,
        UserPasswordHasherInterface $passwordEncoder,
        UserManager $userManager,
        ForgottenPasswordManager $forgottenPasswordManager,
        MailerManager $mailer
    ): Response {
        $user = new User();
        $form = $this->createForm(CreateUserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setLocale($request->getDefaultLocale());
            $userManager->create($user, $userManager->generateRandomPassword(), $passwordEncoder);
            $token = $forgottenPasswordManager->getForgottenPasswordToken($user);
            $mailer->sendUserCreatedEmail($user->getEmail(), [
                'user' => $user,
                'url' => $this->generateUrl('odl_reset_password', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL),
                'website' => $this->getParameter('app.name'),
            ]);
            $this->success($this->translator->trans('user_created_success', ['%username%' => $user->getUsername()], 'users'));
            $this->addFlashMessage();

            return $this->redirectToRoute('odl_admin_users');
        }

        return $this->render('admin/create-user.html.twig', ['form' => $form->createView()]);
    }
}

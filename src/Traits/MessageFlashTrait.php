<?php

namespace ODL\Traits;

/**
 * @internal
 *
 * @method addFlash(string $type, $message)
 */
trait MessageFlashTrait
{
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $message;
    /**
     * @var string[]
     */
    protected $availableTypes = [
        'info',
        'success',
        'warning',
        'error',
    ];

    public function info(string $message)
    {
        $this->setType('info');
        $this->message = $message;
    }

    public function success(string $message)
    {
        $this->setType('success');
        $this->message = $message;
    }

    public function warning(string $message)
    {
        $this->setType('warning');
        $this->message = $message;
    }

    public function error(string $message)
    {
        $this->setType('error');
        $this->message = $message;
    }

    public function addFlashMessage()
    {
        $this->addFlash($this->type, $this->message);
    }

    private function setType(string $type): void
    {
        if (!in_array($type, $this->availableTypes)) {
            throw new \InvalidArgumentException('You must pass a valid message flash type');
        }

        $this->type = $type;
    }
}

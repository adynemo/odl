<?php

namespace ODL\Traits;

use RuntimeException;

trait EmailTrait
{
    public function checkEmail(?string $email): string
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new RuntimeException('You must type a valid email.');
        }

        return $email;
    }
}

<?php

namespace ODL\Traits;

use ODL\Entity\Era;
use ODL\Security\EraVoter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @internal
 *
 * @method isGranted(string $attribute, $subject = null)
 */
trait IsGrantedEditorTrait
{
    public function isGrantedEditor(Era $era): bool
    {
        return $this->isGranted('ROLE_ADMIN') ||
        ($this->isGranted('ROLE_EDITOR') && $this->isGranted(EraVoter::EDIT, $era));
    }

    public function isGrantedEditorOrThrow(Era $era): void
    {
        if (!$this->isGrantedEditor($era)) {
            throw new AccessDeniedHttpException();
        }
    }
}

<?php

namespace ODL\Traits;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use ODL\Entity\Section;
use ODL\Exception\BadPositionException;

/**
 * @var ServiceEntityRepository $repository
 */
trait PositionTrait
{
    public function getLastPosition(?Section $parent): int
    {
        if (null !== $parent) {
            $lastChild = $this->repository->findLastChildByParent($parent);
        } else {
            $lastChild = $this->repository->findLastUniverse();
        }

        return (null !== $lastChild) ? $lastChild->getPosition() + 1 : 1;
    }

    /**
     * @throws BadPositionException
     */
    public function checkPositionValue(Section $section): void
    {
        $position = $section->getPosition();

        if (null === $position || 0 >= $position) {
            throw new BadPositionException('Position must be a strict positive integer');
        }
    }
}

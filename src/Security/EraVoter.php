<?php

namespace ODL\Security;

use LogicException;
use ODL\Entity\Era;
use ODL\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class EraVoter extends Voter
{
    public const EDIT = 'edit';

    protected function supports($attribute, $subject): bool
    {
        if (self::EDIT !== $attribute) {
            return false;
        }

        if (!($subject instanceof Era)) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /**
         * @var Era $eras
         */
        $eras = $subject;

        if (self::EDIT === $attribute) {
            return $this->canEdit($eras, $user);
        }

        throw new LogicException('This code should not be reached!');
    }

    private function canEdit(Era $eras, User $user): bool
    {
        return $user->getEditableEras()->contains($eras);
    }
}

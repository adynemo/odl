<?php

namespace ODL\Service;

use ODL\Entity\Setting;
use ODL\Repository\SettingRepository;
use RuntimeException;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class MailerManager
{
    private MailerInterface $mailer;
    private TemplatedEmail $email;
    private Address $sender;
    private SettingRepository $settingRepository;
    private TranslatorInterface $translator;

    public function __construct(
        MailerInterface $mailer,
        SettingRepository $settingRepository,
        TranslatorInterface $translator
    ) {
        $this->mailer = $mailer;
        $this->settingRepository = $settingRepository;
        $this->translator = $translator;
        $this->setSender();
        $this->setEmail();
    }

    public function sendForgottenPasswordEmail(string $recipient, array $args): void
    {
        $subject = $this->translator->trans('reset_password.subject', [], 'email');
        $this->createEmail($recipient, $args, $subject, 'emails/forgotten-password.html.twig');

        $this->send();
    }

    public function sendUserCreatedEmail(string $recipient, array $args): void
    {
        $subject = $this->translator->trans('user_created.subject', ['%website%' => $args['website']], 'email');
        $this->createEmail($recipient, $args, $subject, 'emails/user-created.html.twig');

        $this->send();
    }

    public function sendRegistrationEmail(string $recipient, array $args): void
    {
        $subject = $this->translator->trans('registration.subject', ['%website%' => $args['website']], 'email');
        $this->createEmail($recipient, $args, $subject, 'emails/registration.html.twig');

        $this->send();
    }

    private function setSender(): void
    {
        $address = $this->getValueByParam('admin_sender_email');
        if ('' === $address) {
            throw new RuntimeException('You must register sender email to use Mailer');
        }

        $name = $this->getValueByParam('admin_sender_name');

        $this->sender = new Address($address, $name);
    }

    private function setEmail(): void
    {
        $this->email = (new TemplatedEmail())
            ->from($this->sender);
    }

    public function getEmail(): TemplatedEmail
    {
        return $this->email;
    }

    private function getValueByParam(string $param): string
    {
        $value = '';
        $setting = $this->settingRepository->findOneBy(['param' => $param]);

        if ($setting instanceof Setting) {
            $value = $setting->getValue();
        }

        return $value;
    }

    private function createEmail(string $recipient, array $args, string $subject, string $template): void
    {
        $banner = $this->getValueByParam('website_banner');

        if ('' !== $banner) {
            $args = array_merge($args, ['banner' => $banner]);
        }

        if ('' !== $this->sender->getName()) {
            $args = array_merge($args, ['sign' => $this->sender->getName()]);
        }

        $this->email->to($recipient)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($args);
    }

    private function send(): void
    {
        $this->mailer->send($this->email);

        // Reset email
        $this->setEmail();
    }
}

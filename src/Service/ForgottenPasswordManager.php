<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\User;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class ForgottenPasswordManager
{
    private $entityManager;
    private $tokenGenerator;

    public function __construct(
        EntityManagerInterface $entityManager,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $this->entityManager = $entityManager;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function getForgottenPasswordToken(User $user): string
    {
        $now = new \DateTime('now');
        $token = $this->tokenGenerator->generateToken().'date'.$now->format('YmdHis');

        $user->setForgottenPassword($token);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $token;
    }

    public function isValidDate(string $token): bool
    {
        $dateString = explode('date', $token)[1] ?? null;
        if (null === $dateString) {
            return false;
        }
        $date = new \DateTime($dateString);
        $ref = new \DateTime('10 minutes ago');

        return $date >= $ref;
    }
}

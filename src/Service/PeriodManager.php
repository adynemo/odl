<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\DataCollector\Collection;
use ODL\Entity\Era;
use ODL\Entity\Period;
use ODL\Traits\PositionTrait;
use ODL\Util\StringModifier;

class PeriodManager
{
    use PositionTrait;

    private $entityManager;
    private $stringModifier;
    private $repository;

    public function __construct(
        EntityManagerInterface $entityManager,
        StringModifier $stringModifier
    ) {
        $this->entityManager = $entityManager;
        $this->stringModifier = $stringModifier;
        $this->repository = $this->entityManager->getRepository(Period::class);
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function findBy(array $criteria): Collection
    {
        $results = $this->repository->findBy($criteria, ['position' => 'ASC']);

        return new Collection($results, Period::class);
    }

    public function upsert(Collection $periods, Era $parent)
    {
        $position = null;
        foreach ($periods->getCollection() as $period) {
            if (null === $period->getSlug()) {
                $position = $position ? ++$position : $this->getLastPosition($parent);
                $period->setPosition($position);
                $period->setEra($parent);
            }

            $this->checkPositionValue($period);
            $slug = $this->stringModifier->generateSlug($period->getName());
            $period->setSlug($slug);

            $this->entityManager->persist($period);
        }

        $this->entityManager->flush();
    }
}

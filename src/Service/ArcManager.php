<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Arc;
use ODL\Entity\Era;
use ODL\Event\ArcEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ArcManager
{
    private $entityManager;
    private $repository;
    private $fileManager;
    private $eventDispatcher;

    public function __construct(
        EntityManagerInterface $entityManager,
        FileManager $fileManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Arc::class);
        $this->fileManager = $fileManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function create(Arc $arc, UploadedFile $file)
    {
        $lastArc = $this->repository->findLastChildByParent($arc->getPeriod());

        if (!($lastArc instanceof Arc)) {
            $arc->setPosition(1);
        } else {
            if ((null === $arc->getPosition()) || ($arc->getPosition() > $lastArc->getPosition())) {
                $arc->setPosition($lastArc->getPosition() + 1);
            } else {
                $this->handlePositions($arc, $lastArc->getPosition());
            }
        }

        $this->uploadCover($arc, $file);

        $this->save($arc);
        $this->log($arc, ArcEvent::CREATED);
    }

    public function remove(Arc $arc)
    {
        $cover = $arc->getCover();
        $lastArc = $this->repository->findLastChildByParent($arc->getPeriod());

        if ($arc->getId() !== $lastArc->getId()) {
            $this->handlePositions($arc, $lastArc->getPosition(), true);
        }

        $this->log($arc, ArcEvent::REMOVED);

        $this->entityManager->remove($arc);
        $this->entityManager->flush();

        $this->fileManager->setTargetDirectory($this->fileManager::COVERS);
        $this->fileManager->remove($cover);
    }

    public function update(Arc $arc, ?UploadedFile $file, Arc $oldArc): bool
    {
        if (null === $arc->getPeriod()) {
            $arc->setPeriod($oldArc->getPeriod());
        }
        if ($file instanceof UploadedFile) {
            $actualCover = $arc->getCover();
            $this->uploadCover($arc, $file);
            $this->fileManager->remove($actualCover);
        }

        $this->handlePositions($arc, $oldArc->getPosition());

        $this->save($arc);
        $this->log($arc, ArcEvent::UPDATED, $oldArc);

        return true;
    }

    public function findArcsByQuery(string $query, ?Era $era): array
    {
        $periodIds = [];

        if (null !== $era) {
            $periodIds = array_map(function ($period) {
                return $period->getId();
            }, $era->getPeriods()->toArray());
        }

        return $this->repository->findArcsByQuery($query, $periodIds);
    }

    private function handlePositions(Arc $arc, int $positionToCompare, bool $isRemoval = false)
    {
        $firstPosition = $arc->getPosition();
        $lastPosition = $positionToCompare;
        $isUp = !$isRemoval;

        if ($firstPosition > $positionToCompare) {
            $firstPosition = ++$positionToCompare;
            $lastPosition = $arc->getPosition();
            $isUp = false;
        }

        $arcs = $this->repository->findArcsFromPeriodBetweenPositions($arc->getPeriod(), $firstPosition, $lastPosition);

        $this->updatePositions($arc, $arcs, $isUp);
    }

    /**
     * @param array<Arc> $arcs
     */
    private function updatePositions(Arc $currentArc, array $arcs, bool $isUp)
    {
        foreach ($arcs as $arc) {
            if ($arc->getId() === $currentArc->getId()) {
                continue;
            }
            $position = $arc->getPosition();
            $newPosition = (true === $isUp) ? ++$position : --$position;
            $arc->setPosition($newPosition);
            $this->entityManager->persist($arc);
        }

        $this->entityManager->flush();
    }

    public function uploadCover(Arc $arc, UploadedFile $file)
    {
        $this->fileManager->setTargetDirectory($this->fileManager::COVERS);
        $filename = $this->fileManager->upload($file);
        $arc->setCover($filename);
    }

    public function log(Arc $arc, string $type, ?Arc $oldArc = null)
    {
        if (!in_array($type, ArcEvent::ALLOWED_EVENT)) {
            throw new \InvalidArgumentException(sprintf('`%s` argument is not provided by %s', $type, ArcEvent::class));
        }
        if ((ArcEvent::UPDATED === $type) && (null === $oldArc)) {
            throw new \InvalidArgumentException('You must pass the old arc for an update event');
        }

        $this->eventDispatcher->dispatch(new ArcEvent($arc, $oldArc), $type);
    }

    public function save(Arc $arc)
    {
        $this->entityManager->persist($arc);
        $this->entityManager->flush();
    }
}

<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Setting;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SettingManager
{
    private $em;
    private $repo;
    private $fileManager;

    public function __construct(
        EntityManagerInterface $em,
        FileManager $fileManager
    ) {
        $this->em = $em;
        $this->repo = $this->em->getRepository(Setting::class);
        $this->fileManager = $fileManager;
    }

    public function findAll(): array
    {
        $settings = [];
        $results = $this->repo->findAll();

        foreach ($results as $result) {
            $settings[$result->getParam()] = $result->getValue();
        }

        return $settings;
    }

    public function update(array $data)
    {
        foreach ($data as $param => $value) {
            if ($value instanceof UploadedFile) {
                $this->fileManager->setTargetDirectory($this->fileManager::WEBSITE_BANNER);
                $value = $this->fileManager->upload($value, $param);
            }

            $this->createOrUpdate($value, $param);
        }

        $this->em->flush();
    }

    private function createOrUpdate(string $value, string $param)
    {
        $setting = $this->repo->findOneBy(['param' => $param]);

        if (!($setting) instanceof Setting) {
            $setting = (new Setting())->setParam($param);
        }

        $setting->setValue($value);

        $this->em->persist($setting);
    }
}

<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\DataCollector\Collection;
use ODL\Entity\Era;
use ODL\Entity\Universe;
use ODL\Traits\PositionTrait;
use ODL\Util\StringModifier;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class EraManager
{
    use PositionTrait;

    private $em;
    private $stringModifier;
    private $fileManager;
    private $repository;

    public function __construct(
        EntityManagerInterface $em,
        StringModifier $stringModifier,
        FileManager $fileManager
    ) {
        $this->em = $em;
        $this->stringModifier = $stringModifier;
        $this->fileManager = $fileManager;
        $this->repository = $this->em->getRepository(Era::class);
    }

    public function findBy(array $criteria): Collection
    {
        $results = $this->repository->findBy($criteria, ['position' => 'ASC']);

        return new Collection($results, Era::class);
    }

    /**
     * @param array<array<UploadedFile>> $images
     */
    public function upsert(Collection $eras, array $images, Universe $parent): void
    {
        $position = null;
        foreach ($eras->getCollection() as $id => $era) {
            if (null === $era->getSlug()) {
                $position = $position ? ++$position : $this->getLastPosition($parent);
                $era->setPosition($position)
                    ->setUniverse($parent);
            }

            $this->checkPositionValue($era);
            $slug = $this->stringModifier->generateSlug($era->getName());
            $era->setSlug($slug);
            $this->updateImages($era, $images[$id]);

            $this->em->persist($era);
        }

        $this->em->flush();
    }

    public function remove(Era $era): void
    {
        $images = [
            'banner' => $era->getImage(),
            'linkA' => $era->getLogoLinkA(),
            'linkB' => $era->getLogoLinkB(),
        ];

        $this->em->remove($era);
        $this->em->flush();

        foreach ($images as $key => $image) {
            if (null !== $image) {
                $this->fileManager->setTargetDirectory(('banner' === $key) ? $this->fileManager::ERA_BANNER : $this->fileManager::ERA_LOGO);
                $this->fileManager->remove($image);
            }
        }
    }

    /**
     * @param array<UploadedFile> $images
     */
    private function updateImages(Era $era, array $images)
    {
        if (null !== $images['image']) {
            $this->updateBanner($era, $images['image']);
        }

        if ((null !== $images['logoLinkA']) || (null !== $images['logoLinkB'])) {
            $this->updateLogos(
                $era,
                [
                    'a' => $images['logoLinkA'],
                    'b' => $images['logoLinkB'],
                ]
            );
        }
    }

    private function updateBanner(Era $era, UploadedFile $banner)
    {
        $this->fileManager->setTargetDirectory($this->fileManager::ERA_BANNER);
        $actualBanner = $era->getImage();
        $filename = $this->fileManager->upload($banner);

        if ($actualBanner) {
            $this->fileManager->remove($actualBanner);
        }

        $era->setImage($filename);
    }

    /**
     * @param array<UploadedFile> $logos
     */
    private function updateLogos(Era $era, array $logos)
    {
        $this->fileManager->setTargetDirectory($this->fileManager::ERA_LOGO);

        foreach ($logos as $key => $value) {
            if (null === $value) {
                continue;
            }
            $filename = $this->fileManager->upload($value);
            $actualLogo = ('a' === $key) ? $era->getLogoLinkA() : $era->getLogoLinkB();
            if (null !== $actualLogo) {
                $this->fileManager->remove($actualLogo);
            }
            ('a' === $key) ? $era->setLogoLinkA($filename) : $era->setLogoLinkB($filename);
        }
    }
}

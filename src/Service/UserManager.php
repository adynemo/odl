<?php

namespace ODL\Service;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\User;
use ODL\Repository\UserRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserManager
{
    private string $defaultLocale;
    private EntityManagerInterface $entityManager;
    private UserRepository $repository;
    private FileManager $fileManager;
    private TokenGeneratorInterface $tokenGenerator;

    public function __construct(
        string $defaultLocale,
        EntityManagerInterface $entityManager,
        UserRepository $repository,
        FileManager $fileManager,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $this->defaultLocale = $defaultLocale;
        $this->entityManager = $entityManager;
        $this->repository = $repository;
        $this->fileManager = $fileManager;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function findBy(array $criteria): array
    {
        return $this->repository->findBy($criteria);
    }

    public function findOneBy(array $criteria): ?User
    {
        return $this->repository->findOneBy($criteria);
    }

    public function find($id): ?User
    {
        return $this->repository->find($id);
    }

    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function update(User $user, string $avatar64 = '')
    {
        if ('' !== $avatar64) {
            $this->fileManager->setTargetDirectory($this->fileManager::AVATAR);
            $filename = $this->fileManager->upload64($avatar64);

            if ($user->getAvatar()) {
                $this->fileManager->remove($user->getAvatar());
            }
            $user->setAvatar($filename);
        }

        $this->save($user);
    }

    public function removeAvatar(User $user, bool $save = true)
    {
        $this->fileManager->setTargetDirectory($this->fileManager::AVATAR);
        if ($user->getAvatar()) {
            $this->fileManager->remove($user->getAvatar());
        }

        if (true === $save) {
            $user->setAvatar(null);
            $this->save($user);
        }
    }

    public function create(
        User $user,
        string $password,
        UserPasswordHasherInterface $passwordEncoder
    ): void {
        $user->setPassword($passwordEncoder->hashPassword($user, $password));
        $user->setLocale($this->defaultLocale);

        $this->save($user);
    }

    public function remove(User $user): void
    {
        $this->removeAvatar($user, false);
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    public function generateAPIToken(User $user): void
    {
        $uuid = Uuid::uuid4();
        $user->setApiToken($uuid);
        $this->save($user);
    }

    public function setActivationFields(User $user, bool $activate = false): void
    {
        $user->setActivationToken($this->tokenGenerator->generateToken());

        if (true === $activate) {
            $user->setActivationDate(new DateTime());
        }
    }

    public function generateRandomPassword(): string
    {
        return md5(random_bytes(25));
    }

    private function save(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}

<?php

namespace ODL\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class URLService
{
    /**
     * @var Request|null
     */
    private $request;
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RequestStack $requestStack, RouterInterface $router)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->router = $router;
    }

    public function getReferrer(): ?string
    {
        if (null === $this->request) {
            return null;
        }

        $referrer = $this->request->headers->get('Referer');
        if (null === $referrer) {
            return null;
        }

        $path = str_replace($this->getBaseURL(), '', $referrer);
        try {
            $router = $this->router->match($path);
        } catch (\Throwable $exception) {
            return null;
        }

        $route = $router['_route'];
        unset($router['_route']);
        unset($router['_controller']);
        $parameters = $router;
        unset($router);

        return $this->router->generate($route, $parameters);
    }

    private function getScheme(): string
    {
        return $this->request->headers->contains('X_FORWARDED_PROTO', 'https') ? 'https' : 'http';
    }

    private function getBaseURL(): string
    {
        return sprintf(
            '%s://%s%s',
            $this->getScheme(),
            $this->request->getHttpHost(),
            $this->request->getBasePath()
        );
    }
}

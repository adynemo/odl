<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\DataCollector\Collection;
use ODL\Entity\Universe;
use ODL\Exception\BadPositionException;
use ODL\Repository\UniverseRepository;
use ODL\Traits\PositionTrait;
use ODL\Util\StringModifier;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UniverseManager
{
    use PositionTrait;

    private EntityManagerInterface $entityManager;
    private StringModifier $stringModifier;
    private UniverseRepository $repository;
    private FileManager $fileManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        UniverseRepository $repository,
        StringModifier $stringModifier,
        FileManager $fileManager
    ) {
        $this->entityManager = $entityManager;
        $this->stringModifier = $stringModifier;
        $this->fileManager = $fileManager;
        $this->repository = $repository;
    }

    /**
     * @throws BadPositionException
     */
    public function upsert(Collection $universes, array $logos): void
    {
        $position = null;
        foreach ($universes->getCollection() as $i => $universe) {
            if (null === $universe->getPosition()) {
                $position = $position ? ++$position : $this->getLastPosition(null);
                $universe->setPosition($position);
            }
            $this->checkPositionValue($universe);

            $slug = $this->stringModifier->generateSlug($universe->getName());
            $universe->setSlug($slug);

            $logo = $logos[$i]['logo'] ?? null;
            if ($logo instanceof UploadedFile) {
                $this->updateLogo($universe, $logo);
            }

            $this->entityManager->persist($universe);
            $this->entityManager->flush();
        }
    }

    public function findAll(): Collection
    {
        $results = $this->repository->findAllSortedByPosition();

        return new Collection($results, Universe::class);
    }

    public function remove(Universe $universe): void
    {
        $logo = $universe->getLogo();

        if (null !== $logo) {
            $this->fileManager->setTargetDirectory($this->fileManager::UNIVERSE_LOGO);
            $this->fileManager->remove($logo);
        }

        $this->entityManager->remove($universe);
        $this->entityManager->flush();
    }

    private function updateLogo(Universe $universe, UploadedFile $logo): void
    {
        $this->fileManager->setTargetDirectory($this->fileManager::UNIVERSE_LOGO);

        $filename = $this->fileManager->upload($logo);
        $actualLogo = $universe->getLogo();

        if (null !== $actualLogo) {
            $this->fileManager->remove($actualLogo);
        }

        $universe->setLogo($filename);
    }
}

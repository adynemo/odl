<?php

namespace ODL\Service;

use Symfony\Component\HttpFoundation\File\Exception\ExtensionFileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageManager
{
    public const PNG_MIME_TYPE = 'image/png';
    public const JPEG_MIME_TYPE = 'image/jpeg';

    private $parameters;
    private $size;
    private $type;

    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    public function setParameters(string $type): self
    {
        if (!isset($this->parameters[$type])) {
            throw new \InvalidArgumentException("You must pass valid parameter's name");
        }

        $this->size = $this->parameters[$type]['size'];
        $this->type = $this->parameters[$type]['type'];

        return $this;
    }

    public function resize(UploadedFile $file): bool
    {
        $pathname = $file->getPathname();
        $mimeType = $file->getMimeType();
        $this->checkMimeType($mimeType);

        if (!([$srcWidth, $srcHeight] = @getimagesize($pathname))) {
            return false;
        }

        if ('height' === $this->type) {
            [$height, $width] = $this->calculateSize($srcWidth, $srcHeight);
        } else {
            [$width, $height] = $this->calculateSize($srcHeight, $srcWidth);
        }

        $image = imagecreatetruecolor($width, $height);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        $srcImage = imagecreatefromstring(file_get_contents($pathname));
        imagecopyresampled($image, $srcImage, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);
        imagedestroy($srcImage);

        $result = (self::PNG_MIME_TYPE === $mimeType) ? imagepng($image, $pathname, 7) : imagejpeg($image, $pathname, 70);
        imagedestroy($image);

        return $result;
    }

    public function createFromData(string $data, string $filepath): bool
    {
        $finfo = new \finfo();
        $mimeType = $finfo->buffer($data, FILEINFO_MIME_TYPE);
        $this->checkMimeType($mimeType);

        $imageSrc = imagecreatefromstring($data);

        if (self::PNG_MIME_TYPE === $mimeType) {
            $width = imagesx($imageSrc);
            $height = imagesy($imageSrc);
            $image = imagecreatetruecolor($width, $height);
            imagesavealpha($image, true);
            $color = imagecolorallocatealpha($image, 0, 0, 0, 127);
            imagefill($image, 0, 0, $color);
            imagecopy($image, $imageSrc, 0, 0, 0, 0, $width, $height);
            $result = imagepng($image, $filepath, 7);
            imagedestroy($image);
        } else {
            $result = imagejpeg($imageSrc, $filepath, 70);
        }

        imagedestroy($imageSrc);

        return $result;
    }

    private function checkMimeType(string $mimeType)
    {
        if ((self::JPEG_MIME_TYPE !== $mimeType) && (self::PNG_MIME_TYPE !== $mimeType)) {
            throw new ExtensionFileException(sprintf('You must pass an available mime type: %s or %s. %s provided', self::PNG_MIME_TYPE, self::JPEG_MIME_TYPE, $mimeType));
        }
    }

    /**
     * @return array<int>
     */
    private function calculateSize(int $srcSide, int $srcOtherSide): array
    {
        return [
            $this->size,
            ($this->size * $srcSide) / $srcOtherSide,
        ];
    }
}

<?php

namespace ODL\Service;

use Ady\Bundle\MaintenanceBundle\Drivers\DriverFactory;
use Symfony\Contracts\Translation\TranslatorInterface;

class MaintenanceManager
{
    private $driver;
    private $translator;

    public function __construct(DriverFactory $driverFactory, TranslatorInterface $translator)
    {
        $this->driver = $driverFactory->getDriver();
        $this->translator = $translator;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function isExists(): ?bool
    {
        return $this->driver->isExists();
    }

    public function getTtl(): ?bool
    {
        return $this->driver->getTtl();
    }

    public function handleMaintenance(bool $activate, ?string $ttl): string
    {
        if (false === $activate) {
            $this->driver->unlock();

            return $this->translator->trans('maintenance.unlock', [], 'settings');
        }

        $message = $this->translator->trans('maintenance.already_lock', [], 'settings');
        if ((null === $this->driver->isExists()) || !$this->driver->isExists()) {
            $choice = 'full';
            if ((null !== $ttl) && 0 < $ttl) {
                $this->driver->setTtl($ttl);
                $choice = 'ttl';
            }
            $this->driver->getMessageLock($this->driver->lock());
            $message = $this->translator->trans('maintenance.lock.'.$choice, ['%ttl%' => $ttl], 'settings');
        }

        return $message;
    }
}

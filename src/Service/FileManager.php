<?php

namespace ODL\Service;

use finfo;
use InvalidArgumentException;
use ODL\Util\StringModifier;
use RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\ExtensionFileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileManager
{
    public const MIME_TYPES_MAP = [
        ImageManager::JPEG_MIME_TYPE => 'jpeg',
        ImageManager::PNG_MIME_TYPE => 'png',
    ];

    public const COVERS = 'arc.covers';
    public const ERA_BANNER = 'era.banner';
    public const ERA_LOGO = 'era.logo';
    public const UNIVERSE_LOGO = 'universe.logo';
    public const WEBSITE_BANNER = 'setting.website_banner';
    public const AVATAR = 'user.avatar';

    private const MAP = [
        self::COVERS => 'app.assets_image_covers',
        self::ERA_BANNER => 'app.assets_image_sections',
        self::ERA_LOGO => 'app.assets_image_logos',
        self::UNIVERSE_LOGO => 'app.assets_image_logos',
        self::WEBSITE_BANNER => 'app.assets_image',
        self::AVATAR => 'app.assets_image_user_avatars',
    ];

    private $params;
    private $stringModifier;
    private $targetDirectory;
    private $imageManager;
    private $filesystem;
    private $type;

    public function __construct(
        ContainerBagInterface $params,
        StringModifier $stringModifier,
        ImageManager $imageParameter,
        Filesystem $filesystem
    ) {
        $this->params = $params;
        $this->stringModifier = $stringModifier;
        $this->imageManager = $imageParameter;
        $this->filesystem = $filesystem;
    }

    public function setTargetDirectory(string $type): self
    {
        if (!isset(self::MAP[$type])) {
            throw new InvalidArgumentException('Target directory cannot be set, wrong type provided');
        }

        $this->type = $type;
        $this->targetDirectory = $this->params->get(self::MAP[$type]);

        return $this;
    }

    public function upload(UploadedFile $file, string $filename = ''): string
    {
        $filename = $this->buildFilename($filename, $file->guessExtension());

        $this->imageManager->setParameters($this->type);
        if (false === $this->imageManager->resize($file)) {
            throw new RuntimeException('Image resizing fails');
        }
        $file->move($this->targetDirectory, $filename);

        return $filename;
    }

    public function upload64(string $data64, string $filename = ''): string
    {
        $data64 = explode(';base64,', $data64);
        $base64 = $data64[1];
        $data = base64_decode($base64);
        $finfo = new finfo();
        $mimeType = $finfo->buffer($data, FILEINFO_MIME_TYPE);
        $extension = self::MIME_TYPES_MAP[$mimeType] ?? false;

        if (false === $extension) {
            throw new ExtensionFileException('Image must be png or jpeg file');
        }

        $filename = $this->buildFilename($filename, $extension);
        $filepath = $this->buildFilepath($filename);

        $this->imageManager->createFromData($data, $filepath);

        return $filename;
    }

    public function remove(string $filename)
    {
        $targetFile = $this->buildFilepath($filename);
        if ('' !== $filename && $this->filesystem->exists($targetFile)) {
            $this->filesystem->remove($targetFile);
        }
    }

    private function buildFilename(string $filename, string $extension): string
    {
        $name = ('' === $filename) ? $this->stringModifier->generateRandomString() : $filename;

        return sprintf('%s.%s', $name, $extension);
    }

    private function buildFilepath(string $filename): string
    {
        return sprintf('%s/%s', $this->targetDirectory, $filename);
    }
}

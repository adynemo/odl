<?php

namespace ODL\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use ODL\Entity\Setting;

class SettingFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $setting1 = new Setting();
        $setting1->setParam('setting_one')
            ->setValue('value_one');
        $setting2 = new Setting();
        $setting2->setParam('setting_two')
            ->setValue('value_two');

        $manager->persist($setting1);
        $manager->persist($setting2);
        $manager->flush();
    }
}

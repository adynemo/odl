<?php

namespace ODL\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use ODL\Entity\Period;

class PeriodFixtures extends Fixture implements OrderedFixtureInterface
{
    public const NEXT_POSITION = 3;

    public function load(ObjectManager $manager)
    {
        $era = $this->getReference('era');

        $period = (new Period())
            ->setName('Period')
            ->setSlug('period')
            ->setEra($era)
            ->setPosition(1);

        $manager->persist($period);

        $period2 = (new Period())
            ->setName('Period 2')
            ->setSlug('period-2')
            ->setEra($era)
            ->setPosition(2);

        $manager->persist($period2);

        $manager->flush();

        $this->setReference('period', $period);
    }

    public function getOrder(): int
    {
        return 3;
    }
}

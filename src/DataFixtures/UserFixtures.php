<?php

namespace ODL\DataFixtures;

use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use ODL\Entity\User;
use Ramsey\Uuid\Uuid;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public const UUID_USER = '0cbcb52f-e782-409b-abc7-e27e92017e4b';
    public const UUID_CONTRIBUTOR = 'c0c1be85-563d-45d5-8e2c-715c7ad74302';

    private UserPasswordHasherInterface $passwordEncoder;

    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $now = new DateTimeImmutable();

        $user = (new User())
            ->setUsername('user')
            ->setEmail('user@odl.test')
            ->setLocale('en')
            ->setRoles(['ROLE_USER'])
            ->setApiToken(Uuid::fromString(self::UUID_USER))
            ->setActivationDate($now);
        $password = $this->passwordEncoder->hashPassword($user, 'password');
        $user->setPassword($password);

        $contributor = (new User())
            ->setUsername('contributor')
            ->setEmail('contributor@odl.test')
            ->setPassword('password')
            ->setLocale('en')
            ->setRoles(['ROLE_CONTRIBUTOR'])
            ->setApiToken(Uuid::fromString(self::UUID_CONTRIBUTOR))
            ->setActivationDate($now);
        $password = $this->passwordEncoder->hashPassword($contributor, 'password');
        $contributor->setPassword($password);

        $inactive = (new User())
            ->setUsername('inactive')
            ->setEmail('inactive@odl.test')
            ->setPassword('password')
            ->setLocale('en');
        $password = $this->passwordEncoder->hashPassword($inactive, 'password');
        $inactive->setPassword($password);

        $manager->persist($user);
        $manager->persist($contributor);
        $manager->persist($inactive);
        $manager->flush();
    }
}

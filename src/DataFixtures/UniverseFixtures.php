<?php

namespace ODL\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use ODL\Entity\Universe;

class UniverseFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $universe1 = new Universe();
        $universe1->setName('Marvel')
            ->setSlug('marvel')
            ->setPosition(1);
        $universe2 = new Universe();
        $universe2->setName('DC Comics')
            ->setSlug('dc-comics')
            ->setPosition(2);

        $manager->persist($universe1);
        $manager->persist($universe2);
        $manager->flush();

        $this->setReference('marvel', $universe1);
        $this->setReference('dccomics', $universe2);
    }

    public function getOrder(): int
    {
        return 1;
    }
}

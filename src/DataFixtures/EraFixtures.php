<?php

namespace ODL\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use ODL\Constant\EraStatus;
use ODL\Entity\Era;

class EraFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $universe = $this->getReference('marvel');

        $era1 = (new Era())
            ->setName('Era')
            ->setSlug('era')
            ->setImage('filename')
            ->setUniverse($universe)
            ->setStatus(EraStatus::IN_PROGRESS)
            ->setPosition(1);

        $era2 = (new Era())
            ->setName('Era 2')
            ->setSlug('era-2')
            ->setImage('filename')
            ->setUniverse($universe)
            ->setStatus(EraStatus::IN_PROGRESS)
            ->setPosition(2);

        $manager->persist($era1);
        $manager->persist($era2);
        $manager->flush();

        $this->setReference('era', $era1);
        $this->setReference('era2', $era2);
    }

    public function getOrder(): int
    {
        return 2;
    }
}

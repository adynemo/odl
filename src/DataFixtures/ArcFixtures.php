<?php

namespace ODL\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use ODL\Entity\Arc;

class ArcFixtures extends Fixture implements OrderedFixtureInterface
{
    public const NEXT_POSITION = 3;

    public function load(ObjectManager $manager)
    {
        $period = $this->getReference('period');

        $arc1 = (new Arc())
            ->setTitle('First Arc')
            ->setContent('Description')
            ->setCover('filename')
            ->setPosition(1)
            ->setIsEvent(false)
            ->setIsValidated(true)
            ->setPeriod($period);

        $arc2 = (new Arc())
            ->setTitle('Second Arc')
            ->setContent('Description')
            ->setCover('filename')
            ->setPosition(2)
            ->setIsEvent(false)
            ->setIsValidated(true)
            ->setPeriod($period);

        $manager->persist($arc1);
        $manager->persist($arc2);
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 4;
    }
}

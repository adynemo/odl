<?php

namespace ODL\DataCollector;

use Doctrine\Common\Collections\ArrayCollection;

class Collection
{
    /**
     * @var string
     */
    private $entityName;
    /**
     * @var ArrayCollection
     */
    private $collection;

    public function __construct(array $entities, string $entityName)
    {
        $this->collection = new ArrayCollection($entities);
        $this->entityName = $entityName;
    }

    public function getCollection(): ArrayCollection
    {
        return $this->collection;
    }

    public function getEntityName(): string
    {
        return $this->entityName;
    }
}

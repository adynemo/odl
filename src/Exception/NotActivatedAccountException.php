<?php

namespace ODL\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

class NotActivatedAccountException extends AuthenticationException
{
    public function getMessageKey(): string
    {
        return 'not_activated_account_exception';
    }
}

<?php

namespace ODL\Exception;

class BadPositionException extends \Exception
{
    /**
     * @param string     $message  The internal exception message
     * @param \Throwable $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct(string $message = null, \Throwable $previous = null, int $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}

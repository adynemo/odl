<?php

namespace ODL\Command;

use ODL\Entity\User;
use ODL\Service\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Throwable;

class UpdatePasswordUserCommand extends Command
{
    protected static $defaultName = 'odl:user:update-password';

    private UserManager $userManager;
    private UserPasswordHasherInterface $passwordEncoder;

    protected function configure()
    {
        $this
            ->setDescription('Update a user password.')
            ->setHelp('This command allows you to update a user password.');
    }

    public function __construct(UserManager $userManager, UserPasswordHasherInterface $passwordEncoder, string $name = null)
    {
        parent::__construct($name);

        $this->userManager = $userManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $username = $io->ask('Give me your username, please');
        if (null === $username) {
            return Command::INVALID;
        }

        $password = $io->askHidden('And choose a password!');
        if (null === $password) {
            return Command::INVALID;
        }

        try {
            $user = $this->userManager->findOneBy(['username' => $username]);
            if ($user instanceof User) {
                $user->setPassword(
                    $this->passwordEncoder->hashPassword($user, $password)
                );
                $this->userManager->update($user);
            } else {
                $io->note('No users found with this username');

                return Command::SUCCESS;
            }
        } catch (Throwable $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }

        $io->success('Password updated!');

        return Command::SUCCESS;
    }
}

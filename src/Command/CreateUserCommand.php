<?php

namespace ODL\Command;

use ODL\Entity\User;
use ODL\Service\UserManager;
use ODL\Traits\EmailTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Throwable;

class CreateUserCommand extends Command
{
    use EmailTrait;

    protected static $defaultName = 'odl:user:create';

    private UserManager $userManager;
    private UserPasswordHasherInterface $passwordEncoder;

    protected function configure()
    {
        $this
            ->setDescription('Creates a new user.')
            ->setHelp('This command allows you to create a user.');
    }

    public function __construct(UserManager $userManager, UserPasswordHasherInterface $passwordEncoder, string $name = null)
    {
        parent::__construct($name);

        $this->userManager = $userManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $user = new User();

        $username = $io->ask('What\'s your username?');
        if (null === $username) {
            return Command::INVALID;
        }
        $user->setUsername($username);

        $email = $io->ask('Give me your email, please', null, [$this, 'checkEmail']);
        $user->setEmail($email);

        $isSuperAdmin = $io->confirm('Are you the Super Admin?');
        if ($isSuperAdmin) {
            $io->comment('Welcome, God!');
            $user->setRoles(['ROLE_SUPER_ADMIN']);
        }

        $password = $io->askHidden('And choose a password!');
        if (null === $password) {
            return Command::INVALID;
        }

        try {
            $this->userManager->create($user, $password, $this->passwordEncoder);
        } catch (Throwable $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }

        $io->success('User has been created!');

        return Command::SUCCESS;
    }
}

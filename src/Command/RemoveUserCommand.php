<?php

namespace ODL\Command;

use ODL\Entity\User;
use ODL\Service\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class RemoveUserCommand extends Command
{
    protected static $defaultName = 'odl:user:remove';

    private UserManager $userManager;

    protected function configure()
    {
        $this
            ->setDescription('Removes a user.')
            ->setHelp('This command allows you to remove a user.');
    }

    public function __construct(UserManager $userManager, string $name = null)
    {
        parent::__construct($name);

        $this->userManager = $userManager;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $username = $io->ask('Give me his username, please');
        if (null === $username) {
            return Command::INVALID;
        }

        $confirm = $io->confirm('Are you sure you want to remove this user?', false);
        if (false === $confirm) {
            $io->note('Abort!');

            return Command::SUCCESS;
        }

        try {
            $user = $this->userManager->findOneBy(['username' => $username]);
            if ($user instanceof User) {
                $this->userManager->remove($user);
            } else {
                $io->note('No users found with this username');

                return Command::SUCCESS;
            }
        } catch (Throwable $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }

        $io->success('User has been removed!');

        return Command::SUCCESS;
    }
}

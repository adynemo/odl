<?php

namespace ODL\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use ODL\Constant\JournalStatus;
use ODL\Entity\Traits\DateActionTrait;
use ODL\Repository\JournalRepository;
use RuntimeException;

/**
 * @ORM\Entity(repositoryClass=JournalRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Journal
{
    use DateActionTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $arcId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $readingOrder;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $period;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $newPosition = 0;

    /**
     * @ORM\Column(type="text")
     */
    private $title;

    /**
     * @ORM\Column(type="text", options={"default": ""})
     */
    private $newTitle = '';

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $cover = false;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $content = false;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $linkA = false;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $linkB = false;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $isEvent = 0;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(User $user): self
    {
        $this->username = $user->getUsername();

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if (!in_array($status, JournalStatus::ALLOWED_STATUS)) {
            throw new RuntimeException('You must pass a valid status');
        }
        $this->status = $status;

        return $this;
    }

    public function getArcId(): ?int
    {
        return $this->arcId;
    }

    public function setArcId(Arc $arc): self
    {
        $this->arcId = $arc->getId();

        return $this;
    }

    public function getReadingOrder(): ?string
    {
        return $this->readingOrder;
    }

    public function setReadingOrder(string $readingOrder): self
    {
        $this->readingOrder = $readingOrder;

        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(string $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getNewPosition(): ?int
    {
        return $this->newPosition;
    }

    public function setNewPosition(int $newPosition): self
    {
        $this->newPosition = $newPosition;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getNewTitle(): ?string
    {
        return $this->newTitle;
    }

    public function setNewTitle(string $newTitle): self
    {
        $this->newTitle = $newTitle;

        return $this;
    }

    public function getCover(): ?bool
    {
        return $this->cover;
    }

    public function setCover(bool $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(bool $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLinkA(): ?bool
    {
        return $this->linkA;
    }

    public function setLinkA(bool $linkA): self
    {
        $this->linkA = $linkA;

        return $this;
    }

    public function getLinkB(): ?bool
    {
        return $this->linkB;
    }

    public function setLinkB(bool $linkB): self
    {
        $this->linkB = $linkB;

        return $this;
    }

    public function getIsEvent(): ?int
    {
        return $this->isEvent;
    }

    public function setIsEvent(int $isEvent): self
    {
        $this->isEvent = $isEvent;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /** @ORM\PrePersist */
    public function onPrePersist()
    {
        $this->generateCreatedAt();
    }
}

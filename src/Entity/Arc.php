<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="ODL\Repository\ArcRepository")
 */
class Arc implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ODL\Entity\Period", inversedBy="arcs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $period;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=37)
     */
    private $cover;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="text", options={"default": ""})
     * @Assert\Type(type="string")
     */
    private $linkA = '';

    /**
     * @ORM\Column(type="text", options={"default": ""})
     * @Assert\Type(type="string")
     */
    private $linkB = '';

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isEvent;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isValidated = true;

    /**
     * @ORM\OneToMany(targetEntity=Reading::class, mappedBy="arc", orphanRemoval=true)
     */
    private $readers;

    public function __construct()
    {
        $this->readers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPeriod(): Period
    {
        return $this->period;
    }

    public function setIdPeriod(Period $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLinkA(): ?string
    {
        return $this->linkA;
    }

    public function setLinkA(string $linkA): self
    {
        $this->linkA = $linkA;

        return $this;
    }

    public function getLinkB(): ?string
    {
        return $this->linkB;
    }

    public function setLinkB(string $linkB): self
    {
        $this->linkB = $linkB;

        return $this;
    }

    public function getIsEvent(): ?bool
    {
        return $this->isEvent;
    }

    public function setIsEvent(bool $isEvent): self
    {
        $this->isEvent = $isEvent;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPeriod(): ?Period
    {
        return $this->period;
    }

    public function setPeriod(?Period $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'period' => $this->period,
            'position' => $this->position,
            'title' => $this->title,
            'content' => $this->content,
            'cover' => $this->cover,
            'linkA' => $this->linkA,
            'linkB' => $this->linkB,
            'isEvent' => $this->isEvent,
        ];
    }

    /**
     * @return Collection|Reading[]
     */
    public function getReaders(): Collection
    {
        return $this->readers;
    }

    public function addReader(Reading $reader): self
    {
        if (!$this->readers->contains($reader)) {
            $this->readers[] = $reader;
            $reader->setArc($this);
        }

        return $this;
    }

    public function removeReader(Reading $reader): self
    {
        if ($this->readers->removeElement($reader)) {
            // set the owning side to null (unless already changed)
            if ($reader->getArc() === $this) {
                $reader->setArc(null);
            }
        }

        return $this;
    }
}

<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use ODL\Constant\EraStatus;
use RuntimeException;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="ODL\Repository\EraRepository")
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"universe_id", "position", "enabled_unique_key"}),
 *     @ORM\UniqueConstraint(columns={"universe_id", "name"}),
 *     @ORM\UniqueConstraint(columns={"universe_id", "slug"})
 * })
 * @ORM\EntityListeners({"ODL\EventListener\SectionPositionUniqueConstraintListener"})
 * @UniqueEntity(
 *     fields={"universe", "position", "enabledUniqueKey"},
 *     errorPath="position",
 *     message="This position is already in use on that universe."
 * )
 */
class Era extends Section implements JsonSerializable
{
    /**
     * @ORM\ManyToOne(targetEntity="ODL\Entity\Universe", inversedBy="eras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $universe;

    /**
     * @ORM\Column(type="string", length=37)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="ODL\Entity\Period", mappedBy="era", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $periods;

    /**
     * @ORM\Column(type="string", length=37, options={"default": ""})
     */
    private $logoLinkA = '';

    /**
     * @ORM\Column(type="string", length=37, options={"default": ""})
     */
    private $logoLinkB = '';
    /**
     * @ORM\Column(type="smallint", options={"default": EraStatus::IN_PROGRESS})
     */
    private $status = EraStatus::IN_PROGRESS;

    public function __construct()
    {
        $this->periods = new ArrayCollection();
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Period[]
     */
    public function getPeriods(): Collection
    {
        return $this->periods;
    }

    public function addPeriod(Period $period): self
    {
        if (!$this->periods->contains($period)) {
            $this->periods[] = $period;
            $period->setEra($this);
        }

        return $this;
    }

    public function removePeriod(Period $period): self
    {
        if ($this->periods->contains($period)) {
            $this->periods->removeElement($period);
            // set the owning side to null (unless already changed)
            if ($period->getEra() === $this) {
                $period->setEra(null);
            }
        }

        return $this;
    }

    public function getUniverse(): ?Universe
    {
        return $this->universe;
    }

    public function setUniverse(?Universe $universe): self
    {
        $this->universe = $universe;

        return $this;
    }

    public function getLogoLinkA(): ?string
    {
        return $this->logoLinkA;
    }

    public function setLogoLinkA(string $logoLinkA): self
    {
        $this->logoLinkA = $logoLinkA;

        return $this;
    }

    public function getLogoLinkB(): ?string
    {
        return $this->logoLinkB;
    }

    public function setLogoLinkB(string $logoLinkB): self
    {
        $this->logoLinkB = $logoLinkB;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        if (!in_array($status, EraStatus::ALLOWED_STATUS)) {
            throw new RuntimeException('You must pass a valid status');
        }
        $this->status = $status;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'universe' => $this->universe,
            'position' => $this->position,
            'name' => $this->name,
            'slug' => $this->slug,
            'image' => $this->image,
            'periods' => $this->periods,
            'logoLinkA' => $this->logoLinkA,
            'logoLinkB' => $this->logoLinkB,
        ];
    }
}

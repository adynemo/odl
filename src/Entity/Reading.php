<?php

namespace ODL\Entity;

use Doctrine\ORM\Mapping as ORM;
use ODL\Entity\Traits\DateActionTrait;
use ODL\Repository\ReadingRepository;

/**
 * @ORM\Entity(repositoryClass=ReadingRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Reading
{
    use DateActionTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="readings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Arc::class, inversedBy="readers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $arc;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getArc(): ?Arc
    {
        return $this->arc;
    }

    public function setArc(?Arc $arc): self
    {
        $this->arc = $arc;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /** @ORM\PrePersist */
    public function onPrePersist()
    {
        $this->generateCreatedAt();
    }
}

<?php

namespace ODL\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use ODL\Repository\RateRepository;

/**
 * @ORM\Entity(repositoryClass=RateRepository::class)
 */
class Rate implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Arc::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Arc $arc;

    /**
     * @ORM\Column(type="integer")
     */
    private int $value;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private ?User $user;

    public function getId(): int
    {
        return $this->id;
    }

    public function getArc(): Arc
    {
        return $this->arc;
    }

    public function setArc(Arc $arc): self
    {
        $this->arc = $arc;

        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'arc' => $this->arc->getId(),
            'value' => $this->value,
            'user' => $this->user ? $this->user->getId() : '',
        ];
    }
}

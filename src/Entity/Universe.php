<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="universe")
 * @ORM\Entity(repositoryClass="ODL\Repository\UniverseRepository")
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"position", "enabled_unique_key"}),
 *     @ORM\UniqueConstraint(columns={"name"}),
 *     @ORM\UniqueConstraint(columns={"slug"})
 * })
 * @ORM\EntityListeners({"ODL\EventListener\SectionPositionUniqueConstraintListener"})
 * @UniqueEntity(
 *     fields={"position", "enabledUniqueKey"},
 *     errorPath="position",
 *     message="This position is already in use."
 * )
 */
class Universe extends Section implements JsonSerializable
{
    /**
     * @ORM\OneToMany(targetEntity="ODL\Entity\Era", mappedBy="universe", cascade={"remove"})
     */
    private $eras;

    /**
     * @ORM\Column(type="string", length=37, options={"default": ""})
     */
    private $logo = '';

    public function __construct()
    {
        $this->eras = new ArrayCollection();
    }

    /**
     * @return Collection|Era[]
     */
    public function getEras(): Collection
    {
        return $this->eras;
    }

    public function addEra(Era $era): self
    {
        if (!$this->eras->contains($era)) {
            $this->eras[] = $era;
            $era->setUniverse($this);
        }

        return $this;
    }

    public function removeEra(Era $era): self
    {
        if ($this->eras->contains($era)) {
            $this->eras->removeElement($era);
            // set the owning side to null (unless already changed)
            if ($era->getUniverse() === $this) {
                $era->setUniverse(null);
            }
        }

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'position' => $this->position,
            'name' => $this->name,
            'slug' => $this->slug,
            'logo' => $this->logo,
        ];
    }
}

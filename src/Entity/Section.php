<?php

namespace ODL\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class Section
{
    public const ENABLED_CONSTRAINT_DB_VALUE = 1;
    public const ENABLED_CONSTRAINT_FIELD_NAME = 'enabledUniqueKey';
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=13)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="ODL\Util\RandomIdGenerator")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $slug;
    /**
     * @ORM\Column(type="integer")
     */
    protected $position;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;
    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": "1"})
     */
    protected $enabledUniqueKey = self::ENABLED_CONSTRAINT_DB_VALUE;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function disableUniqueKey(): void
    {
        $this->enabledUniqueKey = null;
    }

    public function enableUniqueKey(): void
    {
        $this->enabledUniqueKey = self::ENABLED_CONSTRAINT_DB_VALUE;
    }
}

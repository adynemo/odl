<?php

namespace ODL\Entity\Traits;

trait DateActionTrait
{
    /**
     * @return self
     */
    public function generateCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return self
     */
    public function generateUpdatedAt()
    {
        if (!property_exists($this, 'updatedAt')) {
            throw new \RuntimeException('No updateAt property');
        }

        $this->updatedAt = new \DateTimeImmutable();

        return $this;
    }
}

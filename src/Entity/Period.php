<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="ODL\Repository\PeriodRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"era_id", "position", "enabled_unique_key"})})
 * @ORM\EntityListeners({"ODL\EventListener\SectionPositionUniqueConstraintListener"})
 * @UniqueEntity(
 *     fields={"era", "position", "enabledUniqueKey"},
 *     errorPath="position",
 *     message="This position is already in use on that era."
 * )
 */
class Period extends Section implements JsonSerializable
{
    /**
     * @ORM\ManyToOne(targetEntity="ODL\Entity\Era", inversedBy="periods")
     * @ORM\JoinColumn(nullable=false)
     */
    private $era;

    /**
     * @ORM\OneToMany(targetEntity="ODL\Entity\Arc", mappedBy="period", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $arcs;

    public function __construct()
    {
        $this->arcs = new ArrayCollection();
    }

    /**
     * @return Collection|Arc[]
     */
    public function getArcs(): Collection
    {
        return $this->arcs;
    }

    public function addArc(Arc $arc): self
    {
        if (!$this->arcs->contains($arc)) {
            $this->arcs[] = $arc;
            $arc->setIdPeriod($this);
        }

        return $this;
    }

    public function removeArc(Arc $arc): self
    {
        if ($this->arcs->contains($arc)) {
            $this->arcs->removeElement($arc);
            // set the owning side to null (unless already changed)
            if ($arc->getPeriod() === $this) {
                $arc->setPeriod(null);
            }
        }

        return $this;
    }

    public function getEra(): ?Era
    {
        return $this->era;
    }

    public function setEra(?Era $era): self
    {
        $this->era = $era;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'era' => $this->era,
            'position' => $this->position,
            'name' => $this->name,
            'slug' => $this->slug,
            'arcs' => $this->arcs,
        ];
    }
}

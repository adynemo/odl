<?php

namespace ODL\Event;

use ODL\Entity\Arc;
use Symfony\Contracts\EventDispatcher\Event;

class ArcEvent extends Event
{
    public const CREATED = 'arc.created';
    public const PENDING = 'arc.pending';
    public const UPDATED = 'arc.updated';
    public const REMOVED = 'arc.removed';
    public const ALLOWED_EVENT = [self::CREATED, self::PENDING, self::UPDATED, self::REMOVED];

    /**
     * @var Arc
     */
    protected $arc;

    /**
     * @var ?Arc
     */
    protected $oldArc;

    public function __construct(Arc $arc, ?Arc $oldArc = null)
    {
        $this->arc = $arc;
        $this->oldArc = $oldArc;
    }

    public function getArc(): Arc
    {
        return $this->arc;
    }

    public function getOldArc(): ?Arc
    {
        return $this->oldArc;
    }
}

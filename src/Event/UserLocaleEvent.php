<?php

namespace ODL\Event;

use ODL\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;

class UserLocaleEvent extends Event
{
    public const UPDATED = 'user.locale.updated';

    protected User $user;
    private Request $request;

    public function __construct(User $user, Request $request)
    {
        $this->user = $user;
        $this->request = $request;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }
}

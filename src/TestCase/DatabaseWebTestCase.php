<?php

namespace ODL\TestCase;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DatabaseWebTestCase extends WebTestCase
{
    /**
     * @var ContainerInterface
     */
    protected static $container;
    protected EntityManager $entityManager;
    protected KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();
        self::$container = $this->client->getContainer();

        $this->entityManager = self::$container
            ->get('doctrine')
            ->getManager();

        $this->entityManager->beginTransaction();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if (true === $this->entityManager->getConnection()->isTransactionActive()) {
            // doing this is recommended to avoid memory leaks
            $this->entityManager->rollback();
        }
        $this->entityManager->close();
        unset($this->entityManager);
    }
}

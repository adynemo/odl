<?php

namespace ODL\Subscriber;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Constant\JournalStatus;
use ODL\Entity\Arc;
use ODL\Entity\Journal;
use ODL\Event\ArcEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ArcSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserInterface|null
     */
    private $user;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->user = $security->getUser();
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ArcEvent::CREATED => 'onCreate',
            ArcEvent::PENDING => 'onCreate',
            ArcEvent::UPDATED => 'onUpdate',
            ArcEvent::REMOVED => 'onRemove',
        ];
    }

    public function onCreate(ArcEvent $event)
    {
        $arc = $event->getArc();

        $status = (false === $arc->getIsValidated()) ? JournalStatus::PENDING : JournalStatus::CREATED;
        $journal = new Journal();
        $journal->setUsername($this->user)
            ->setStatus($status)
            ->setArcId($arc)
            ->setReadingOrder($this->getReadingOrder($arc))
            ->setPeriod($arc->getPeriod()->getName())
            ->setTitle($arc->getTitle())
            ->setPosition($arc->getPosition())
            ->setIsEvent($arc->getIsEvent());

        $this->save($journal);
    }

    public function onUpdate(ArcEvent $event)
    {
        $arc = $event->getArc();
        $oldArc = $event->getOldArc();

        $journal = new Journal();
        $journal->setUsername($this->user)
            ->setStatus(JournalStatus::UPDATED)
            ->setArcId($arc)
            ->setReadingOrder($this->getReadingOrder($arc))
            ->setPeriod($arc->getPeriod()->getName())
            ->setTitle($oldArc->getTitle())
            ->setNewTitle($arc->getTitle())
            ->setPosition($oldArc->getPosition())
            ->setNewPosition($arc->getPosition())
            ->setIsEvent($arc->getIsEvent());

        $this->compare([$arc, 'getContent'], [$oldArc, 'getContent'], [$journal, 'setContent']);
        $this->compare([$arc, 'getCover'], [$oldArc, 'getCover'], [$journal, 'setCover']);
        $this->compare([$arc, 'getLinkA'], [$oldArc, 'getLinkA'], [$journal, 'setLinkA']);
        $this->compare([$arc, 'getLinkB'], [$oldArc, 'getLinkB'], [$journal, 'setLinkB']);

        $this->save($journal);
    }

    public function onRemove(ArcEvent $event)
    {
        $arc = $event->getArc();

        $journal = new Journal();
        $journal->setUsername($this->user)
            ->setStatus(JournalStatus::REMOVED)
            ->setArcId($arc)
            ->setReadingOrder($this->getReadingOrder($arc))
            ->setPeriod($arc->getPeriod()->getName())
            ->setTitle($arc->getTitle())
            ->setPosition($arc->getPosition());

        $this->save($journal);
    }

    private function getReadingOrder(Arc $arc): string
    {
        return $arc->getPeriod()->getEra()->getUniverse()->getName().' / '.$arc->getPeriod()->getEra()->getName();
    }

    private function save(Journal $journal): void
    {
        $this->entityManager->persist($journal);
        $this->entityManager->flush();
    }

    private function compare(callable $new, callable $old, callable $journal)
    {
        if (call_user_func($new) !== call_user_func($old)) {
            call_user_func($journal, true);
        }
    }
}

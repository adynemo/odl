<?php

namespace ODL\Subscriber;

use ODL\Entity\User;
use ODL\Event\UserLocaleEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * Stores the locale of the user in the session after the
 * login. This can be used by the LocaleSubscriber afterwards.
 */
class UserLocaleSubscriber implements EventSubscriberInterface
{
    private string $defaultLocale;

    public function __construct(string $defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'onInteractiveLogin',
            KernelEvents::REQUEST => [['onKernelRequest', 20]],
            UserLocaleEvent::UPDATED => 'onUpdateUser',
        ];
    }

    public function onInteractiveLogin(InteractiveLoginEvent $event): void
    {
        /**
         * @var User $user
         */
        $user = $event->getAuthenticationToken()->getUser();

        if ('' !== $user->getLocale()) {
            $event->getRequest()->getSession()->set('_locale', $user->getLocale());
        }
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }

        $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
    }

    public function onUpdateUser(UserLocaleEvent $event): void
    {
        $user = $event->getUser();
        $session = $event->getRequest()->getSession();

        if ('' === $user->getLocale() && $session->has('_locale')) {
            $session->remove('_locale');

            return;
        }

        $session->set('_locale', $user->getLocale());
    }
}

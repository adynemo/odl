#! /bin/sh

PIPELINE=false

Help()
{
   echo "Runs project's unit tests."
   echo
   echo "Syntax: scriptTemplate [-c|h]"
   echo "options:"
   echo "p     Runs without colors, for pipelines."
   echo "h     Print this Help."
   echo
   exit 1
}

while getopts ":hp" o; do
    case "${o}" in
        h)
            Help
            ;;
        p)
            PIPELINE=true
            ;;
        *)
            Help
            ;;
    esac
done

bin/console doctrine:schema:drop --force --env=test
bin/console doctrine:schema:create --env=test
bin/console doctrine:fixtures:load -n --env=test

if $PIPELINE == "true"; then
  bin/phpunit --colors=never -c phpunit.ci.xml
else
  bin/phpunit
fi

PHPUNIT_RESULT=$?

# Check results
exit ${PHPUNIT_RESULT}

<div align="center">

# <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/15129536/190718033628504.jpg?width=20" alt="logo"> ODL

[![lang: PHP 7.4](https://img.shields.io/badge/PHP-7.4-777bb4.svg?style=for-the-badge&logo=php)](http://php.net/manual/migration74.php)
[![framework: Symfony5.4](https://img.shields.io/badge/Symfony-5.4-black?style=for-the-badge&logo=symfony)](https://symfony.com/doc/5.4/index.html)

[![pipeline status](https://gitlab.com/adynemo/odl/badges/master/pipeline.svg)](https://gitlab.com/adynemo/odl/-/commits/master)
[![coverage report](https://gitlab.com/adynemo/odl/badges/master/coverage.svg)](https://gitlab.com/adynemo/odl/-/commits/master)
[![Violinist enabled](https://img.shields.io/badge/violinist-enabled-brightgreen.svg)](https://violinist.io)
[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/5078/badge)](https://bestpractices.coreinfrastructure.org/projects/5078)
</div>

## 📓 Description

ODL means Ordre De Lecture in french, reading order in english.

This is a CMS to create a website for comics reading orders (or books).

<div align="center">

<img src="./docs/demo.png" alt="demo">
</div>

## 🏗️ Installation in production

### Prerequisites

|   Stack    |
|:----------:|
|  PHP7.4+   |
|   MySQL    |
| composer 2 |

### Installation

1. Download the [latest release](https://gitlab.com/adynemo/odl/-/releases) or git clone the project.
2. Copy `./.env` to `./.env.local` and fill following parameters.
    - Requirements:
        - APP_SECRET
        - DATABASE_URL
        - MAILER_DSN

    - Optionals:
        - LOCALE_LANGUAGE (default: en)
        - SENDER_EMAIL (used for error logging sent by email)
        - RECIPIENT_EMAIL (used for error logging sent by email)

3. `composer install --no-dev --prefer-dist` from the project root in order to install composer libs and bundles.

### Create schema and run migrations

From the project root, run the following commands:

```shell
php bin/console doctrine:schema:create
```

## 🎟 Issues

This project is always in progress. It works well but it may contain bugs and some features are missing. The installation is not yet easy for everyone. This is part of the next improvements.

You can see [the to-do list and work in progress](https://gitlab.com/adynemo/odl/-/boards) or [submit ideas and report bugs](https://gitlab.com/adynemo/odl/-/issues/new?issue).

## 💻 Contribute

Please, read the [CONTRIBUTING](./CONTRIBUTING.md) documentation.

## 🗣 Available languages

- 🇫🇷 French
- 🇬🇧 English

## 📜 License

[GNU General Public License v2](http://opensource.org/licenses/gpl-2.0.php)

## 🤝 Support

Thanks to [JetBrains](https://jb.gg/OpenSource) to support this project with an Open Source License.

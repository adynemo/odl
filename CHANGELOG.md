# Changelog


### [4.2.2](https://gitlab.com/adynemo/odl/compare/v4.2.1...v4.2.2) (2022-03-22)


### 🐛 Bug Fixes

* see ratings is must be allowed for anonymous users ([ee83533](https://gitlab.com/adynemo/odl/commit/ee8353334dbdd888f2d6257eed7ebacc044be4b4))

### [4.2.1](https://gitlab.com/adynemo/odl/compare/v4.2.0...v4.2.1) (2022-03-21)


### 🐛 Bug Fixes

* section doesn't open for not logged user ([c9941d9](https://gitlab.com/adynemo/odl/commit/c9941d948cfe2d49642004911c41b4c7b8fc7919))

## [4.2.0](https://gitlab.com/adynemo/odl/compare/v4.1.9...v4.2.0) (2022-03-21)


### ♻ Refactor

* more jQuery free ([da30982](https://gitlab.com/adynemo/odl/commit/da3098266acc4df2536f6bfb0cddac01a2191135))


### 🐛 Bug Fixes

* **JS:** deprecated event usage ([9eb126a](https://gitlab.com/adynemo/odl/commit/9eb126a7c4b48974f85e5aedc30416d5565445f7))


### 🎉 Features

* allow user to rate arcs + display averages ([73424e2](https://gitlab.com/adynemo/odl/commit/73424e22c7069f6056adff5d53bbfc6cf50df165))
* implement user ratings on arcs ([4273b99](https://gitlab.com/adynemo/odl/commit/4273b998031e59d1afd875d725d8054890dc1820))


### ✨ Improvements

* allow to unrate and improve UI ([431d1c2](https://gitlab.com/adynemo/odl/commit/431d1c28e62c3d04f2b795de7ca3e7a8d475f374))
* allow user to get arc's unique URL like anchor ([d551b18](https://gitlab.com/adynemo/odl/commit/d551b188409e35855cc34b560975b46c8d782074))
* make dark-mode easier and load it earlier ([14f43fa](https://gitlab.com/adynemo/odl/commit/14f43fad03d4f2ce086c6d6a7c9f6fcd6dd7bdff))


### 🏗️ Build system

* **deps-dev:** Update dama/doctrine-test-bundle from v6.7.4 to v6.7.5 ([af1f6e9](https://gitlab.com/adynemo/odl/commit/af1f6e9c62d07e457de90fce69e3c50691f71e9d))
* **deps-dev:** Update friendsofphp/php-cs-fixer from v3.5.0 to v3.6.0 ([182928a](https://gitlab.com/adynemo/odl/commit/182928a0e4745998439f360cadeadb3d0ba483e1))
* **deps-dev:** Update friendsofphp/php-cs-fixer from v3.6.0 to v3.7.0 ([861c0a4](https://gitlab.com/adynemo/odl/commit/861c0a4633f11682e90405a8b40e5f62ab9c3d47))
* **deps-dev:** Update phpstan/phpstan from 1.4.3 to 1.4.5 ([1cddb6a](https://gitlab.com/adynemo/odl/commit/1cddb6af3951aca0bea75586e97b0706d7cf1755))
* **deps-dev:** Update phpstan/phpstan from 1.4.5 to 1.4.6 ([6ad41ef](https://gitlab.com/adynemo/odl/commit/6ad41ef45d376174e822ef2ef987ba0cc1edae36))
* **deps-dev:** Update phpstan/phpstan from 1.4.6 to 1.4.8 ([a9f8618](https://gitlab.com/adynemo/odl/commit/a9f86188b769a210457468b3b9315e56707b88c7))
* **deps-dev:** Update phpstan/phpstan from 1.4.8 to 1.4.10 ([0084212](https://gitlab.com/adynemo/odl/commit/008421207a7fad9f583f2e537f184995467c660a))
* **deps-dev:** Update symfony/maker-bundle from v1.36.4 to v1.38.0 ([f8372a8](https://gitlab.com/adynemo/odl/commit/f8372a84c5aa1ba80d5692a033c7c5e3a74f092f))
* **deps:** Update symfony/flex from v1.18.3 to v1.18.5 ([e6033ab](https://gitlab.com/adynemo/odl/commit/e6033ab9dfe4fb07c5a4ca4bf84fbded9e97a2ad))
* **deps:** Update symfony/framework-bundle from v5.4.3 to v5.4.6 ([1e94e00](https://gitlab.com/adynemo/odl/commit/1e94e00c032522e24036f9867ced737fad710544))
* update dependencies ([700b578](https://gitlab.com/adynemo/odl/commit/700b5784ff9d31855d7d2a6f01fe7ccde0a68f14))

### [4.1.9](https://gitlab.com/adynemo/odl/compare/v4.1.8...v4.1.9) (2022-01-29)


### 🏗️ Build system

* update maintenance-bundle ([e6752d7](https://gitlab.com/adynemo/odl/commit/e6752d776799f38a859606cd8abe4a69ba4e1288))

### [4.1.8](https://gitlab.com/adynemo/odl/compare/v4.1.7...v4.1.8) (2022-01-29)


### 🏗️ Build system

* update maintenance-bundle ([2bb60d6](https://gitlab.com/adynemo/odl/commit/2bb60d655e6113aedff89aca5bf4bda987c0f3c6))

### [4.1.7](https://gitlab.com/adynemo/odl/compare/v4.1.6...v4.1.7) (2022-01-29)


### 🏗️ Build system

* remove useless config/bootstrap.php ([88bcc95](https://gitlab.com/adynemo/odl/commit/88bcc95c99b7f21bf8b3384d0ea8fd71b7285c7a))

### [4.1.6](https://gitlab.com/adynemo/odl/compare/v4.1.5...v4.1.6) (2022-01-29)


### 🏗️ Build system

* update vendors ([2497e2a](https://gitlab.com/adynemo/odl/commit/2497e2a8663455b30e8d0fa26e8cac3761a6ab26))

### [4.1.5](https://gitlab.com/adynemo/odl/compare/v4.1.4...v4.1.5) (2022-01-29)


### 🐛 Bug Fixes

* broken users admin page ([e01e64e](https://gitlab.com/adynemo/odl/commit/e01e64e2725a5d3ac05320edfc168457d5f2cf5b))


### 🏃 Performance

* load dark-mode js quickly and earlier ([6fea498](https://gitlab.com/adynemo/odl/commit/6fea4987e469b47642fcca06c5e31d52e482e8cd))


### 🏗️ Build system

* **deps-dev:** Update dama/doctrine-test-bundle from v6.7.3 to v6.7.4 ([27859c1](https://gitlab.com/adynemo/odl/commit/27859c19e008bbedc8a3b8cb4efbbdb8c9252aa4))
* **deps-dev:** Update phpstan/phpstan from 1.4.2 to 1.4.3 ([1512dfc](https://gitlab.com/adynemo/odl/commit/1512dfc9b4e7971dd6d693ec3cf83f2f32433e46))
* **deps:** Update ady/maintenance-bundle from v3.0.1 to v3.0.2 ([27ce7a7](https://gitlab.com/adynemo/odl/commit/27ce7a74a08336873bb8c5a39a618a8e07546ec4))
* **deps:** Update symfony/flex from v1.18.1 to v1.18.3 ([638c6ca](https://gitlab.com/adynemo/odl/commit/638c6ca2082cb1f3d46f15af2d13e2a3c8059d9e))
* **deps:** Update symfony/framework-bundle from v5.4.2 to v5.4.3 ([27caf1e](https://gitlab.com/adynemo/odl/commit/27caf1e17f0d04354dd0d41149b73cf5f5e5b9eb))
* update violonist config ([916b3e4](https://gitlab.com/adynemo/odl/commit/916b3e4576f69c6375f8a208f53359bd8ec26e77))

### [4.1.4](https://gitlab.com/adynemo/odl/compare/v4.1.3...v4.1.4) (2022-01-22)


### 🧩 CI

* disable mailer and force test env ([6d2e548](https://gitlab.com/adynemo/odl/commit/6d2e54822cccbed411a6561e0325fa5834dd1a6b))
* fix PHP image extension for 7.4 ([f569ea9](https://gitlab.com/adynemo/odl/commit/f569ea90dd532d473c943d13e7788ffea8ba6eb3))
* use PHP7.4 image ([21bf239](https://gitlab.com/adynemo/odl/commit/21bf2392b926df74590a60bd0459a0329c173dca))


### 🩺 Tests

* fix bad params usage for UniverseManager ([c5e1a03](https://gitlab.com/adynemo/odl/commit/c5e1a03730e5e054b34aa195eebfa560f23cd59c))
* increase max attempts for login throttling ([40c7721](https://gitlab.com/adynemo/odl/commit/40c7721f3d20b82e317210c081797da3e908c26c))


### ✨ Improvements

* prettify all dropdown fields ([af302bc](https://gitlab.com/adynemo/odl/commit/af302bc92f0b1f75cfb30e9c81fcf41394e10b2a))
* **SEO:** use slugs in routes instead of id ([9e116ee](https://gitlab.com/adynemo/odl/commit/9e116ee387fdd026c1d77c6b83d2b46b359857b3))


### 🏗️ Build system

* **deps:** Update symfony/flex from v1.17.6 to v1.18.1 ([8fbf407](https://gitlab.com/adynemo/odl/commit/8fbf407f29a45730fd427f6e385ff56d847f1064))
* remove SwiftMailer ([503f182](https://gitlab.com/adynemo/odl/commit/503f182df5e2cffbecb2d1575367f6fb9c1754a5))
* upgrade to PHP7.4 and use composer image ([6ccf89f](https://gitlab.com/adynemo/odl/commit/6ccf89f2872dc9e86d2219959b39b96af82db9b1))
* upgradre to Symfony 5.4 and update vendors ([6571362](https://gitlab.com/adynemo/odl/commit/6571362d811ec06aa645ee5d15a5ad8154615cc3))


### ♻ Refactor

* fix several deprecations ([23eb9dc](https://gitlab.com/adynemo/odl/commit/23eb9dce47de00528a51a70acfa4efdfbb55b533))
* improve dark mode and use inverted Fomantic class ([04007bc](https://gitlab.com/adynemo/odl/commit/04007bce49e3ad6cd4772b3f27948046be5756d3))
* move migrations after Symfony upgrade ([838d538](https://gitlab.com/adynemo/odl/commit/838d5380cfe9e65febfc8a91f78c0cd5701c6c32))
* replace deprecated common ManagerRegistry ([b26bb6b](https://gitlab.com/adynemo/odl/commit/b26bb6bbeb2a0fccd728f3ec144fde938ac79609))
* **Security:** use the new Symfony system ([b87d8b7](https://gitlab.com/adynemo/odl/commit/b87d8b71d188e41812aaeee1f5f4cb8d4b86ed93))
* upgrade config to Symfony 5.4 ([a77b1e5](https://gitlab.com/adynemo/odl/commit/a77b1e5f03975b85f5514e96caae62496faf4bef))

### [4.1.3](https://gitlab.com/adynemo/odl/compare/v4.1.2...v4.1.3) (2021-12-21)


### ✨ Improvements

* add pagination to the journal ([0fcb3c9](https://gitlab.com/adynemo/odl/commit/0fcb3c9d9315c3dff2c9c9b05b067d81644d9de8))
* better dark mode handling and check users prefers ([71fadb3](https://gitlab.com/adynemo/odl/commit/71fadb34d77a1cbdf37605499b6b2e009878e627))


### 🐛 Bug Fixes

* corrects label color for progress bar in dark mode ([eb0406e](https://gitlab.com/adynemo/odl/commit/eb0406e3cc1eb668944c7b33b2a679e7ae08f791))

### [4.1.2](https://gitlab.com/adynemo/odl/compare/v4.1.1...v4.1.2) (2021-12-21)


### ✨ Improvements

* add divider between each section form ([8da980e](https://gitlab.com/adynemo/odl/commit/8da980e93e6ac8f9c1e32c5c7c45dd596c939735))


### 🏗️ Build system

* **deps-dev:** Update symfony/phpunit-bridge from v5.3.10 to v5.4.0 ([95789f0](https://gitlab.com/adynemo/odl/commit/95789f0cccac18bd88a095e88d36e52dbabae41c))
* **deps-dev:** Update symfony/phpunit-bridge from v5.3.8 to v5.3.10 ([396125d](https://gitlab.com/adynemo/odl/commit/396125d5ba5cea93ffcf111ea8beb26308469cd4))
* **deps:** Update friendsofsymfony/jsrouting-bundle from 2.7.0 to 2.8.0 ([d097bd9](https://gitlab.com/adynemo/odl/commit/d097bd94b8c604e4a2a7a4a4078c665768c66349))
* **deps:** Update symfony/flex from v1.17.1 to v1.17.2 ([d17a663](https://gitlab.com/adynemo/odl/commit/d17a6636a0ed161384130bdf83b3182a9565d167))
* **deps:** Update symfony/flex from v1.17.2 to v1.17.6 ([3b09f0d](https://gitlab.com/adynemo/odl/commit/3b09f0d054f4c9414fb3449422c5d9e3589a68ca))
* **deps:** Update twig/extra-bundle from v3.3.3 to v3.3.4 ([3a688ab](https://gitlab.com/adynemo/odl/commit/3a688abd190006a5a1a59bfb7414fbbd94e38a64))
* **deps:** Update twig/twig from v3.3.3 to v3.3.4 ([f8c543d](https://gitlab.com/adynemo/odl/commit/f8c543d9a6a44bb99a1e081ed267fda76b414752))
* update composer libs ([3e2a664](https://gitlab.com/adynemo/odl/commit/3e2a664dab644ee45e54193eda6508fe7fbb3260))
* update front libs ([cee3f67](https://gitlab.com/adynemo/odl/commit/cee3f67cd2530ea43a59905a347e3ddc1eec642a))
* update symfony/maker-bundle and remove composer PVD ([5c06653](https://gitlab.com/adynemo/odl/commit/5c06653af46589c620709f9aa4a88b593ff542c1))


### 🐛 Bug Fixes

* avoid position violation due to multiple updates ([47c3acf](https://gitlab.com/adynemo/odl/commit/47c3acf93580f65dfb897aa2ed3c8aaf22a0026b))
* corrects division by zero on percent function ([ff30c23](https://gitlab.com/adynemo/odl/commit/ff30c23991cf854c1cfe8455c44fcb40b5993422))

### [4.1.1](https://gitlab.com/adynemo/odl/compare/v4.1.0...v4.1.1) (2021-10-17)


### ✨ Improvements

* add description to each section ([798bc0d](https://gitlab.com/adynemo/odl/commit/798bc0de8a19c44dfa684d1f0d5bcf04b1ef4e70))
* add more stats on reading progress page ([5ca0d2e](https://gitlab.com/adynemo/odl/commit/5ca0d2e590d030f1a7bba4545ceddcc192d8f532))
* add status field to Era ([21c5bcf](https://gitlab.com/adynemo/odl/commit/21c5bcfbb07546399a66bf2f2f82b6bbd8fce64a))


### ♻ Refactor

* factorize form types of sections ([cfe2a20](https://gitlab.com/adynemo/odl/commit/cfe2a207cb51ed975c5fd706cc1292bdd8af37aa))

## [4.1.0](https://gitlab.com/adynemo/odl/compare/v4.0.8...v4.1.0) (2021-10-16)


### 🎉 Features

* display progress by reading orders ([585b7d4](https://gitlab.com/adynemo/odl/commit/585b7d4bea2425f7410f3b501e148213388d9f19))


### ✨ Improvements

* add missing data to journal for updates ([fe30b68](https://gitlab.com/adynemo/odl/commit/fe30b68eefd510721f22333ec21b00b290509e9e))


### 🏗️ Build system

* **deps-dev:** Update phpstan/phpstan from 0.12.96 to 0.12.98 ([039c80b](https://gitlab.com/adynemo/odl/commit/039c80b63a587b521362ffbcbf56bce0cdfe9b66))
* **deps-dev:** Update phpstan/phpstan from 0.12.98 to 0.12.99 ([fceaf50](https://gitlab.com/adynemo/odl/commit/fceaf509c9bdbd49efaca1a2d9d407beb6a7954a))
* **deps-dev:** Update symfony/maker-bundle from v1.33.0 to v1.34.0 ([7f9b46d](https://gitlab.com/adynemo/odl/commit/7f9b46d55061d67fceba5678485db311f7842919))
* **deps-dev:** Update symfony/phpunit-bridge from v5.3.4 to v5.3.7 ([ff3d8db](https://gitlab.com/adynemo/odl/commit/ff3d8dbfe3a9574878422b8958594da883260153))
* **deps-dev:** Update symfony/phpunit-bridge from v5.3.7 to v5.3.8 ([8aeb768](https://gitlab.com/adynemo/odl/commit/8aeb768cee4cc7c446d7f737a1963e7e0710e37f))
* **deps:** Update composer/package-versions-deprecated from 1.11.99.2 to 1.11.99.4 ([ebcf10f](https://gitlab.com/adynemo/odl/commit/ebcf10f925049bdcce343929f0434b17751df39e))
* **deps:** Update ramsey/uuid from 4.2.1 to 4.2.3 ([2d3e971](https://gitlab.com/adynemo/odl/commit/2d3e9714815d2d74a75f79992aae2ce4b069699d))
* **deps:** Update symfony/flex from v1.14.1 to v1.15.1 ([7fefc2d](https://gitlab.com/adynemo/odl/commit/7fefc2d913c2878ae0fda39b6c989e2c2296b2df))
* **deps:** Update symfony/flex from v1.14.1 to v1.15.3 ([b4a3a8a](https://gitlab.com/adynemo/odl/commit/b4a3a8a66e84e4b9da7c19aa900469e3fe93afb8))
* **deps:** Update symfony/flex from v1.15.3 to v1.17.1 ([34874ca](https://gitlab.com/adynemo/odl/commit/34874ca42b39d7d4d3d5cd75ad445753ac8642e8))
* **deps:** Update symfony/framework-bundle from v4.4.27 to v4.4.30 ([029674c](https://gitlab.com/adynemo/odl/commit/029674c7d8e262fa3f2026e030dd2594eebffe7e))
* **deps:** Update symfony/framework-bundle from v4.4.30 to v4.4.31 ([c75daf7](https://gitlab.com/adynemo/odl/commit/c75daf7d2cdae8cf6c7ce9cc3dfd60f32fb88593))
* **deps:** Update symfony/twig-bundle from v4.4.27 to v4.4.30 ([f101da2](https://gitlab.com/adynemo/odl/commit/f101da21b128278a9d7f306c90935bd23253e472))
* **deps:** Update symfony/web-profiler-bundle from v4.4.28 to v4.4.31 ([670522a](https://gitlab.com/adynemo/odl/commit/670522abc762a04883b782d602cac3ca2185f2ec))
* **deps:** Update twig/extra-bundle from v3.3.1 to v3.3.3 ([4cf2242](https://gitlab.com/adynemo/odl/commit/4cf2242eaf481932c206cd9dfb8231c0ef4d06b1))
* **deps:** Update twig/twig from v3.3.2 to v3.3.3 ([0c86759](https://gitlab.com/adynemo/odl/commit/0c86759f79e780aff6ba22d7a445be7de3562dc4))
* downgrade ramsey/collection and update other libs ([a24c00e](https://gitlab.com/adynemo/odl/commit/a24c00ee2cdb344d91a381da3f1d7ff23d9955f3))

### [4.0.8](https://gitlab.com/adynemo/odl/compare/v4.0.7...v4.0.8) (2021-08-24)


### 🏃 Performance

* pagination calls backend only one time per page ([115d1c4](https://gitlab.com/adynemo/odl/commit/115d1c4b5e73df11e2ae19b6ac4c2786f18a2c88))


### ♻ Refactor

* **frontend:** better handling of user roles ([6947d4a](https://gitlab.com/adynemo/odl/commit/6947d4ad23856550353652e4fd2e161a95ea8abd))


### 🐛 Bug Fixes

* check if user exists in readings controller ([daa455f](https://gitlab.com/adynemo/odl/commit/daa455f6693439378bff50688f3f034e5be0be33))
* set string type when empty title and desc arc is submitted ([4810f35](https://gitlab.com/adynemo/odl/commit/4810f35e1ec7009b4dea2a95c21651e9d22bd618))


### 🏗️ Build system

* **deps:** Update symfony/flex from v1.14.0 to v1.14.1 ([c5d35a1](https://gitlab.com/adynemo/odl/commit/c5d35a12f667ee212d287e7ff3936b4bfa57ee99))
* **Docker:** use php7.2-alpine image ([1ff3250](https://gitlab.com/adynemo/odl/commit/1ff3250c0f659e03765da4da58af363cc54f6aee))


### 🧩 CI

* add job to automatically up assets version ([b7af276](https://gitlab.com/adynemo/odl/commit/b7af2760ba04cdb3317b2336befaa6c3401070f2))
* use php7.2-alpine image for PHP job ([d23aec3](https://gitlab.com/adynemo/odl/commit/d23aec3218a2f73268f2af83e26ca48be1c1f11b))

### [4.0.7](https://gitlab.com/adynemo/odl/compare/v4.0.6...v4.0.7) (2021-08-22)


### ✨ Improvements

* add logo and arcs counter in reading order title ([0781c77](https://gitlab.com/adynemo/odl/commit/0781c77eb0deff168e43526858babcdcfaff2cf0))
* add readings on the search result page ([3abb74f](https://gitlab.com/adynemo/odl/commit/3abb74fd6b85ccd17e5389a44fe41e43c1e643d6))
* sort sections by position ([4536c8e](https://gitlab.com/adynemo/odl/commit/4536c8e788cbd2678668fb5398db513b9c643370))


### ♻ Refactor

* less jQuery and JS improvements ([cb7c4cf](https://gitlab.com/adynemo/odl/commit/cb7c4cfc9c0a2c5492447a639dedaf2d67043bab))
* migrate from AJAX to Fetch API ([58a10c0](https://gitlab.com/adynemo/odl/commit/58a10c0bff5efb5b00dcdfa066ae7375ccbbcdcb))
* rename odl_ajax_reading_by_era path to odl_readings_by_era ([079f6d8](https://gitlab.com/adynemo/odl/commit/079f6d8e8102fa9eac4b55c5dab459c0224d0718))


### 🐛 Bug Fixes

* adapt arc height to the height of its description ([c5bf315](https://gitlab.com/adynemo/odl/commit/c5bf31524bc54fec1500471e13a8e44627dfefe4))
* corrects breaking design on the search result modal ([9377e7a](https://gitlab.com/adynemo/odl/commit/9377e7ac51ef754effb494bd771f0ab59f8f1392))

### [4.0.6](https://gitlab.com/adynemo/odl/compare/v4.0.5...v4.0.6) (2021-08-21)


### 🐛 Bug Fixes

* corrects findLastChildByParent method ([804c10b](https://gitlab.com/adynemo/odl/commit/804c10b4acb0b51e0e625b11ed813212195052dd))


### 🏗️ Build system

* use commit message conventional config for Violinist ([2d15994](https://gitlab.com/adynemo/odl/commit/2d15994895377a40e7808d00fd5f2a09ccad396d))

### [4.0.5](https://gitlab.com/adynemo/odl/compare/v4.0.4...v4.0.5) (2021-08-20)


### 🐛 Bug Fixes

* corrects close's button on reading orders closes period ([93affbb](https://gitlab.com/adynemo/odl/commit/93affbb07796c9bcb57f30d721f3c4769390ce92))
* corrects returns codes of commands ([686b609](https://gitlab.com/adynemo/odl/commit/686b6091ed07f372411fc71d3a764cb0258ed685))
* corrects some bugs on arcs logos ([8a1a846](https://gitlab.com/adynemo/odl/commit/8a1a84646bda0ad364a8a540bc9cd69d066e45f2))
* corrects universes banners height on large screen ([ebe600c](https://gitlab.com/adynemo/odl/commit/ebe600c006bc95584b55f0761ceb5abdaefca43e))
* make more margin between universe's title and its logo ([a0e554d](https://gitlab.com/adynemo/odl/commit/a0e554d679cad17ffb08984cd7b0f51f24021b9c))
* remove error message from email subject ([cb3ca03](https://gitlab.com/adynemo/odl/commit/cb3ca037819698cea57c1d0ee3817cfe0a85e102))

### [4.0.4](https://gitlab.com/adynemo/odl/compare/v4.0.3...v4.0.4) (2021-08-18)


### 🧩 CI

* run phpunit without colors ([fd35818](https://gitlab.com/adynemo/odl/commit/fd35818934cc7b8dee7db2bc1c52781154b22c08))

### [4.0.3](https://gitlab.com/adynemo/odl/compare/v4.0.2...v4.0.3) (2021-08-18)


### 🧩 CI

* add SAST jobs to gitlab-ci ([145873c](https://gitlab.com/adynemo/odl/commit/145873c0878693051b7cf7eebac5ad67be8fc2d1))


### ✨ Improvements

* add captcha to the registration form ([a370b44](https://gitlab.com/adynemo/odl/commit/a370b44482f39e4ed735132b0fbfc2e3afe23cbf))


### 🩺 Tests

* use composer scripts to test and lint ([853b313](https://gitlab.com/adynemo/odl/commit/853b3137c061d58f607cad7680f38e15c8591710))


### 📕 Documentation

* update tests part on CONRTIBUTIND.md ([4734212](https://gitlab.com/adynemo/odl/commit/473421201ef8e3a70cd4d76c4d649ba5622238b4))


### 🏗️ Build system

* upgrade dependencies ([28b75d1](https://gitlab.com/adynemo/odl/commit/28b75d182e32c4bd3e7c97ac32a6097561e92ceb))

### [4.0.2](https://gitlab.com/adynemo/odl/compare/v4.0.1...v4.0.2) (2021-07-30)


### ✨ Improvements

* not login user if his account is not activated ([f85cd12](https://gitlab.com/adynemo/odl/commit/f85cd12cfbdab0aa35e1ec5ac89b40a7c066db49))


### 🏗️ Build system

* **dev:** add symfony/css-selector for functional html testing ([bf11844](https://gitlab.com/adynemo/odl/commit/bf118442857e1e9b7b62cfea1b528db1c9cc44bc))


### 🩺 Tests

* add functional test for UserChecker ([d32c770](https://gitlab.com/adynemo/odl/commit/d32c770634beab9c98e64d16a37e90ebc57bc16a))
* **perf:** make password encoding faster ([6d1c3bb](https://gitlab.com/adynemo/odl/commit/6d1c3bb5d9a81d3271a0f04e4425841bfc5b0681))

### [4.0.1](https://gitlab.com/adynemo/odl/compare/v4.0.0...v4.0.1) (2021-07-27)


### ♻ Refactor

* builds automatically the back button from the referrer ([af1f909](https://gitlab.com/adynemo/odl/commit/af1f909b100faae2190c2ddcc287d2711f9c28e3))


### 📕 Documentation

* adds CII Best Practices badge ([7a7f93f](https://gitlab.com/adynemo/odl/commit/7a7f93fb31e1647742d138e4e49f32e187cb70dc))


### 🏗️ Build system

* update dama/doctrine-test-bundle from v6.5.0 to v6.6.0 ([69725a4](https://gitlab.com/adynemo/odl/commit/69725a4a2ca01fcb142e1e3a8bf898072f8f8e09))
* update phpstan/phpstan from 0.12.88 to 0.12.93 ([0b48c0c](https://gitlab.com/adynemo/odl/commit/0b48c0c72b25bf7e6ffd1defda8ee934f052ce7a))
* update symfony/asset from v4.4.22 to v4.4.27 ([5620b05](https://gitlab.com/adynemo/odl/commit/5620b05a232fe7c5ff8005e681f0c5a81b70282c))
* update symfony/browser-kit from v4.4.24 to v4.4.27 ([a612e99](https://gitlab.com/adynemo/odl/commit/a612e99595975c059a40d672be669b50bd2aed22))
* update symfony/console from v4.4.24 to v4.4.27 ([44d7ae7](https://gitlab.com/adynemo/odl/commit/44d7ae78191764bc84890fe9ce0eb9a17e93f6ca))
* update symfony/dotenv from v4.4.20 to v4.4.27 ([91b40fd](https://gitlab.com/adynemo/odl/commit/91b40fdb97b155c16485063768d421542778da05))
* update symfony/expression-language from v4.4.22 to v4.4.27 ([c92b1f6](https://gitlab.com/adynemo/odl/commit/c92b1f6b295f3c2564608f125ece05852cb1b29f))
* update symfony/form from v4.4.24 to v4.4.27 ([bac91ea](https://gitlab.com/adynemo/odl/commit/bac91ea3acda67097dfd4be71e206eebbfe01639))
* update symfony/framework-bundle from v4.4.24 to v4.4.27 ([51011fa](https://gitlab.com/adynemo/odl/commit/51011fac7e93dc8636e62bdccaca5d818bf62e6f))
* update symfony/mailer from v4.4.22 to v4.4.27 ([12eafcf](https://gitlab.com/adynemo/odl/commit/12eafcf1cbec9e2e616f8b3289de08259175e4c5))
* update symfony/maker-bundle from v1.31.1 to v1.33.0 ([818e2da](https://gitlab.com/adynemo/odl/commit/818e2da82bb4cabb6bf13d9a7f7aaf6910e92cbf))
* update symfony/phpunit-bridge from v5.2.9 to v5.3.4 ([165187d](https://gitlab.com/adynemo/odl/commit/165187da18fec08437ac80684c3e67309b96166f))
* update symfony/process from v4.4.22 to v4.4.27 ([d0fa0d1](https://gitlab.com/adynemo/odl/commit/d0fa0d1b1d4cffc39db63335b1193d54d3740e9a))
* update symfony/security-bundle from v4.4.23 to v4.4.27 ([1ba0030](https://gitlab.com/adynemo/odl/commit/1ba003049fa9f74503c0a73f9b47b6b9bf3ce58c))
* update symfony/validator from v4.4.24 to v4.4.27 ([d3548c0](https://gitlab.com/adynemo/odl/commit/d3548c0587820768d161f26e81f891d6eee1ed0f))
* update symfony/var-dumper from v4.4.22 to v4.4.27 ([395b4fc](https://gitlab.com/adynemo/odl/commit/395b4fc69a023655b4b3aa919c3d61b03af6836b))
* update symfony/yaml from v4.4.24 to v4.4.27 ([40b0fe8](https://gitlab.com/adynemo/odl/commit/40b0fe86fff45699892746f98c67c68f4a782188))

## [4.0.0](https://gitlab.com/adynemo/odl/compare/v3.6.1...v4.0.0) (2021-07-25)


### ⚠ BREAKING CHANGES

* opens registration to public

### 📕 Documentation

* add testing informations to CONTRIBUTING.md ([14a30ae](https://gitlab.com/adynemo/odl/commit/14a30ae288fc85e3094ca3385ed9e4912a4ea4dd))


### 🖊 Coding style

* update php-cs-fixer and up to PSR12 ([03017c7](https://gitlab.com/adynemo/odl/commit/03017c792ac691827f767de659bbd21edd77f174))


### ✨ Improvements

* adds icons to messages flash ([67937b7](https://gitlab.com/adynemo/odl/commit/67937b7c429f8659850724b0ab7bfc7a0361cc42))
* can add a logo to Universe ([5874429](https://gitlab.com/adynemo/odl/commit/58744290b264eb64052f72b11382b144834ee37e))
* user set his own password when he is created by admin ([bd94ad3](https://gitlab.com/adynemo/odl/commit/bd94ad3b294dc9b12eb6e7b1b8570ac3cad2c2e3))


### ♻ Refactor

* changes profile path from admin to user ([937939b](https://gitlab.com/adynemo/odl/commit/937939bde6c1dd2f55554c2f6afe18a33a6c8040))


### 🎉 Features

* adds arcs readings for users ([2dfda5b](https://gitlab.com/adynemo/odl/commit/2dfda5b3eced385f713e3e485f6ca4f91148db76))
* allows admin to delete users ([119dd1f](https://gitlab.com/adynemo/odl/commit/119dd1f93e79c2fa9e362b4d98638f8c41350185))
* allows user to delete his own account ([ac95c31](https://gitlab.com/adynemo/odl/commit/ac95c31578cbbcc286accfcdb50cdb3016e93779))
* opens registration to public ([b48ac20](https://gitlab.com/adynemo/odl/commit/b48ac20c6a2b070410b6caed63b34b1b3a38c74b))
* opens registration to the public and sets an activation process ([7ad2e4e](https://gitlab.com/adynemo/odl/commit/7ad2e4e38475df2a6c2a2222691ecf9a957943bb))

### [3.6.1](https://gitlab.com/adynemo/odl/compare/v3.6.0...v3.6.1) (2021-07-24)


### 🏗️ Build system

* connects Violinist to auto check dependencies ([9f1e870](https://gitlab.com/adynemo/odl/commit/9f1e870891dc6677581e3fac6fa2e1a040fb0c31))


### 📕 Documentation

* adds Violonist badge to README.md ([38069ae](https://gitlab.com/adynemo/odl/commit/38069aec5f46d82f8d19c4f04c23ffd1fbcf320b))

## [3.6.0](https://gitlab.com/adynemo/odl/compare/v3.5.0...v3.6.0) (2021-07-18)


### 🐛 Bug Fixes

* apiToken too long for unique index ([2345448](https://gitlab.com/adynemo/odl/commit/23454488fd8cc0a97818b1d310a04010eb85de32))
* avatars directory missing ([8a48fc6](https://gitlab.com/adynemo/odl/commit/8a48fc64507d4c8a1a3c8e0528de801cb5689ae2))
* csrf token did not get on validate arc ([d18f7a2](https://gitlab.com/adynemo/odl/commit/d18f7a242c3ca0d6bb6f1c8fe752cb27199c77d9))
* dark mode and admin message flash ([1417c37](https://gitlab.com/adynemo/odl/commit/1417c3797c08fec33dab8f4bebe631c79950a7d2))
* dark mode on form fields ([4bb68bc](https://gitlab.com/adynemo/odl/commit/4bb68bc26ebf9cede38390385550ff668869374f))
* doctrine migrations config deprecation ([a7eeda8](https://gitlab.com/adynemo/odl/commit/a7eeda8393368f9e6b030998b344afe7ac2cf973))
* empty period should not displayed on reading order ([ec1bf41](https://gitlab.com/adynemo/odl/commit/ec1bf4139707fc777ec9b053823a9cef273bc75d))
* empty result for search query on multi words ([5ff79ef](https://gitlab.com/adynemo/odl/commit/5ff79ef6560355f07b511a83be0890b54e3334e1))
* fix swal modal on profile page ([67c09a2](https://gitlab.com/adynemo/odl/commit/67c09a2aca29deafbca8f64383b85d2bf1f420d7))
* form validation condition ([805da8c](https://gitlab.com/adynemo/odl/commit/805da8c273ab7daf003084cec363387f3109ba29))
* i18n mail for english ([84ff105](https://gitlab.com/adynemo/odl/commit/84ff1052e4a16ddb4faab92b9120a353696102a0))
* i18n missing on search page ([26b372f](https://gitlab.com/adynemo/odl/commit/26b372fcc1534e9f92fba7d47266c3166735514c))
* limit search feature to 3 characters at least ([421e27a](https://gitlab.com/adynemo/odl/commit/421e27ae0ba88d2a41c632e3ea319579fcbc149c))
* login throw error on empty redirect URL ([2635ee2](https://gitlab.com/adynemo/odl/commit/2635ee2922f02621303f98382e110bf21786325e))
* make Journal ordered by date desc ([c444655](https://gitlab.com/adynemo/odl/commit/c444655248d836379e9638f41997d848c61c2893))
* new arc created with isValidated at true for Editor or higher ([a81f5ef](https://gitlab.com/adynemo/odl/commit/a81f5efdbc59d743e10cc39c4d54561b47605c98))
* period is null with update form without period field ([a1dbb4b](https://gitlab.com/adynemo/odl/commit/a1dbb4b6921c5aa152344c0c47c60c64aa2c4689))
* redirect route with param missing ([3a4a27c](https://gitlab.com/adynemo/odl/commit/3a4a27c06806269cb619b6f38881fd2b3161e246))
* remove password form for super admin on users form ([1442645](https://gitlab.com/adynemo/odl/commit/14426451509a865f426d9cfcdc5ac906ec136aed))
* remove trick for getFilters in twig extensions ([1d080f2](https://gitlab.com/adynemo/odl/commit/1d080f21d784b88c679c838c4f735077a2b3be39))
* search bar ([4618082](https://gitlab.com/adynemo/odl/commit/46180828c74122bd1c5b8df8e3680208497ae45c))
* set locale for user creation with command ([eaf5fc6](https://gitlab.com/adynemo/odl/commit/eaf5fc64f4445af352e4ee779cf15c25e4308ce0))
* translation formattage ([13bb490](https://gitlab.com/adynemo/odl/commit/13bb490b8e15642de504f0bd227984e3698dfc6e))
* translations var displays on FR ([57eab71](https://gitlab.com/adynemo/odl/commit/57eab7140f1e1d50fc64b136aa452c5168622a4a))
* two display of update arc form ([6cef171](https://gitlab.com/adynemo/odl/commit/6cef171fc4fd4221d7dd0891b8ff1f3b40a4f27a))
* undefined arc id on search result click event ([ed719ea](https://gitlab.com/adynemo/odl/commit/ed719ea7ca89b4dade56cdbdc9590ee439113855))
* undefined old variable in forgotten-password email template ([6708ecd](https://gitlab.com/adynemo/odl/commit/6708ecd10c7f31794e8c3a986f2b287cb95b0f69))
* update dark mode global variable ([8b27322](https://gitlab.com/adynemo/odl/commit/8b273223490e0d1e7750a075d83af551ef58e89f))


### ✨ Improvements

* add assets versionning ([24ccad4](https://gitlab.com/adynemo/odl/commit/24ccad42c65d052b2b959815ad67a517efda7fc8))
* add isValidated flag on Arc ([932678c](https://gitlab.com/adynemo/odl/commit/932678cbd725371514b9bf5278e52767ae08ffd4))
* add log into Journal for arc creation by API ([fdad676](https://gitlab.com/adynemo/odl/commit/fdad67668d07230ea556226387c3f5889b4b98d0))
* add option to clean Journal beyond 50 items ([14fb4a7](https://gitlab.com/adynemo/odl/commit/14fb4a751e53347dde7a58aa3b958a7e17231b2e))
* add translations to error's template ([a7651fe](https://gitlab.com/adynemo/odl/commit/a7651fea9255e5afb2799b78629429821a933a7e))
* improve moderation with pagination and ajax ([aea345d](https://gitlab.com/adynemo/odl/commit/aea345d081a87a1f45bfba134102d27812819234))
* some styles, fixes and improvements ([b22914d](https://gitlab.com/adynemo/odl/commit/b22914d7191aa366193da63094b0557fb719107a))


### 🩺 Tests

* add functional tests for ArcApi ([59db4f1](https://gitlab.com/adynemo/odl/commit/59db4f155d1d79120f2ebebacebd8503cbfc3a25))
* add sections and arc fixtures and start to test ArcManager ([40fc5d0](https://gitlab.com/adynemo/odl/commit/40fc5d08627ed9ba31e27b12a9df8d016af667c1))
* add test for SecurityListener ([2c106e7](https://gitlab.com/adynemo/odl/commit/2c106e7c038861f27c39bde2085fd56e070e2d83))
* add tests for FileManager, ImageManager and ArcManager ([dc90d74](https://gitlab.com/adynemo/odl/commit/dc90d7477fd810a8bc9eeb704333a56c71233928))
* add unit test for EraVoter ([9ded321](https://gitlab.com/adynemo/odl/commit/9ded3219c67dc4ac752cfebf2e4d027bfd5715ae))
* full testing for ArcManager ([a764828](https://gitlab.com/adynemo/odl/commit/a7648282427b90a3b16891a024ec21c88fc417e6))
* SettingManagerTest ([7ae6245](https://gitlab.com/adynemo/odl/commit/7ae6245df717230b6c5cd89c1bc43e0e591b1a85))


### ♻ Refactor

* add a more explicit indication for events ([2f27c9d](https://gitlab.com/adynemo/odl/commit/2f27c9d829af8a21b94dbda33fadb90be560c6ed))
* ArcManager ([477bd00](https://gitlab.com/adynemo/odl/commit/477bd006d13c93982dba4cc1f5f7a69db39d5e41))
* better code and responsabilities for FileManager and ImageManager ([d246bb1](https://gitlab.com/adynemo/odl/commit/d246bb1d302673d7888d5276a9d12d648863b7a4))
* better dark mode implementation ([a2b5931](https://gitlab.com/adynemo/odl/commit/a2b5931de9689467e305e99dda89f578dc69156f))
* code and error handling for Arc, Period and Universe managers ([4b43aa4](https://gitlab.com/adynemo/odl/commit/4b43aa4732625e95a78399b917b9f0ae5639aaa6))
* code and error handling for eras management ([336d425](https://gitlab.com/adynemo/odl/commit/336d4257405b54171b95c49b338f612ade145a3b))
* code and error handling for settings ([5cb72d9](https://gitlab.com/adynemo/odl/commit/5cb72d98e8e78d968bd0bb6b8cd8578209dd1f8c))
* error handling for File and Image managers ([85db3f2](https://gitlab.com/adynemo/odl/commit/85db3f20929bd7b74f36b78c6c929e2acb38ddcb))
* factorization of pagination javascript code ([e45cb9a](https://gitlab.com/adynemo/odl/commit/e45cb9a869e4d365f19b6fb6c658bf197511d380))
* improve settings handling ([504a1f1](https://gitlab.com/adynemo/odl/commit/504a1f17ef89b8da4cc77f3d9a883684352ffb5c))
* make a better docker and install sass ([4fe0eac](https://gitlab.com/adynemo/odl/commit/4fe0eac4c2febd494cfaf3ccac5a6a163bf42118))
* make collections management more easier ([ec03b12](https://gitlab.com/adynemo/odl/commit/ec03b122a3ef85b02b0e804fd4c8bdbef287f4ae))
* make register form accessible ([848e987](https://gitlab.com/adynemo/odl/commit/848e9876353fab9b729647047c327e5d618f1335))
* make roles management easier ([41e7b28](https://gitlab.com/adynemo/odl/commit/41e7b283b057373128760497e7f477077d7f86b5))
* more check on remove avatar and improve route ([811d61c](https://gitlab.com/adynemo/odl/commit/811d61cc900ade63b82a2835a734f86908cbc165))
* move copyrights to DB and allow user to disable developer copyright ([d4acee2](https://gitlab.com/adynemo/odl/commit/d4acee2a6bea857b2fa4050204fcf3b6c61c0d0b))
* move DTO, Entity and DataCollector ([fb565d4](https://gitlab.com/adynemo/odl/commit/fb565d47851177dd0e2286f87ae3e307837289c0))
* move scss outside of the public ([d801eb8](https://gitlab.com/adynemo/odl/commit/d801eb8496e22bed8f80dca9b97ea3613f530bf7))
* move settings forms to sub directory ([826ffe8](https://gitlab.com/adynemo/odl/commit/826ffe8e101a6bb72bae64af4de1e80eff36ed2e))
* multiple services refactoring ([6bc3eb9](https://gitlab.com/adynemo/odl/commit/6bc3eb98a5e7d35c42482cdb3f954048536c6661))
* put message flash at the layout ([6987f8e](https://gitlab.com/adynemo/odl/commit/6987f8e327f61a6c58b2e6e92568d6985ab33e67))
* real mime types check on FileManager, improve and cleanup ([bb870a1](https://gitlab.com/adynemo/odl/commit/bb870a14ec888c1ac6d00f12bd6ebe7f3256851f))
* remove nodejs dependencies, use cdn and migrate to fomantic-ui ([0d8ead4](https://gitlab.com/adynemo/odl/commit/0d8ead4a30cedb63077eb237fc383822459bf879))
* remove, improve and refactor old code and somes fixes ([71fa96e](https://gitlab.com/adynemo/odl/commit/71fa96eb6589ed530d30418099db63e9f944049e))
* return error in response for invalid token ([7831714](https://gitlab.com/adynemo/odl/commit/78317147a715320d083127d0499f58d13eec59f2))
* standardize sections ([5188e75](https://gitlab.com/adynemo/odl/commit/5188e75e1a708044c25cb84ff520c461860f50c0))
* start cleanup css ([34d84be](https://gitlab.com/adynemo/odl/commit/34d84bebdc8113de74ba9ec200c45faa5ca0da64))
* use Fomantic UI Toast component ([17e6c44](https://gitlab.com/adynemo/odl/commit/17e6c4471ff078b4893fc35bb4d15a9135d89fd9))
* use modules to import pagination base ([db31565](https://gitlab.com/adynemo/odl/commit/db315659dd367b222a0cbc630d3512211a306d2a))
* use trait for flash messages ([172c5e8](https://gitlab.com/adynemo/odl/commit/172c5e8b03450ea2d319c8c1a4a2ff286985b739))


### 🎉 Features

* add translation with english and french languages ([a56c67a](https://gitlab.com/adynemo/odl/commit/a56c67a49b34b0d088216ea016c393d04a21c17d)), closes [#45](https://gitlab.com/adynemo/odl/issues/45)
* allow API to create an Arc to be validated ([560e8aa](https://gitlab.com/adynemo/odl/commit/560e8aaaab41c474a3da2b8af931b34353a21ab3))
* create page to list and moderate arcs ([73c3131](https://gitlab.com/adynemo/odl/commit/73c31311a6a2a4f42c52446913ca9c5cb67a5b12))
* display last login on profile page ([ba793ba](https://gitlab.com/adynemo/odl/commit/ba793babce5d8a997ef0d070cca8c0b1e4b86ba8))
* display welcome message after login ([7f375d3](https://gitlab.com/adynemo/odl/commit/7f375d35606d49c302a7721a3a9f4c5af593d838))
* handle positions for each section ([8fe97b8](https://gitlab.com/adynemo/odl/commit/8fe97b8fbf52b8cde709628030726738c9250479))
* implement API with new firewall ([ce9167f](https://gitlab.com/adynemo/odl/commit/ce9167f286a7d753c739811d599c0451bfe3900d))
* implement csrf on ajax routes ([cd7366f](https://gitlab.com/adynemo/odl/commit/cd7366fc49f89d517cb99230c0628b853012faca))
* implement voter to choose on which era an editor can act ([a2af4a3](https://gitlab.com/adynemo/odl/commit/a2af4a3146d1bb2aa0718dcf45947b2f1a30f686))
* log errors by email ([61d6959](https://gitlab.com/adynemo/odl/commit/61d69593e646a33d4c210a40487947ab821fa4d5))
* store user last login and display it on users page ([bafcd43](https://gitlab.com/adynemo/odl/commit/bafcd43f79c5386f2a2796d4739163a2120faf74))
* super admin can generate users API token ([892e1a5](https://gitlab.com/adynemo/odl/commit/892e1a5ec46bdd5fdb9356faa2dcae9087aae3d0))
* use SASS variables ([04a97bd](https://gitlab.com/adynemo/odl/commit/04a97bdc608391e3636f93e667a19af3cba72e90))
* user can choose his interface language ([32a9cdb](https://gitlab.com/adynemo/odl/commit/32a9cdbdf1e3b6fc5b88ddf84f9188d94692d772))


### 🏗️ Build system

* add browser-kit and some php ext required ([3f47738](https://gitlab.com/adynemo/odl/commit/3f47738c28c706298a2441473c89f98ccd1701cb))
* composer update and add translations to yaml linter ([00863ab](https://gitlab.com/adynemo/odl/commit/00863abccfa4ee0e5abe5c107b141b061242e683))
* fix symfony process missing in requirements ([8ea0b24](https://gitlab.com/adynemo/odl/commit/8ea0b24fa04591e65dfd9fa1b67838679289dff6))
* migrate maintenance bundle to handle deprecations ([6173cfd](https://gitlab.com/adynemo/odl/commit/6173cfdd8a62eacac0045ef1568b91a62e7ccdba))
* php ext dependencies ([6524034](https://gitlab.com/adynemo/odl/commit/652403478e45c8602b6017f9d17d226a40074bc7))
* remove git-chglog ([8dee7c5](https://gitlab.com/adynemo/odl/commit/8dee7c532facbebb6d8e9d03fbf908d138443b3f))
* upgrade bundles ([216914f](https://gitlab.com/adynemo/odl/commit/216914f0fe696c0f4e8c675a87f4d1ff9a327147))


### 📕 Documentation

* update CONTRIBUTING.md with commit message guidelines ([720f7a0](https://gitlab.com/adynemo/odl/commit/720f7a09c5812162b9866300eed3ffce82a22ef9))
* update MR template ([85e92c6](https://gitlab.com/adynemo/odl/commit/85e92c6a3dce679f84f20a0366abfd0700ae18f4))


### 🧩 CI

* add release stage with standard-version tool ([f7607de](https://gitlab.com/adynemo/odl/commit/f7607de4159085007a9f5f69f37b4d19a87690b9))
* appends changelog to CHANGELOG.md ([180003f](https://gitlab.com/adynemo/odl/commit/180003f5ff2d0498d46b1f65c0b190420dd8339e))
* corrects the way jobs are triggered ([0e622a5](https://gitlab.com/adynemo/odl/commit/0e622a5c7ee37475f82535283dffbf0274f2467a))
* corrects the way jobs are triggered ([d6a6779](https://gitlab.com/adynemo/odl/commit/d6a6779c9ab6d5499330346954458569b12a1d52))
* fix no JPEG support error ([084a5b1](https://gitlab.com/adynemo/odl/commit/084a5b14642131af5bd04a8930cf29be78d0e625))
* fix publish stage and allow it to define the tag according the semver ([1e8bca5](https://gitlab.com/adynemo/odl/commit/1e8bca5ef9015379072f4553a22e5476611a361c))
* trigger release's jobs to merged_result event ([7ead9a3](https://gitlab.com/adynemo/odl/commit/7ead9a3646c13b5f163956a0ac04f7b2b920a56d))

## Changelog [before 3.6.0](docs/CHANGELOG.old.md)

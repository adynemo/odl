const toggleUniverse = (event) => {
  const universeId = event.target.dataset.universeId;
  const container = document.getElementById(universeId);
  toggle(container);
}

const handleUniversesToggle = () => {
  const universeTitles = document.getElementsByClassName('universe-title');
  for (const universeTitle of universeTitles) {
    universeTitle.addEventListener('click', toggleUniverse, false);
  }
}

toggleSection();
handleUniversesToggle()

import {Toast} from "./modules/toast.mjs";
import {request} from "./modules/request.mjs";

$(document).ready(function () {
  $('.generate-token').click(function () {
    const username = $(this).data('username');
    const id = $(this).data('user-id');
    swal({
      text: ODL.translation.security.confirmGenerateToken(username),
      buttons: {
        cancel: {
          text: ODL.translation.form.cancel,
          value: false,
          visible: true,
        },
        confirm: {
          text: ODL.translation.form.confirm,
        }
      },
    }).then(function (answer) {
      if (answer) {
        request.get(
          'odl_ajax_user_generate_api_token',
          {
            id,
            token: ODL.security.token.update
          }
        ).then(data => {
          $('body').toast({
            class: 'info',
            showIcon: 'key',
            displayTime: 0,
            closeIcon: true,
            message: ODL.translation.security.confirmSuccess(data.token),
            classActions: 'bottom attached',
            actions: [{
              text: ODL.translation.general.copyToClipboard,
              icon: 'clipboard',
              class: 'green',
              click: function () {
                copyToClipboard(data.token);
                return false;
              }
            }]
          });
        }).catch(() => {
          Toast.generalError();
        });
      }
    });
  });
});

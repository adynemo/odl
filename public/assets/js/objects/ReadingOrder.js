class ReadingOrder {
  constructor() {
    this.order = {};
  }

  isPeriodExists(periodId) {
    return this.order.hasOwnProperty(periodId);
  }

  isPageExists(periodId, pageNumber) {
    return this.isPeriodExists(periodId) && this.order[periodId].hasOwnProperty(pageNumber);
  }

  addPage(periodId, pageNumber, page) {
    if (!this.isPeriodExists(periodId)) {
      this.order[periodId] = {};
    }

    this.order[periodId][pageNumber] = page;
  }

  getPage(periodId, pageNumber) {
    return this.isPageExists(periodId, pageNumber) ? this.order[periodId][pageNumber] : null;
  }
}

export {ReadingOrder};

const revealPassword = (icon) => {
    const passwordInput = icon.parentNode.previousElementSibling;
    passwordInput.type = ('password' === passwordInput.type) ? 'text' : 'password';
    icon.classList.toggle('slash')
}

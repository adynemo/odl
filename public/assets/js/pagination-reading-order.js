import {Pagination} from './modules/pagination.mjs';
import ArcLineHandler from "./modules/arc-line-handler.mjs";

let scrollTo = true;
const params = new URLSearchParams(window.location.search);
const page = params.get("page");
const arc = params.get("arc");
const init = async ($title) => {
  const $periodContainer = $title.parent();
  const $periodContentContainer = $periodContainer.children('div.period-container');
  const toOpen = 'none' === $periodContentContainer.css('display');

  const $paginationContainers = $periodContentContainer.children('.pagination');
  $paginationContainers.find('.btn_page.active').removeClass('active');

  if (true === toOpen) {
    $paginationContainers.find(`.btn_page[data-page="1"]`).addClass('active');
    $paginationContainers.find(`.btn_prev`).attr('disabled', true);
    $paginationContainers.find(`.btn_next`).attr('disabled', false);
    await buildPage($periodContentContainer, 1);
  }

  toggle($periodContentContainer.get(0));
}

const buildPage = async ($periodContentContainer, page) => {
  const periodId = $periodContentContainer.parents('.period').attr('id');
  const $arcsContainer = $periodContentContainer.children('div.arcs-container');
  const response = await Pagination.getPage('odl_ajax_arc_paginate', {
    id: periodId,
    page
  });
  $arcsContainer.find('.arc-line').remove();

  ArcLineHandler.appendLine(response.arcs, response.html, $arcsContainer.get(0));

  if (0 < $arcsContainer.has('button.read').length && ODL.user.hasOwnProperty('readings') && 0 < ODL.user.readings.length) {
    const $readingButtons = $arcsContainer.find(`button.read`);
    $readingButtons.each((i, button) => {
      const $button = $(button);
      if (ODL.user.readings.includes($button.data('arcId'))) {
        $button.addClass('violet');
      }
    });
  }
}

const $pagination = $('.pagination');
$('.period-title').click(async function () {
  await init($(this));
  if (scrollTo && arc && !page) {
    // todo: better handling, maybe with event
    setTimeout(() => {
      document.getElementById(arc).scrollIntoView({block: "center", inline: "nearest", behavior: 'smooth'});
    }, 350);
    scrollTo = false;
  }
});

$pagination.on('click', '.btn_prev', async function () {
  const $container = $(this).parents('.period-container');
  const $periodContainer = $(this).parents('.period');
  await Pagination.navigate($container, buildPage, true);
  upToContainer($periodContainer.get(0));
});

$pagination.on('click', '.btn_page', async function () {
  const $container = $(this).parents('.period-container');
  const $periodContainer = $(this).parents('.period');
  const navigateTo = $(this).data('page');
  await Pagination.navigateByPage($container, navigateTo, buildPage);
  if (scrollTo && arc && page) {
    // todo: better handling, maybe with event
    document.getElementById(arc).scrollIntoView({block: "center", inline: "nearest", behavior: 'smooth'});
    scrollTo = false;
  } else {
    upToContainer($periodContainer.get(0));
  }
});

$pagination.on('click', '.btn_next', async function () {
  const $container = $(this).parents('.period-container');
  const $periodContainer = $(this).parents('.period');
  await Pagination.navigate($container, buildPage);
  upToContainer($periodContainer.get(0));
});

const button = document.getElementById('dark-mode-button');

const handleInvertedFUI = (container = document) => {
  const inverted = container.getElementsByClassName('odl-inv');
  const dropdown = container.getElementsByClassName('ui dropdown');
  const cards = container.getElementsByClassName('ui card');
  const progress = container.getElementsByClassName('ui progress');
  const icons = container.querySelectorAll('i.icon');
  const menu = container.querySelector('.ui.menu#up');
  const allInverted = [...inverted, ...cards, ...dropdown, ...progress, ...icons, ...[menu]];

  for (const element of allInverted) {
    if (element) {
      element.classList.toggle('inverted');
    }
  }
}

const handleDarkMode = () => {
  document.getElementsByTagName('html')[0].classList.toggle('odl-dark');
  const icon = button.querySelector('i');
  icon.classList.toggle('sun');
  icon.classList.toggle('moon');
  handleInvertedFUI()
}

const getDarkMode = () => {
  let mode = 'light';
  const stored = window.localStorage.getItem('odl_dark_mode');
  if (null !== stored) {
    mode = stored;
  } else if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    mode = 'dark';
  }

  return mode;
}

const saveDarkMode = () => {
  window.localStorage.setItem('odl_dark_mode', ODL.darkMode);
}

ODL.darkMode = getDarkMode();
saveDarkMode();

if ('dark' === ODL.darkMode) {
  handleDarkMode();
}

document.addEventListener('arc:update', function (e) {
  if ('dark' === ODL.darkMode) {
    handleInvertedFUI(e.target);
  }
}, false);

button.addEventListener('click', () => {
  handleDarkMode();
  ODL.darkMode = (ODL.darkMode === 'dark') ? 'light' : 'dark';
  saveDarkMode();
});

import { ArcEditor } from "./modules/arc-editor.mjs";

$(document).ready(function () {
  const $container = $('.arcs-container');
  $container.on('click', '.edit', async function () {
    await ArcEditor.updateArc($(this));
  });
  $container.on('click', '.remove', function () {
    ArcEditor.removeArc($(this));
  });
});

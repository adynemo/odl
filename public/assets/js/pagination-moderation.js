import {Pagination} from './modules/pagination.mjs';

$(document).ready(function () {
  const $pagination = $('.pagination');
  $pagination.on('click', '.btn_prev', async function () {
    const $container = $(this).parents('.moderation-content');
    await Pagination.navigate($container, buildPage, true);
    upToContainer($container.get(0));
  });

  $pagination.on('click', '.btn_page', async function () {
    const $container = $(this).parents('.moderation-content');
    const navigateTo = $(this).data('page');
    await Pagination.navigateByPage($container, navigateTo, buildPage);
    upToContainer($container.get(0));
  });

  $pagination.on('click', '.btn_next', async function () {
    const $container = $(this).parents('.moderation-content');
    await Pagination.navigate($container, buildPage);
    upToContainer($container.get(0));
  });
});

const buildPage = async ($container, page) => {
  const $arcsContainer = $container.children('div.arcs-container');
  const response = await Pagination.getPage('odl_ajax_arc_moderation_paginate', {page});
  $arcsContainer.remove();
  $container.children('.pagination.top').after(response.html);
  $container.children('div.arcs-container').get(0).dispatchEvent(ODL.events.arc.update);
}

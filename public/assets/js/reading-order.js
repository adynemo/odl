import {request} from "./modules/request.mjs";
import {Toast} from "./modules/toast.mjs";

const getRates = () => {
  request.get('odl_ajax_era_rates', {id: ODL.era.id})
    .then(rates => {
      ODL.data.rates = rates;
    });
  if (ODL.security.roles.user === ODL.user.role) {
    request.get('odl_ajax_era_user_rates', {id: ODL.era.id})
      .then(rates => {
        ODL.user.rates = rates;
      });
  }
};

getRates();

const readingProgress = () => {
  const progressContainer = document.getElementById('reading-progress');
  progressContainer.dataset.value = ODL.user.readings.length;
  $(progressContainer).progress({
    limitValues: false,
    precision: 15,
    text: {
      active: '{value}/{total}'
    }
  })
}

const handleUserReadings = () => {
  request.get('odl_readings_by_era', {id: ODL.era.id})
    .then(readings => {
      ODL.user.readings = readings;
      readingProgress();
    });

  $('.arcs-container').on('click', '.read', function () {
    const $button = $(this);
    const arcId = $button.data('arcId');
    const arcTitle = $button.data('arcTitle');
    const cover = document.getElementById(arcId).querySelector('.arc-cover img').src;
    const userRating = document.getElementById(arcId).querySelector('.rating-container .rating');
    const stars = userRating.children;
    const progressContainer = $('#reading-progress');

    if (false === $button.hasClass('violet')) {
      request.post(
        'odl_ajax_reading_read',
        {
          id: arcId,
          token: ODL.security.token.read
        }
      ).then(() => {
        $button.addClass('violet');
        progressContainer.progress('increment');
        ODL.user.readings.push(arcId);
        // todo: use event
        $(userRating).rating('enable');
        for (const i in stars) {
          if (stars[i] instanceof Element) {
            stars[i].title = ODL.translation.readingOrder.starsTitles[i];
          }
        }
        Toast.displayWithAvatar('success', cover, ODL.translation.readingOrder.read(arcTitle));
      }).catch(() => {
        Toast.generalError();
      });
    } else {
      request.delete(
        'odl_ajax_reading_unread',
        {
          id: arcId,
          token: ODL.security.token.unread
        }
      ).then(() => {
        $button.removeClass('violet');
        progressContainer.progress('decrement');
        ODL.user.readings.splice(ODL.user.readings.findIndex(id => id === arcId), 1);
        // todo: use event
        $(userRating).rating('clear rating');
        $(userRating).rating('disable');
        for (const i in stars) {
          if (stars[i] instanceof Element) {
            stars[i].title = ODL.translation.readingOrder.disabledRating;
          }
        }
        const childNodes = userRating.previousElementSibling?.childNodes || [];
        const childNode = childNodes.find(child => child.classList.contains('unrate'));
        if (childNode) childNode.style.display = 'none';
        request.delete('odl_ajax_unrate', {id: arcId});
        Toast.displayWithAvatar('success', cover, ODL.translation.readingOrder.unread(arcTitle));
      }).catch(() => {
        Toast.generalError();
      });
    }
  });
};

const arcsCounter = () => {
  const counterContainer = document.getElementById('arcs-count-label');
  const count = parseInt(counterContainer.dataset.count);
  const counterElement = counterContainer.querySelector('span');
  let i = 0;

  const counter = setInterval(() => {
    if (count === i) {
      clearInterval(counter);
    }
    counterElement.innerText = `${i}`;
    i++;
  }, 5);
}

arcsCounter();

if (ODL.security.roles.user === ODL.user.role) {
  handleUserReadings();
}

const handleArcLink = () => {
  const params = new URLSearchParams(window.location.search);
  const period = params.get("period");
  const page = params.get("page");
  const arc = params.get("arc");

  if (period && arc) {
    const container = document.getElementById(period);
    const title = container.querySelector('.period-title');
    simulateClick(title);
    if (page) {
      const pageButton = container.querySelector(`.btn_page[data-page="${page}"]`);
      simulateClick(pageButton);
    }
  }
};

handleArcLink();

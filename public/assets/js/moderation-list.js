import {Toast} from "./modules/toast.mjs";
import {request} from "./modules/request.mjs";

$(document).ready(function () {
  let $checkboxes = $('.ui.checkbox');
  $checkboxes.checkbox('uncheck');
  const $validateAllContainer = $('#validation-container');
  const $validationCount = $validateAllContainer.children('.label');
  const $validateAllButton = $validateAllContainer.children('.button');
  let count = 0;

  $('.moderation-content').on('click', '.validate-one', function () {
    const $checkbox = $(this).children('input');
    (true === $checkbox.get(0).checked) ? ++count : count--;
    $validationCount.text(count);
  });

  $validateAllButton.click(function () {
    $checkboxes = $('.ui.checkbox');
    if (0 < count) {
      const $checked = $checkboxes.find(':checked');
      const ids = []
      $checked.map((i, element) => {
        ids.push(element.dataset.arcId);
      });
      swal({
        text: ODL.translation.moderationList.confirmValidation(count),
        buttons: {
          cancel: {
            text: ODL.translation.form.cancel,
            value: false,
            visible: true,
          },
          confirm: {
            text: ODL.translation.form.confirm,
          }
        },
      }).then(function (answer) {
        if (answer) {
          request.post(
            'odl_ajax_arc_validation',
            {
              ids: ids,
              token: ODL.security.token.validate
            }
          ).then(() => {
            Toast.generalSuccess(ODL.translation.moderationList.validateSuccess);
            setTimeout(function () {
              location.reload();
            }, 3000)
          }).catch(() => {
            Toast.generalError();
          })
        }
      });
    }
  });

  $('.pagination').on('click', function () {
    count = 0;
    $validationCount.text(count);
  });
});

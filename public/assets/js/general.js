const buttonUpHandler = () => {
  const buttonUp = document.getElementById('btnup');
  if (buttonUp) {
    buttonUp.addEventListener('click', () => {
      upToContainer(document.body);
    })
  }
}
buttonUpHandler();

/**
 * Toggle slowly functions
 * https://jsfiddle.net/cferdinandi/qgpxvhhb/23/
 */
const show = element => {
  const getHeight = () => {
    element.style.display = 'block';
    const height = element.scrollHeight + 'px';
    element.style.display = '';
    return height;
  };

  const height = getHeight();
  element.classList.add('active');
  element.style.height = height;

  window.setTimeout(function () {
    element.style.height = '';
  }, 350);
};

const hide = element => {
  element.style.height = element.scrollHeight + 'px';
  window.setTimeout(function () {
    element.style.height = '0';
  }, 1);
  window.setTimeout(function () {
    element.classList.remove('active');
    element.style.height = '';
  }, 350);
};

const toggle = element => {
  if (element.classList.contains('active')) {
    hide(element);
    return;
  }
  show(element);
};
/**
 * End of Toggle slowly functions
 */

const toggleSection = () => {
  const bottomBtnHide = document.querySelectorAll('.btn_hide.down');
  for (const button of bottomBtnHide) {
    button.addEventListener('click', () => {
      hide(button.parentElement);
    })
  }
}

const upToContainer = (container) => {
  container.scrollIntoView({block: "start", inline: "nearest", behavior: 'smooth'});
}

const nl2br = str => {
  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
}

const removeSlowly = $container => {
  $container.hide('slow')
  setTimeout(() => {
    $container.remove();
  }, 1000);
}

const coverPath = filename => {
  return `${ODL.path.covers}/${filename}`;
}

const setAvailableArcLogos = (container, arc) => {
  const imgElements = container.getElementsByTagName('img');

  for (let i = 0; i < 2; i++) {
    const link = (i === 0) ? arc.linkA : arc.linkB;
    const logo = imgElements[i];

    if (link) {
      const linkElement = document.createElement('a');
      linkElement.href = link;
      linkElement.target = '_blank';
      const newLogo = logo.cloneNode();
      newLogo.classList.remove('disabled', 'ui', 'mini', 'image');
      newLogo.title = '';
      linkElement.append(newLogo);
      linkElement.classList.add('ui', 'mini', 'image');
      container.replaceChild(linkElement, logo);
    }
  }
}

function copyToClipboard(textToCopy) {
  if (navigator.clipboard && window.isSecureContext) {
    return navigator.clipboard.writeText(textToCopy);
  } else {
    const textArea = document.createElement("textarea");
    textArea.value = textToCopy;
    textArea.style.position = "fixed";
    textArea.style.left = "-999999px";
    textArea.style.top = "-999999px";
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();
    return new Promise((res, rej) => {
      document.execCommand('copy') ? res() : rej();
      textArea.remove();
    });
  }
}

function simulateClick(element) {
  const event = new MouseEvent('click', {
    view: window,
    bubbles: true,
    cancelable: true
  });
  element.dispatchEvent(event);
}

import {Toast} from "./toast.mjs";
import {request} from "./request.mjs";

const ArcEditor = {
  updateArc: async ($button) => {
    const toValidate = window.location.pathname === Routing.generate('odl_admin_arc_moderation_list');
    $('#update-inline').remove();
    const $line = $button.parents('.arc-line');
    const id = $line.attr('id');
    const position = $line.data('position');

    try {
      const form = await request.get('odl_ajax_arc_get_update_form', {
        id,
        toValidate
      });

      const $form = $(form);
      const coverInputLabel = $form.find('.arc_form_cover_label').text();
      $form.find('.arc_form_cover_label').html(`<i class="icon upload"></i> ${coverInputLabel}`);
      $line.after($form);
      $('#update-inline').show('slow');
      $('#update-inline').get(0).dispatchEvent(ODL.events.arc.update);
      $('#form_arc_update').submit(function (e) {
        e.preventDefault();
        const formData = this;
        request.post(
          'odl_ajax_arc_update',
          {
            id: id,
            token: ODL.security.token.update
          },
          new FormData(formData)
        ).then(data => {
          $form.addClass('loading');
          const arc = data.arc;
          const cover = coverPath(arc.cover);

          Toast.displayWithAvatar(data.type, cover, data.message);
          if (arc.position !== parseInt(position)) {
            setTimeout(() => {
              location.reload();
            }, 3000);
          }

          // Update data on front-end
          $line.find('.arc-cover img').attr('src', cover);
          $line.find('.arc-title h3').text(arc.title);
          $line.find('.arc-description p').html(nl2br(arc.content));
          if (arc.isEvent) {
            $line.addClass('isEvent');
          } else {
            $line.removeClass('isEvent');
          }
          $form.removeClass('loading');
        }).catch(() => {
          Toast.generalError();
        });
      });

      $('#update-close').click(function () {
        removeSlowly($('#update-inline'));
      });
      $('#fake-input').click(function () {
        $('#arc_form_cover').click();
      });
      const fileInput = document.querySelector('#arc_form_cover');
      fileInput.addEventListener('change', function (event) {
        var text = $(this)
          .val()
          .replace('C:\\fakepath\\', '');
        $('#result-file-selected').text(text);
      });
    } catch (error) {
      Toast.generalError();
    }
  },

  removeArc: ($button) => {
    const $line = $button.parents('.arc-line');
    const title = $line.find('.arc-title').children('h3').text();
    const $periodContainer = $line.parents('div.period-container');
    const lines = $periodContainer.find('.arc-line');
    const $counter = $periodContainer.find('.arcs-count').children('span');
    let count = parseInt($counter.text());
    const id = $line.attr('id');
    const position = $line.data('position');
    swal({
      dangerMode: true,
      icon: 'warning',
      title: ODL.translation.general.dangerZone,
      text: ODL.translation.readingOrder.confirmRemoveArc(title),
      buttons: {
        cancel: {
          text: ODL.translation.form.cancel,
          value: false,
          visible: true,
        },
        confirm: {
          text: ODL.translation.form.confirm,
        }
      },
      className: 'swal-danger-mode'
    }).then(function (answer) {
      if (answer) {
        request.delete('odl_ajax_arc_remove', {id}, {token: ODL.security.token.remove})
          .then(() => {
            // Remove arc
            removeSlowly($line);
            const $updateForm = $('#update-inline');
            if (0 !== $updateForm.length) {
              removeSlowly($updateForm);
            }
            Toast.generalSuccess(ODL.translation.general.removeSuccess);

            // Readjust all position
            lines.each(function () {
              let positionSpan = $(this).find('.arc-position > span');
              let currentPosition = parseInt($(this).data('position'));
              if (currentPosition > position) {
                let newPosition = (--currentPosition).toString();
                positionSpan.text(newPosition);
                $(this).attr('data-position', newPosition);
              }
            });

            // Readjust the total count
            $counter.text(--count);
          }).catch(() => {
          Toast.generalError();
        });
      }
    });
  },
}

export {ArcEditor};

const Toast = {
  generalError: () => {
    $('body').toast({
      class: 'error',
      showIcon: 'bug',
      displayTime: 0,
      message: ODL.translation.general.generalError
    });
  },
  generalSuccess: (message = ODL.translation.general.confirmSuccess, time = 3000) => {
    $('body').toast({
      class: 'success',
      showIcon: 'check',
      displayTime: time,
      showProgress: 'bottom',
      classProgress: 'blue',
      pauseOnHover: true,
      message: message
    });
  },
  displayWithAvatar: (type, avatar, message = ODL.translation.general.confirmSuccess, time = 3000) => {
    $('body').toast({
      class: type,
      displayTime: 3000,
      showProgress: 'bottom',
      classProgress: 'blue',
      pauseOnHover: true,
      message: message,
      showImage: avatar,
      classImage: 'avatar'
    });
  }
}

export {Toast};

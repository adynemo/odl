import {request} from "./request.mjs";

const pieConfig = {
  strokeBottom: 5,
  stroke: 2,
  fill: "rgba(133,133,133,0.49)",
  colorCircle: "transparent",
  round: true,
  size: 80,
}

const ArcLineHandler = {
  appendLine: (arcs, html, arcsContainer) => {
    const anchorListener = function (e) {
      const period = arcsContainer.parentNode.parentNode.id;
      const pagination = arcsContainer.previousElementSibling;
      let query = `?period=${period}&arc=${e.currentTarget.parentElement.id}`;
      if (pagination) {
        const page = pagination.querySelector('.btn_page.active').dataset.page;
        query += `&page=${page}`;
      }
      const uri = `${window.location.origin}${window.location.pathname}`;
      copyToClipboard(`${uri}${query}`);
    };
    for (const arc of arcs) {
      const placeholder = document.createElement('div');
      placeholder.innerHTML = html;
      const line = placeholder.firstElementChild;
      placeholder.remove();

      if (false === arc.isEvent) {
        line.querySelector('.crossover-logo').remove();
      } else {
        line.classList.add('isEvent');
      }

      line.id = arc.id;
      line.dataset.position = arc.position;
      line.querySelector('.arc-position > span').innerText = arc.position;
      const cover = line.querySelector('.arc-cover > img');
      cover.src = coverPath(arc.cover);
      cover.alt = arc.title;
      cover.title = arc.title;
      line.querySelector('.arc-title > h3').innerHTML = arc.title;
      line.querySelector('.arc-description > p').innerHTML = nl2br(arc.content);


      const logosContainer = line.querySelector('.arc-logos div');
      if (logosContainer) {
        setAvailableArcLogos(logosContainer, arc)
      }

      const buttonRead = line.querySelector('.user-actions .button.read');
      if (buttonRead) {
        buttonRead.dataset.arcId = arc.id;
        buttonRead.dataset.arcTitle = arc.title;
      }

      const editor = line.querySelector('.arc-editor');
      if (editor?.children) {
        for (const child of editor.children) {
          child.dataset.arcId = arc.id;
        }
      }

      // todo: use event
      const arcRates = ODL.data.rates?.filter(rate => rate.arc === arc.id) || [];
      const totalRatings = arcRates.length;
      if (totalRatings) {
        let rate = 0;
        for (const arcRate of arcRates) {
          rate += arcRate.value;
        }
        const percent = computePercent(rate, totalRatings);

        const pie = line.querySelector(".rating-container .pie");
        pie.dataset.pie = JSON.stringify({percent, colorSlice: percentColorMap(percent)})
        const totalContainer = line.querySelector(".rating-container .total-rating");
        totalContainer.innerText = ODL.translation.readingOrder.totalRatings(totalRatings);
      }

      // todo: use event
      const unrateButton = line.querySelector('.rating-container .unrate');
      unrateButton.dataset.arcId = arc.id;
      unrateButton.addEventListener('click', unratingListener);

      // todo: use event
      const userRating = line.querySelector('.rating-container .rating');
      if (userRating) {
        const userRate = ODL.user.rates.find(rate => rate.arc === arc.id);
        if (userRate) {
          userRating.dataset.rating = userRate.value;
          userRating.dataset.arcId = arc.id;
          unrateButton.style.display = 'inline';
        }
        $(userRating).rating({maxRating: 5});
        userRating.dataset.arcId = arc.id;
        if (!ODL.user.readings.includes(arc.id)) {
          userRating.classList.add('disabled');
          userRating.title = ODL.translation.readingOrder.disabledRating;
        }
        const stars = userRating.childNodes;
        for (const i in stars) {
          if (stars[i] instanceof Element && ODL.user.readings.includes(arc.id)) {
            stars[i].title = ODL.translation.readingOrder.starsTitles[i];
          }
        }
        userRating.addEventListener('click', ratingListener);
      }

      line.querySelector('.arc-position').addEventListener('click', anchorListener);

      arcsContainer.append(line);
    }

    const circle = new CircularProgressBar('pie', pieConfig);
    circle.initial();
    arcsContainer.dispatchEvent(ODL.events.arc.update);
  }
};

const percentColorMap = (percent) => {
  if (0 <= percent && 25 > percent) return '#7c0004';
  if (25 <= percent && 50 > percent) return '#ff5400';
  if (50 <= percent && 75 > percent) return '#ffb800';
  if (75 <= percent && 100 >= percent) return '#1b7c00';
}

const computePercent = (value, total) => (100 * value) / (total * 5);


const ratingListener = (e) => {
  const arcId = Number.parseInt(e.currentTarget.dataset.arcId);
  if (!ODL.user.readings.includes(arcId)) {
    return;
  }
  const value = $(e.currentTarget).rating('get rating');
  request.post('odl_ajax_rate', {id: arcId}, {rate: value});
  const childNodes = e.currentTarget?.previousElementSibling?.childNodes || [];
  const childNode = childNodes.find(child => child.classList.contains('unrate'));
  if (childNode) childNode.style.display = 'inline';
};

const unratingListener = (e) => {
  const arcId = Number.parseInt(e.currentTarget.dataset.arcId);
  if (!ODL.user.readings.includes(arcId)) {
    return;
  }
  request.delete('odl_ajax_unrate', {id: arcId});
  e.currentTarget.style.display = 'none';
  const userRating = e.currentTarget.parentElement.nextElementSibling;
  if (userRating.classList.contains('rating')) {
    $(userRating).rating('clear rating');
  }
}

export default ArcLineHandler;

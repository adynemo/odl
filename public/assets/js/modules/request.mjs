const encodeForm = (data) => {
  const formBody = [];
  for (const property in data) {
    const encodedKey = encodeURIComponent(property);
    const encodedValue = encodeURIComponent(data[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }

  return formBody.join("&");
}

const doRequest = (method, routeName, params, body) => {
  const url = Routing.generate(routeName, params);
  const options = {
    method,
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    }
  };
  if (body) {
    if (body instanceof FormData) {
      options.body = body;
    } else if (0 < Object.keys(body).length) {
      options.body = encodeForm(body);
      options.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    }
  }
  return fetch(url, options);
}

const checkResponse = (response, reject) => {
  if (response.ok) {
    const contentType = response.headers.get('Content-Type');
    if (contentType && -1 !== contentType.indexOf('application/json')) {
      return response.json();
    }

    return response.text();
  }

  reject(response);
}

export const request = {
  get: (routeName, params = {}) => {
    return new Promise((resolve, reject) => {
      doRequest('GET', routeName, params)
        .then(response => checkResponse(response, reject))
        .then(data => resolve(data))
        .catch(error => reject(error));
    });
  },
  post: (routeName, params = {}, body = {}) => {
    return new Promise((resolve, reject) => {
      doRequest('POST', routeName, params, body)
        .then(response => checkResponse(response, reject))
        .then(data => resolve(data))
        .catch(error => reject(error));
    });
  },
  delete: (routeName, params = {}, body = {}) => {
    return new Promise((resolve, reject) => {
      doRequest('DELETE', routeName, params, body)
        .then(response => checkResponse(response, reject))
        .then(data => resolve(data))
        .catch(error => reject(error));
    });
  }
}

import {Toast} from "./toast.mjs";
import {request} from "./request.mjs";
import {ReadingOrder} from "../objects/ReadingOrder.js";

const order = new ReadingOrder();

const handleNavigation = (paginationContainers, currentPage, navigateTo, toLeft = false) => {
  const buttonsNext = paginationContainers.find('.btn_next');
  const buttonsPrevious = paginationContainers.find('.btn_prev');
  const buttonsPage = paginationContainers.find('.btn_page');
  const lastButton = buttonsPage.get(buttonsPage.length - 1);
  if (false === toLeft) {
    if (1 === parseInt(currentPage)) {
      buttonsPrevious.attr('disabled', false);
    }
    if (navigateTo === parseInt(lastButton.dataset.page)) {
      buttonsNext.attr('disabled', true);
    }
  } else {
    if (currentPage === parseInt(lastButton.dataset.page)) {
      buttonsNext.attr('disabled', false);
    }
    if (1 === navigateTo) {
      buttonsPrevious.attr('disabled', true);
    }
  }
};

const Pagination = {
  navigate: async ($container, buildPage, toLeft = false) => {
    if ('function' !== typeof buildPage) {
      throw new Error('You must pass a callable for buildPage');
    }
    const paginationContainers = $container.children('.pagination');
    const $activeButtonPage = paginationContainers.find('.btn_page.active').removeClass('active');
    const currentPage = $activeButtonPage.data('page');
    const navigateTo = (false === toLeft) ? currentPage + 1 : currentPage - 1;

    await buildPage($container, navigateTo);

    paginationContainers.find(`.btn_page[data-page="${navigateTo}"]`).addClass('active');
    handleNavigation(paginationContainers, currentPage, navigateTo, toLeft);
  },

  navigateByPage: async ($container, navigateTo, buildPage) => {
    if ('function' !== typeof buildPage) {
      throw new Error('You must pass a callable for buildPage');
    }
    const paginationContainers = $container.children('.pagination');
    const $activeButtonPage = paginationContainers.find('.btn_page.active');
    const currentPage = $activeButtonPage.data('page');
    const toLeft = parseInt(navigateTo) < parseInt(currentPage);

    if (parseInt(navigateTo) === parseInt(currentPage)) return;
    $activeButtonPage.removeClass('active');
    await buildPage($container, navigateTo);
    paginationContainers.find(`.btn_page[data-page="${navigateTo}"]`).addClass('active');
    handleNavigation(paginationContainers, currentPage, navigateTo, toLeft);
  },

  getPage: async (routeName, params) => {
    const periodId = params.id ?? 'no-period';
    const page = params.page;

    if (order.isPageExists(periodId, page)) {
      return order.getPage(periodId, page);
    }

    try {
      const response = await request.get(routeName, params);
      order.addPage(periodId, page, response)

      return response;
    } catch (error) {
      Toast.generalError();
    }
  }
}

export {Pagination};

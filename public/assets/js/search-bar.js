import {ArcEditor} from "./modules/arc-editor.mjs";
import {request} from "./modules/request.mjs";
import ArcLineHandler from "./modules/arc-line-handler.mjs";

$(document).ready(async function () {
  const categoryContent = [];
  const searchBar = document.getElementById('search-bar');
  const resultsContainer = document.getElementById('search-results');
  let from = (typeof ODL.data.eraId !== 'undefined') ? ODL.data.eraId : null;

  // For reading order pages
  if (typeof ODL.data.periods !== 'undefined') {
    for (let key in ODL.data.periods) {
      const period = ODL.data.periods[key];

      try {
        const arcs = await request.get('odl_ajax_arcsByPeriod', {id: period['id']});
        for (let key in arcs) {
          const content = {
            category: period['name'],
            id: arcs[key]['id'],
            image: ODL.data.pathAssetCover + arcs[key]['cover'],
            title: arcs[key]['title'],
            description: arcs[key]['content'],
          };
          categoryContent.push(content);
        }
      } catch (error) {
        Toast.generalError();
      }
    }

    searchBar.addEventListener('keyup', function (e) {
      resultsContainer.innerText = '';
      const term = e.target.value.toLowerCase();
      let category = '';
      let loop = 0;

      if (term) {
        categoryContent.forEach(function (arc) {
          if (loop <= 2) {
            if (arc.title.toLowerCase().indexOf(term) !== -1 || arc.description.toLowerCase().indexOf(term) !== -1) {
              loop++;
              const html =
                `<div class="image">
                  <img src="${arc.image}" alt="">
                </div>
                <div class="content">
                  <div class="title">${arc.title}</div>
                  <div class="description">${arc.description}</div>
                </div>`;
              const template = document.createElement('a');
              template.className = 'result';
              template.dataset.id = arc.id;
              template.addEventListener('click', clickOnResult);
              template.innerHTML = html.trim();
              let categoryTemplate;
              if (category !== arc.category) {
                category = arc.category;
                categoryTemplate = document.createElement('div');
                categoryTemplate.className = 'category';
                const categoryNameTemplate = document.createElement('div');
                categoryNameTemplate.className = 'name';
                categoryNameTemplate.innerHTML = arc.category;
                const resultsTemplate = document.createElement('div');
                resultsTemplate.className = 'results';
                categoryTemplate.appendChild(categoryNameTemplate);
                categoryTemplate.appendChild(resultsTemplate);
              } else {
                const categoriesTemplate = resultsContainer.getElementsByClassName('category');
                for (let i = 0; i < categoriesTemplate.length; i++) {
                  categoryTemplate = categoriesTemplate[i];
                  if (categoryTemplate.getElementsByClassName('name')[0].innerHTML === arc.category) {
                    break;
                  }
                }
              }
              categoryTemplate.getElementsByClassName('results')[0].appendChild(template);
              resultsContainer.appendChild(categoryTemplate);
            }
          }
        });
        const viewMore = document.createElement('a');
        viewMore.className = 'ui bottom attached button';
        viewMore.innerText = ODL.translation.general.viewmore;
        viewMore.href = generateUrl(term);
        resultsContainer.appendChild(viewMore);
        resultsContainer.classList.add('transition', 'visible');
        resultsContainer.style.display = 'block';
      }
    });
  }

  const generateUrl = term => {
    let encodedTerm = encodeURI(term);
    let query = {q: encodedTerm};
    if (null !== from) {
      query.era = from;
    }
    return Routing.generate('odl_search_arcs', query);
  }

  searchBar.addEventListener('keyup', function (e) {
    const value = e.target.value;
    if ((e.key === 'Enter') && (3 <= value.length)) {
      const term = value.toLowerCase();
      window.location = generateUrl(term);
    }
  });

  searchBar.onblur = function () {
    setTimeout(function () {
      resultsContainer.style.display = 'none';
      resultsContainer.classList.remove('transition', 'visible');
    }, 200);
  };

  searchBar.onfocus = function () {
    if (resultsContainer.getElementsByClassName('results').length) {
      resultsContainer.classList.add('transition', 'visible');
      resultsContainer.style.display = 'block';
    }
  };

  function clickOnResult(e) {
    const target = e.target;
    const id = (true === target.classList.contains('result')) ?
      target.dataset.id :
      target.parentNode.parentNode.dataset.id;

    request.get('odl_search_arc', {id})
      .then(response => {
        const modal = document.createElement('div');
        modal.className = 'ui modal';
        modal.setAttribute('id', 'result-arc');
        const contentContainer = document.createElement('div');
        contentContainer.className = 'content arc odl-dmd';
        const searchContainer = document.createElement('div');
        searchContainer.className = 'search-content';
        const arcsContainer = document.createElement('div');
        arcsContainer.className = 'arcs-container';
        const title = document.createElement('div');
        title.className = 'header odl-dmd';
        title.innerText = response.title;
        ArcLineHandler.appendLine([response.arc], response.html, arcsContainer)
        searchContainer.appendChild(arcsContainer);
        contentContainer.appendChild(searchContainer);
        modal.appendChild(title);
        modal.appendChild(contentContainer);
        document.getElementsByTagName('body')[0].appendChild(modal);
        modal.dispatchEvent(ODL.events.arc.update);
        $('.ui.modal').modal({
          onHidden: () => {
            $('.dimmer').remove()
          }
        }).modal('show');
        $(modal).on('click', '.edit', async function () {
          await ArcEditor.updateArc($(this));
        });
        $(modal).on('click', '.remove', function () {
          ArcEditor.removeArc($(this));
          $('.dimmer').remove();
          setTimeout(() => {
            location.reload();
          }, 2000);
        });
      });
  }
});

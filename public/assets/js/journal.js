const paginationNav = document.querySelector('.ui.pagination.menu');
const actualPage = Number.parseInt(paginationNav.dataset.page);
const count = paginationNav.dataset.count;
const maxElements = paginationNav.dataset.maxElements;
const countPages = Math.round(count / maxElements);
const maxPage = 5;
const partial = countPages > maxPage;
const maxButtons = 6;
const left = 1;
const middle = 3;
const right = 5;
const limit = 4;
let page = 0;

for (let i = 0; i <= maxButtons; i++) {
  const item = document.createElement('a');
  item.classList.add('item');
  const disabledItem = document.createElement('a');
  disabledItem.classList.add('item');
  disabledItem.text = '...';
  disabledItem.classList.add('disabled');
  if (0 === i) {
    item.text = '1';
    item.dataset.page = '1';
    item.href = Routing.generate('odl_admin_journal');
    paginationNav.append(item);
    continue;
  }

  if (
    (i === left && limit <= actualPage) ||
    (i === right && maxPage < countPages && 1 < (countPages - page))
  ) {
    paginationNav.append(disabledItem);
    continue;
  }

  if (!partial || actualPage <= middle) {
    page = i + 1;
  } else {
    if (i === middle - 1) page = actualPage - 1;
    if (i === middle) page = actualPage;
    if (i === middle + 1) page = actualPage + 1;
    if (i === right) continue;
  }

  if (page < countPages) {
    item.text = `${page}`;
    item.dataset.page = `${page}`;
    item.href = Routing.generate('odl_admin_journal', {page});
    paginationNav.append(item);
  }

  if (countPages === i || maxButtons === i) {
    item.text = `${countPages}`;
    item.dataset.page = `${countPages}`;
    item.href = Routing.generate('odl_admin_journal', {page: countPages});
    paginationNav.append(item);
    break;
  }
}

const items = paginationNav.querySelectorAll('.item:not(.disabled)');
for (const item of items) {
  if (actualPage === Number.parseInt(item.dataset.page)) {
    item.classList.add('active');
  }
}

$('#garbage').on('click', '.ui.button', async function (e) {
  e.preventDefault();
  const confirm = await swal({
    dangerMode: true,
    icon: 'warning',
    text: ODL.translation.journal.confirmGarbage,
    buttons: {
      cancel: {
        text: ODL.translation.form.cancel,
        value: false,
        visible: true,
      },
      confirm: {
        text: ODL.translation.form.confirm,
      }
    },
    className: 'swal-danger-mode'
  });

  if (true === confirm) {
    e.currentTarget.submit();
  }
})

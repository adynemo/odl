import {Toast} from "./modules/toast.mjs";
import {request} from "./modules/request.mjs";

$(document).ready(function () {
  $('.delete-user').click(function () {
    const button = $(this);
    const username = button.data('username');
    const id = button.data('user-id');
    const user = button.parents('.item');
    swal({
      dangerMode: true,
      icon: 'warning',
      className: 'swal-danger-mode',
      text: ODL.translation.users.confirmDeleteUser(username),
      buttons: {
        cancel: {
          text: ODL.translation.form.cancel,
          value: false,
          visible: true,
        },
        confirm: {
          text: ODL.translation.form.confirm,
        }
      },
    }).then(function (answer) {
      if (answer) {
        request.delete('odl_ajax_user_delete', {
          id,
          token: ODL.security.token.delete
        })
          .then(() => {
            $('body').toast({
              class: 'success',
              showIcon: 'check circle',
              displayTime: 0,
              closeIcon: true,
              message: ODL.translation.users.confirmSuccess(username),
            });
            user.remove()
          }).catch(() => {
          Toast.generalError();
        });
      }
    });
  });
});

import {Toast} from "./modules/toast.mjs";
import {request} from "./modules/request.mjs";

const dataFromTwig = document.querySelector('#twig-to-js');
const entityType = dataFromTwig.dataset.entityTypeClean;
const childEntity = dataFromTwig.dataset.childEntity;
const pathAssetsBanner = dataFromTwig.dataset.pathAssetsBanner;
const pathAssetsLogos = dataFromTwig.dataset.pathAssetsLogos;
const ajaxDeletePathName = 'odl_ajax_' + entityType + '_delete_' + entityType;
// Store the div that contains the 'data-prototype'
const container = $('div#collection_form_collection');

// Define a count to name fields will be dynamically added
let index = container.find(':input').length;

// Function to add entity
const addEntity = (container) => {
  const template = container.attr('data-prototype').replace(/__name__/g, index);

  const prototype = $(template);
  addLinks(prototype);
  container.append(prototype);

  index++;
}

const createImgElement = (src, alt) => {
  const element = document.createElement('img');
  element.src = src;
  element.alt = alt;

  return element;
}

const addImages = (object, id) => {
  request.get('odl_ajax_era_get_era', {id})
    .then((entity) => {
      if (entity.image !== undefined && entity.image !== '') {
        const path = pathAssetsBanner + entity.image;
        object.find('[id$="image"]').after(createImgElement(path, 'banner'));
      }
      if (entity.logoLinkA !== undefined && entity.logoLinkA !== '') {
        const path = pathAssetsLogos + entity.logoLinkA;
        object.find('[id$="_logoLinkA"]').after(createImgElement(path, 'logo'));
      }
      if (entity.logoLinkB !== undefined && entity.logoLinkB !== '') {
        const path = pathAssetsLogos + entity.logoLinkB;
        object.find('[id$="_logoLinkB"]').after(createImgElement(path, 'logo'));
      }
    })
}

const addUniversesLogo = (object, id) => {
  request.get('odl_ajax_universe_get_universe', {id})
    .then((entity) => {
      if (entity.logo !== undefined && entity.logo !== '') {
        const path = pathAssetsLogos + entity.logo;
        object.find('[id$="_logo"]').after(createImgElement(path, 'logo'));
      }
    })
}

// Function to add 'delete' ad 'edit' buttons
const addLinks = (prototype, id = '') => {
  const containerButtons = $('<div class="ui buttons" id="' + id + '"></div>');
  const deleteLink = $(`<button type="button" class="ui red button remove_entity">${ODL.translation.general.remove}</button>`);

  if ('' !== childEntity) {
    const childEntityLink = $(`<a href=""><button type="button" class="ui button edit_childentity">${ODL.translation.general.edit}</button></a>`);

    const childEntityHref = Routing.generate('odl_admin_manage_' + childEntity, {id: id});
    childEntityLink.attr('href', childEntityHref);

    containerButtons.append(deleteLink, childEntityLink);
    prototype.append(containerButtons, '<div class="ui divider"></div>');
  } else {
    prototype.append(deleteLink);
  }

  deleteLink.click(function (e) {
    if (id !== '') {
      swal({
        dangerMode: true,
        icon: 'warning',
        title: ODL.translation.general.dangerZone,
        text: ODL.translation.readingOrder.confirmRemoveSection,
        buttons: {
          cancel: {
            text: ODL.translation.form.cancel,
            value: false,
            visible: true,
          },
          confirm: {
            text: ODL.translation.form.confirm,
          }
        },
        className: 'swal-danger-mode'
      }).then((answer) => {
        if (answer) {
          request.delete(
            ajaxDeletePathName,
            {id},
            {token: ODL.security.token.remove}
          ).then(() => {
            Toast.generalSuccess(ODL.translation.general.removeSuccess);
            prototype.remove();
          }).catch(() => {
            Toast.generalError();
          });
        }
      });
    } else {
      prototype.remove();
    }

    e.preventDefault();
    return false;
  });
}

// Add fields for new entity when the button 'add' is clicked
$('#add_entity').click(function (e) {
  addEntity(container);

  e.preventDefault();
  return false;
});

// Add fields for a first entity if there no exists
if (index === 0) {
  addEntity(container);
} else {
  // Add additional at the existing entities
  const re = new RegExp('collection_form_collection_[\\w\\d]+');
  container.children('div').each(function () {
    const attrId = $(this).children('div').attr('id');
    if (re.test(attrId)) {
      const id = $(this).find('#' + attrId + '_id').val();
      if (entityType === 'era') {
        addImages($(this), id)
      }
      if (entityType === 'universe') {
        addUniversesLogo($(this), id);
      }
      addLinks($(this), id);
    }
  });
}

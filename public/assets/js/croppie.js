import {request} from "./modules/request.mjs";

$(document).ready(function () {
  const croppieContainer = $('#avatar-croppie');
  const userId = croppieContainer.data('user-id');
  let $uploadCrop;

  const readFile = (input) => {
    if (input.files && input.files[0]) {
      const reader = new FileReader();

      reader.onload = function (e) {
        $('.upload-avatar').addClass('ready');
        $uploadCrop.croppie('bind', {
          url: e.target.result
        });
      };

      reader.readAsDataURL(input.files[0]);
    } else {
      swal(ODL.translation.profile.not_supported);
    }
  }

  $uploadCrop = $(croppieContainer).croppie({
    enableExif: true,
    viewport: {
      width: 100,
      height: 100,
      type: 'circle'
    },
    boundary: {
      width: 200,
      height: 200
    }
  });

  $('#user_data_form_avatar_upload').on('change', function () {
    readFile(this);
  });

  $('.upload-result').on('click', function (ev) {
    $uploadCrop
      .croppie('result', {
        type: 'canvas',
        size: 'viewport'
      })
      .then(function (resp) {
        popupResult({
          src: resp
        });
      });
  });

  $('.upload-valid').on('click', function (ev) {
    $uploadCrop
      .croppie('result', {
        type: 'base64',
        size: 'viewport'
      })
      .then(function (resp) {
        $('#user_data_form_avatar_base64').val(resp);
        swal(ODL.translation.profile.validate);
      });
  });

  $('.user-avatar .close.icon').on('click', function () {
    swal({
      dangerMode: true,
      icon: 'warning',
      text: ODL.translation.profile.comfirm_removal,
      buttons: {
        cancel: {
          text: ODL.translation.form.cancel,
          value: false,
          visible: true,
        },
        confirm: {
          text: ODL.translation.form.confirm,
        }
      },
      className: 'swal-danger-mode'
    }).then(function (response) {
      if (response) {
        request.delete('odl_ajax_user_avatar_remove', {
          id: userId,
          token: ODL.security.token.remove
        })
          .then(() => {
            $('.user-avatar').remove();
          })
      }
    });
  });

  const popupResult = (result) => {
    swal({
      title: '',
      content: {
        element: 'img',
        attributes: {
          src: result.src
        }
      },
      allowOutsideClick: true
    });
    setTimeout(function () {
      $('.sweet-alert').css('margin', function () {
        const top = -1 * ($(this).height() / 2);
        const left = -1 * ($(this).width() / 2);

        return top + 'px 0 0 ' + left + 'px';
      });
    }, 1);
  }
});

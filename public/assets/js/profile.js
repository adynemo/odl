document.getElementById('delete_account_form_save').addEventListener('click', async function (e) {
  e.preventDefault();
  const confirm = await swal({
    dangerMode: true,
    icon: 'warning',
    text: ODL.translation.profile.confirm_account_removal_modal,
    buttons: {
      cancel: {
        text: ODL.translation.form.cancel,
        value: false,
        visible: true,
      },
      confirm: {
        text: ODL.translation.form.confirm,
      }
    },
    className: 'swal-danger-mode'
  });

  if (true === confirm) {
    const deleteForm = document.querySelector('form[name="delete_account_form"]');
    deleteForm.submit();
  }
});

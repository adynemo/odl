<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201223205946 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Remove join between journal to user and arc';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE journal DROP FOREIGN KEY FK_C1A7E74D41EB8A3C');
        $this->addSql('ALTER TABLE journal DROP FOREIGN KEY FK_C1A7E74DA76ED395');
        $this->addSql('DROP INDEX IDX_C1A7E74DA76ED395 ON journal');
        $this->addSql('DROP INDEX IDX_C1A7E74D41EB8A3C ON journal');
        $this->addSql('ALTER TABLE journal ADD username VARCHAR(180) NOT NULL, ADD reading_order VARCHAR(255) NOT NULL, ADD `period` VARCHAR(255) NOT NULL, DROP `user_id`, CHANGE arc_id arc_id INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE journal ADD `user_id` INT NOT NULL, DROP username, DROP reading_order, DROP `period`, CHANGE arc_id arc_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE journal ADD CONSTRAINT FK_C1A7E74D41EB8A3C FOREIGN KEY (arc_id) REFERENCES arc (id)');
        $this->addSql('ALTER TABLE journal ADD CONSTRAINT FK_C1A7E74DA76ED395 FOREIGN KEY (`user_id`) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C1A7E74DA76ED395 ON journal (`user_id`)');
        $this->addSql('CREATE INDEX IDX_C1A7E74D41EB8A3C ON journal (arc_id)');
    }
}

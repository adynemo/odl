<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211017000454 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add description field to each section';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE era ADD description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE period ADD description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE universe ADD description LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE era DROP description');
        $this->addSql('ALTER TABLE period DROP description');
        $this->addSql('ALTER TABLE universe DROP description');
    }
}

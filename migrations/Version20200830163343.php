<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200830163343 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create journal table and remove changelog';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE journal (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, arc_id INT DEFAULT NULL, status VARCHAR(7) NOT NULL, position INT NOT NULL, new_position INT DEFAULT 0 NOT NULL, title LONGTEXT NOT NULL, new_title LONGTEXT NOT NULL, cover TINYINT(1) DEFAULT \'0\' NOT NULL, content TINYINT(1) DEFAULT \'0\' NOT NULL, link_a TINYINT(1) DEFAULT \'0\' NOT NULL, link_b TINYINT(1) DEFAULT \'0\' NOT NULL, is_event INT DEFAULT 0 NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_C1A7E74DA76ED395 (user_id), INDEX IDX_C1A7E74D41EB8A3C (arc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE journal ADD CONSTRAINT FK_C1A7E74DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE journal ADD CONSTRAINT FK_C1A7E74D41EB8A3C FOREIGN KEY (arc_id) REFERENCES arc (id)');
        $this->addSql('DROP TABLE changelog');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE changelog (id INT AUTO_INCREMENT NOT NULL, author LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, cl_type LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, name_universe LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, name_era LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, name_period LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, old_position LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, new_position LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, title LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, new_title LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, cover LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, content LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, link_a LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, link_b LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, is_event LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE journal');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211017231946 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Avoid UniqueConstraintViolationException on position fields due to multiple updates';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_96E19A635CD9AF2462CE4F5 ON era');
        $this->addSql('ALTER TABLE era ADD enabled_unique_key TINYINT(1) DEFAULT \'1\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96E19A635CD9AF2462CE4F5B364553F ON era (universe_id, position, enabled_unique_key)');
        $this->addSql('DROP INDEX UNIQ_C5B81ECE707300A1462CE4F5 ON period');
        $this->addSql('ALTER TABLE period ADD enabled_unique_key TINYINT(1) DEFAULT \'1\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C5B81ECE707300A1462CE4F5B364553F ON period (era_id, position, enabled_unique_key)');
        $this->addSql('DROP INDEX UNIQ_61353835462CE4F5 ON universe');
        $this->addSql('ALTER TABLE universe ADD enabled_unique_key TINYINT(1) DEFAULT \'1\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_61353835462CE4F5B364553F ON universe (position, enabled_unique_key)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_96E19A635CD9AF2462CE4F5B364553F ON era');
        $this->addSql('ALTER TABLE era DROP enabled_unique_key');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96E19A635CD9AF2462CE4F5 ON era (universe_id, position)');
        $this->addSql('DROP INDEX UNIQ_C5B81ECE707300A1462CE4F5B364553F ON period');
        $this->addSql('ALTER TABLE period DROP enabled_unique_key');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C5B81ECE707300A1462CE4F5 ON period (era_id, position)');
        $this->addSql('DROP INDEX UNIQ_61353835462CE4F5B364553F ON universe');
        $this->addSql('ALTER TABLE universe DROP enabled_unique_key');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_61353835462CE4F5 ON universe (position)');
    }
}

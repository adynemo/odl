<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210315000211 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Make `positions` field unique for each section';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE era CHANGE logo_link_a logo_link_a VARCHAR(37) DEFAULT \'\' NOT NULL, CHANGE logo_link_b logo_link_b VARCHAR(37) DEFAULT \'\' NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96E19A635CD9AF2462CE4F5 ON era (universe_id, position)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C5B81ECE707300A1462CE4F5 ON period (era_id, position)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_61353835462CE4F5 ON universe (position)');
        $this->addSql('ALTER TABLE user CHANGE api_token api_token VARCHAR(36) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_96E19A635CD9AF2462CE4F5 ON era');
        $this->addSql('ALTER TABLE era CHANGE logo_link_a logo_link_a VARCHAR(37) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE logo_link_b logo_link_b VARCHAR(37) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('DROP INDEX UNIQ_C5B81ECE707300A1462CE4F5 ON period');
        $this->addSql('DROP INDEX UNIQ_61353835462CE4F5 ON universe');
        $this->addSql('ALTER TABLE user CHANGE api_token api_token VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}

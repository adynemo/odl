<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210330033143 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create relation between eras and users to allows editors to edit eras arcs';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_era (user_id INT NOT NULL, era_id VARCHAR(13) NOT NULL, INDEX IDX_7DF7FBE8A76ED395 (user_id), INDEX IDX_7DF7FBE8707300A1 (era_id), PRIMARY KEY(user_id, era_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_era ADD CONSTRAINT FK_7DF7FBE8A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_era ADD CONSTRAINT FK_7DF7FBE8707300A1 FOREIGN KEY (era_id) REFERENCES era (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lexik_maintenance (ttl DATETIME DEFAULT NULL) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200319182712 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add logos for link_a and link_b of arcs';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE era ADD logo_link_a VARCHAR(37) NOT NULL DEFAULT \'\', ADD logo_link_b VARCHAR(37) NOT NULL DEFAULT \'\', CHANGE image image VARCHAR(37) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE era DROP logo_link_a, DROP logo_link_b, CHANGE image image LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}

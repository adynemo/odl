<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220120012852 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Make name and slug unique for Era and Universe and rename clean_name by slug';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE period CHANGE clean_name slug VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE era CHANGE clean_name slug VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE universe CHANGE clean_name slug VARCHAR(50) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96E19A635CD9AF2211DDB11 ON era (universe_id, slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96E19A635CD9AF25E237E06 ON era (universe_id, name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_61353835211DDB11 ON universe (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_613538355E237E06 ON universe (name)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE period CHANGE slug clean_name VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE era CHANGE slug clean_name VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE universe CHANGE slug clean_name VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('DROP INDEX UNIQ_96E19A635CD9AF2211DDB11 ON era');
        $this->addSql('DROP INDEX UNIQ_96E19A635CD9AF25E237E06 ON era');
        $this->addSql('DROP INDEX UNIQ_61353835211DDB11 ON universe');
        $this->addSql('DROP INDEX UNIQ_613538355E237E06 ON universe');
    }
}
